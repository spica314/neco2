# neco2

neco2: **ne**co is a **co**mpiler version 2

hobby compiler & language (Felis)

target architectures: x86_64, NVIDIA GPU (PTX)

previous project: https://gitlab.com/spica314/neco

## directories
- `apps`: applications written in neco2 language
- `tests`: programs for testing
- `stage0`: neco2 compiler written in Rust
    - `neco`: compiler
    - `neco-common`: bitset, id-based table, etc.
    - `neco-ir`: intermediate representation
    - `neco-syn`: parser for neco2
