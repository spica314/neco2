#!/usr/bin/env bash

set -Eeuo pipefail

cargo run --bin main -- chapter2.neco2 --use-crt > t.s 2> /dev/null
gcc -o a.out t.s /opt/cuda/lib64/stubs/libcuda.so
./a.out > t.ppm
convert -strip t.ppm t.png
mogrify -strip t.png

echo "success"
