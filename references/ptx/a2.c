#include <stdio.h>
#include <cuda.h>
#include <assert.h>

int main() {
  CUresult result = CUDA_SUCCESS;

  result = cuInit(0);
  //assert(result == CUDA_SUCCESS);

  CUdevice device;
  result = cuDeviceGet(&device, 0);
  //assert(result == CUDA_SUCCESS);

  CUcontext context;
  result = cuCtxCreate_v2(&context, 0, device);
  //assert(result == CUDA_SUCCESS);

  CUmodule mod;
  result = cuModuleLoad(&mod, "b.ptx");
  //assert(result == CUDA_SUCCESS);

  CUfunction func;
  result = cuModuleGetFunction(&func, mod, "f");
  //assert(result == CUDA_SUCCESS);

  CUdeviceptr d_x;
  result = cuMemAlloc_v2(&d_x, sizeof(int));
  //assert(result == CUDA_SUCCESS);

  CUdeviceptr d_y;
  result = cuMemAlloc_v2(&d_y, sizeof(int));
  //assert(result == CUDA_SUCCESS);

  int y = 42;
  result = cuMemcpyHtoD_v2(d_y, &y, sizeof(int));
  //assert(result == CUDA_SUCCESS);

  void *args[] = {&d_x, &d_y};
  result = cuLaunchKernel(func, 1, 1, 1, 1, 1, 1, 0, 0, args, 0);
  //assert(result == CUDA_SUCCESS);

  int x;
  result = cuMemcpyDtoH_v2(&x, d_x, sizeof(int));
  //assert(result == CUDA_SUCCESS);

  printf("%d\n", x);
}
