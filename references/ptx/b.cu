extern "C" {
  __global__ void copy_array(int *xs, int *ys) {
    int thread_id = blockIdx.x * blockDim.x + threadIdx.x;
    xs[thread_id] = ys[thread_id];
  }
}
