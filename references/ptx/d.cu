__global__ void f(int *xs, int *ys) {
    int x0 = xs[0];
    int x1 = xs[1];
    int thread_id = blockIdx.x * blockDim.x + threadIdx.x;
    int *x;
    if (thread_id % 2 == 0) {
        x = &x0;
    } else {
        x = &x1;
    }
    xs[0] = *x;
}
