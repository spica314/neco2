typedef struct P {
    float x;
    float y;
} P;

__global__ void add(P *xs, P *ys) {
  int thread_id = blockIdx.x * blockDim.x + threadIdx.x;
  xs[thread_id].x = ys[thread_id].x;
  xs[thread_id].y = ys[thread_id].y;
  P t;
  t.x = xs[thread_id].x;
  t.x *= xs[thread_id].y;
  xs[thread_id].x = t.x;
}
