use std::collections::BTreeMap;
use std::marker::PhantomData;
use std::ops::*;

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Id<T> {
    id: usize,
    phantom: PhantomData<T>,
}

impl<T> Id<T> {
    pub fn id(&self) -> usize {
        self.id
    }
}

impl<T> Clone for Id<T> {
    fn clone(&self) -> Id<T> {
        Id::<T> {
            id: self.id,
            phantom: PhantomData,
        }
    }
}

impl<T> Copy for Id<T> {}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Table<T> {
    map: BTreeMap<usize, T>,
    next_id: usize,
}

impl<T> Table<T> {
    pub fn new() -> Table<T> {
        Table::<T> {
            map: BTreeMap::new(),
            next_id: 0,
        }
    }
    pub fn insert(&mut self, t: T) -> Id<T> {
        let id = self.next_id;
        self.next_id += 1;
        let res = Id::<T> {
            id,
            phantom: PhantomData,
        };
        self.map.insert(id, t);
        res
    }
    pub fn get(&self, id: Id<T>) -> Option<&T> {
        self.map.get(&id.id)
    }
    pub fn get_mut(&mut self, id: Id<T>) -> Option<&mut T> {
        self.map.get_mut(&id.id)
    }
    pub fn iter(&self) -> std::collections::btree_map::Iter<'_, usize, T> {
        self.map.iter()
    }
    pub fn iter_mut(&mut self) -> std::collections::btree_map::IterMut<'_, usize, T> {
        self.map.iter_mut()
    }
    pub fn to_ref_vec(&self) -> Vec<(Id<T>, &T)> {
        let mut res = vec![];
        for (id, t) in self.map.iter() {
            let id = Id::<T> {
                id: *id,
                phantom: PhantomData,
            };
            res.push((id, t));
        }
        res
    }
    pub fn next_id(&self) -> Id<T> {
        Id {
            id: self.next_id,
            phantom: PhantomData,
        }
    }
}

impl<T> Index<Id<T>> for Table<T> {
    type Output = T;
    fn index(&self, index: Id<T>) -> &T {
        &self.map[&index.id]
    }
}

impl<T> Index<&Id<T>> for Table<T> {
    type Output = T;
    fn index(&self, index: &Id<T>) -> &T {
        &self.map[&index.id]
    }
}
