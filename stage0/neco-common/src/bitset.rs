#[derive(Debug, Clone)]
pub struct BitSet {
    n: usize,
    xs: Vec<bool>,
}

impl BitSet {
    pub fn new(n: usize) -> BitSet {
        BitSet {
            n,
            xs: vec![false; n],
        }
    }
    pub fn get(&self, i: usize) -> bool {
        assert!(i < self.n);
        self.xs[i]
    }
    pub fn set(&mut self, i: usize) {
        assert!(i < self.n);
        self.xs[i] = true;
    }
    pub fn reset(&mut self, i: usize) {
        assert!(i < self.n);
        self.xs[i] = false;
    }
    pub fn shift_to_msb(&mut self, shift: usize) {
        for i in (shift..self.n).rev() {
            self.xs[i] = self.xs[i - shift];
        }
        for i in (0..shift).rev() {
            self.xs[i] = false;
        }
    }
    pub fn shift_to_lsb(&mut self, shift: usize) {
        for i in 0..self.n - 1 {
            self.xs[i] = self.xs[i + 1];
        }
        for i in self.n - shift..self.n {
            self.xs[i] = false;
        }
    }
}
