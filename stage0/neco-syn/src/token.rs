use super::*;

macro_rules! define_token_struct {
    ($t:ident, $pat:tt, $pu:tt) => {
        #[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
        pub struct $t {
            pub s: String,
        }

        impl Lex for $t {
            fn lex(s: &mut CharStream) -> Result<Token, LexError> {
                let mut i = 0;
                while i < s.len() && s[i].is_whitespace() {
                    i += 1;
                }
                let cs: Vec<_> = $pat.chars().collect();
                if i + cs.len() <= s.len() && &s[i..i + cs.len()] == cs.as_slice() {
                    let res = $t {
                        s: $pat.to_string(),
                    };
                    eprintln!("i + cs.len() = {}", i + cs.len());
                    s.add_offset(cs.len());
                    Ok(Token::$t(res))
                } else {
                    Err(LexError::LexError)
                }
            }
            fn prefer_uniop() -> bool {
                $pu
            }
        }

        impl Parse for $t {
            fn parse(s: &mut TokenStream) -> Result<$t, ParseError> {
                if s.is_empty() {
                    Err(ParseError::ParseError)
                } else if let Token::$t(x) = s[0].clone() {
                    s.add_offset(1);
                    Ok(x)
                } else {
                    Err(ParseError::ParseError)
                }
            }
        }
    };
}

define_token_struct!(Break, "break", false);
define_token_struct!(Match, "match", false);
define_token_struct!(Loop, "loop", false);
define_token_struct!(Data, "data", false);
define_token_struct!(Else, "else", false);
define_token_struct!(Let, "let", false);
define_token_struct!(Fn, "fn", false);
define_token_struct!(If, "if", false);
define_token_struct!(Colon2, "::", true);
define_token_struct!(FatArrow, "=>", true);
define_token_struct!(EqEq, "==", true);
define_token_struct!(Ne, "!=", true);
define_token_struct!(Lt, "<", true);
define_token_struct!(Le, "<=", true);
define_token_struct!(Gt, ">", true);
define_token_struct!(Ge, ">=", true);
define_token_struct!(Unit, "()", false);
define_token_struct!(Lparen, "(", true);
define_token_struct!(Rparen, ")", false);
define_token_struct!(Eq, "=", true);
define_token_struct!(Lbrace, "{", true);
define_token_struct!(Rbrace, "}", false);
define_token_struct!(Lbracket, "[", true);
define_token_struct!(Rbracket, "]", false);
define_token_struct!(Semicolon, ";", true);
define_token_struct!(Camma, ",", true);
define_token_struct!(Colon, ":", true);
define_token_struct!(Add, "+", true);
define_token_struct!(Sub, "-", true);
define_token_struct!(Mul, "*", true);
define_token_struct!(Div, "/", true);
define_token_struct!(Rem, "%", true);
define_token_struct!(Or, "|", true);
define_token_struct!(Pound, "#", true);

define_token_struct!(Ref, "&", true);
define_token_struct!(Deref, "*", true);
define_token_struct!(Neg, "-", true);

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct Literal {
    pub s: String,
}

impl Literal {
    pub fn as_str(&self) -> &str {
        &self.s
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct LitBuiltin {
    pub lit: Literal,
}

impl LitBuiltin {
    pub fn as_str(&self) -> &str {
        self.lit.as_str()
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct LitNum {
    pub lit: Literal,
}

impl LitNum {
    pub fn as_str(&self) -> &str {
        self.lit.as_str()
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum Lit {
    Num(LitNum),
    Builtin(LitBuiltin),
    Verbatim(Literal),
}

impl Lit {
    pub fn as_str(&self) -> &str {
        match self {
            Lit::Num(num) => num.as_str(),
            Lit::Builtin(builtin) => builtin.as_str(),
            Lit::Verbatim(verbatim) => verbatim.as_str(),
        }
    }
}

impl Lex for Lit {
    fn lex(s: &mut CharStream) -> Result<Token, LexError> {
        let mut i = 0;
        while i < s.len() && s[i].is_whitespace() {
            i += 1;
        }
        let mut k = i;
        while k < s.len() && (s[k] == '_' || s[k] == '.' || s[k].is_ascii_alphanumeric()) {
            k += 1;
        }
        if i == k {
            return Err(LexError::LexError);
        }
        let lit_s: String = s[i..k].iter().collect();
        let lit = if s[i] == '_' && s[i + 1] == '_' {
            Lit::Builtin(LitBuiltin {
                lit: Literal { s: lit_s },
            })
        } else if s[i].is_ascii_digit() {
            Lit::Num(LitNum {
                lit: Literal { s: lit_s },
            })
        } else if s[i].is_ascii_alphabetic() {
            Lit::Verbatim(Literal { s: lit_s })
        } else {
            panic!();
        };
        s.add_offset(k);
        Ok(Token::Lit(lit))
    }
    fn prefer_uniop() -> bool {
        false
    }
}

impl Parse for Lit {
    fn parse(s: &mut TokenStream) -> Result<Lit, ParseError> {
        if let Token::Lit(lit) = s[0].clone() {
            s.add_offset(1);
            Ok(lit)
        } else {
            Err(ParseError::ParseError)
        }
    }
}

macro_rules! define_token {
    ($($t:tt),*) => {
        #[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
        pub enum Token {
            $(
                $t($t),
            )*
        }

        impl Token {
            pub fn prefer_uniop(&self) -> bool {
                match self {
                    $(
                        Token::$t(_) => $t::prefer_uniop(),
                    )*
                }
            }
        }
    };
}

define_token!(
    Lit, Let, Fn, Unit, Lparen, Rparen, Eq, Lbrace, Rbrace, Semicolon, Camma, Colon, Ref, EqEq,
    Rem, Else, If, Add, Sub, Mul, Div, Data, Match, FatArrow, Colon2, Or, Neg, Deref, Lbracket,
    Rbracket, Ne, Gt, Ge, Lt, Le, Pound, Loop, Break
);
