use super::token;
use super::*;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct CharStream {
    cs: Vec<char>,
    i: usize,
}

pub struct Chars<'a> {
    iter: std::slice::Iter<'a, char>,
}

impl<'a> Iterator for Chars<'a> {
    type Item = char;
    fn next(&mut self) -> Option<char> {
        if let Some(c) = self.iter.next() {
            Some(*c)
        } else {
            None
        }
    }
}

impl CharStream {
    pub fn new(s: &str) -> CharStream {
        CharStream {
            cs: s.chars().collect(),
            i: 0,
        }
    }
    pub fn is_empty(&self) -> bool {
        self.i == self.cs.len()
    }
    pub fn skip_line_comment(&mut self) {
        while self.i < self.cs.len() && self.cs[self.i] != '\n' {
            self.i += 1;
        }
        self.i += 1;
    }
    pub fn skip_multiline_comment(&mut self) {
        let mut depth = 0;
        while self.i < self.cs.len() {
            if self.i + 2 < self.cs.len() && self.cs[self.i] == '/' && self.cs[self.i + 1] == '*' {
                depth += 1;
                self.i += 2;
                continue;
            }
            if self.i + 2 < self.cs.len() && self.cs[self.i] == '*' && self.cs[self.i + 1] == '/' {
                depth -= 1;
                self.i += 2;
                if depth == 0 {
                    break;
                }
                continue;
            }
            self.i += 1;
        }
        assert!(depth == 0);
    }
    pub fn ignore_whitespaces(&mut self) {
        while self.i < self.cs.len() && self.cs[self.i].is_whitespace() {
            self.i += 1;
        }
    }
    pub fn lex<T: Lex>(&mut self) -> Result<Token, LexError> {
        let prev_i = self.i;
        self.ignore_whitespaces();
        match T::lex(self) {
            Ok(t) => Ok(t),
            Err(err) => {
                self.i = prev_i;
                Err(err)
            }
        }
    }
    pub fn lex_uniops(&mut self, prev_prefer_uniop: bool) -> Result<Vec<Token>, LexError> {
        let mut res = vec![];
        let prev_i = self.i;
        self.ignore_whitespaces();
        loop {
            let t = self
                .lex::<token::Ref>()
                .or_else(|_| self.lex::<token::Neg>())
                .or_else(|_| self.lex::<token::Deref>());
            match t {
                Ok(t) => {
                    res.push(t);
                }
                Err(_) => {
                    break;
                }
            }
        }
        if self.is_empty() {
            self.i = prev_i;
            return Err(LexError::LexError);
        }
        if !prev_prefer_uniop && self[0].is_whitespace() {
            self.i = prev_i;
            return Err(LexError::LexError);
        }
        if res.is_empty() {
            self.i = prev_i;
            return Err(LexError::LexError);
        }
        Ok(res)
    }
    pub fn chars(&self) -> Chars<'_> {
        Chars {
            iter: self.cs[self.i..].iter(),
        }
    }
    pub fn set_offset(&mut self, i: usize) {
        self.i = i;
    }
    pub fn add_offset(&mut self, add: usize) {
        self.i += add;
    }
    pub fn offset(&self) -> usize {
        self.i
    }
}

impl Deref for CharStream {
    type Target = [char];
    fn deref(&self) -> &Self::Target {
        &self.cs[self.i..]
    }
}

pub trait Lex: Sized {
    fn lex(s: &mut CharStream) -> Result<Token, LexError>;
    // TODO: 3 state
    fn prefer_uniop() -> bool;
}
