use super::*;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct TokenStream {
    tokens: Vec<Token>,
    i: usize,
}

impl TokenStream {
    pub fn new(tokens: Vec<Token>) -> TokenStream {
        TokenStream { tokens, i: 0 }
    }
    pub fn is_empty(&self) -> bool {
        self.i == self.tokens.len()
    }
    pub fn parse<T: Parse>(&mut self) -> Result<T, ParseError> {
        let prev_i = self.i;
        match T::parse(self) {
            Ok(t) => Ok(t),
            Err(err) => {
                self.i = prev_i;
                Err(err)
            }
        }
    }
    pub fn parse_rep0<T: Parse>(&mut self) -> Result<Vec<T>, ParseError> {
        let mut res = vec![];
        while let Ok(x) = self.parse::<T>() {
            res.push(x);
        }
        Ok(res)
    }
    pub fn parse_rep1<T: Parse>(&mut self) -> Result<Vec<T>, ParseError> {
        let mut res = vec![];
        while let Ok(x) = self.parse::<T>() {
            res.push(x);
        }
        if res.is_empty() {
            Err(ParseError::ParseError)
        } else {
            Ok(res)
        }
    }
    pub fn set_offset(&mut self, i: usize) {
        self.i = i;
    }
    pub fn add_offset(&mut self, add: usize) {
        self.i += add;
    }
    pub fn offset(&self) -> usize {
        self.i
    }
}

impl Deref for TokenStream {
    type Target = [Token];
    fn deref(&self) -> &Self::Target {
        &self.tokens[self.i..]
    }
}

pub trait Parse: Sized {
    fn parse(s: &mut TokenStream) -> Result<Self, ParseError>;
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct Punctuated<T, U> {
    ts: Vec<T>,
    us: Vec<U>,
}

impl<T, U> Punctuated<T, U> {
    pub fn is_empty(&self) -> bool {
        self.ts.is_empty()
    }
    pub fn lefts(&self) -> &[T] {
        &self.ts
    }
}

impl<T: Parse> Parse for Option<T> {
    fn parse(s: &mut TokenStream) -> Result<Option<T>, ParseError> {
        if let Ok(x) = s.parse::<T>() {
            Ok(Some(x))
        } else {
            Ok(None)
        }
    }
}

impl<T: Parse> Parse for Box<T> {
    fn parse(s: &mut TokenStream) -> Result<Box<T>, ParseError> {
        s.parse::<T>().map(Box::new)
    }
}

impl<T: Parse, U: Parse> Parse for (T, U) {
    fn parse(s: &mut TokenStream) -> Result<(T, U), ParseError> {
        Ok((s.parse::<T>()?, s.parse::<U>()?))
    }
}

impl<T: Parse, U: Parse> Parse for Punctuated<T, U> {
    fn parse(s: &mut TokenStream) -> Result<Punctuated<T, U>, ParseError> {
        let mut ts = vec![];
        let mut us = vec![];
        loop {
            if let Ok(t) = s.parse() {
                ts.push(t);
            } else {
                break;
            }
            if let Ok(u) = s.parse() {
                us.push(u);
            } else {
                break;
            }
        }
        Ok(Punctuated { ts, us })
    }
}
