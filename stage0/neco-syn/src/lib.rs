pub mod char_stream;
pub mod token;
pub mod token_stream;

use char_stream::*;
use token::*;
use token_stream::*;

use std::ops::Deref;

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum LexError {
    LexError,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum ParseError {
    ParseError,
}

pub fn lex(s: &str) -> TokenStream {
    let mut s = CharStream::new(s);
    let mut ts: Vec<Token> = vec![];
    while !s.is_empty() {
        if !ts.is_empty() {
            let uniops = s.lex_uniops(ts[ts.len() - 1].prefer_uniop());
            if let Ok(uniops) = uniops {
                ts.extend(uniops);
                continue;
            }
        }

        s.ignore_whitespaces();
        if s.is_empty() {
            break;
        }

        if s.len() >= 2 && s[0] == '/' && s[1] == '/' {
            s.skip_line_comment();
            continue;
        }

        if s.len() >= 2 && s[0] == '/' && s[1] == '*' {
            s.skip_multiline_comment();
            continue;
        }

        let t = s
            .lex::<Match>()
            .or_else(|_| s.lex::<Break>())
            .or_else(|_| s.lex::<Else>())
            .or_else(|_| s.lex::<Loop>())
            .or_else(|_| s.lex::<Data>())
            .or_else(|_| s.lex::<Let>())
            .or_else(|_| s.lex::<Fn>())
            .or_else(|_| s.lex::<If>())
            .or_else(|_| s.lex::<Colon2>())
            .or_else(|_| s.lex::<FatArrow>())
            .or_else(|_| s.lex::<EqEq>())
            .or_else(|_| s.lex::<Ne>())
            .or_else(|_| s.lex::<Le>())
            .or_else(|_| s.lex::<Ge>())
            .or_else(|_| s.lex::<Unit>())
            .or_else(|_| s.lex::<Lparen>())
            .or_else(|_| s.lex::<Rparen>())
            .or_else(|_| s.lex::<Eq>())
            .or_else(|_| s.lex::<Lbrace>())
            .or_else(|_| s.lex::<Rbrace>())
            .or_else(|_| s.lex::<Semicolon>())
            .or_else(|_| s.lex::<Camma>())
            .or_else(|_| s.lex::<Colon>())
            .or_else(|_| s.lex::<Add>())
            .or_else(|_| s.lex::<Sub>())
            .or_else(|_| s.lex::<Mul>())
            .or_else(|_| s.lex::<Div>())
            .or_else(|_| s.lex::<Rem>())
            .or_else(|_| s.lex::<Lit>())
            .or_else(|_| s.lex::<Or>())
            .or_else(|_| s.lex::<Lt>())
            .or_else(|_| s.lex::<Gt>())
            .or_else(|_| s.lex::<Lbracket>())
            .or_else(|_| s.lex::<Rbracket>())
            .or_else(|_| s.lex::<Pound>());
        match t {
            Ok(t) => {
                ts.push(t);
            }
            Err(err) => {
                s.ignore_whitespaces();
                if s.is_empty() {
                    break;
                }
                panic!("err = {:?}", err);
            }
        }
    }
    TokenStream::new(ts)
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct File {
    pub items: Vec<Item>,
}

impl Parse for File {
    fn parse(s: &mut TokenStream) -> Result<File, ParseError> {
        Ok(File {
            items: s.parse_rep0()?,
        })
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum Item {
    ItemFn(ItemFn),
    ItemData(ItemData),
}

impl Parse for Item {
    fn parse(s: &mut TokenStream) -> Result<Item, ParseError> {
        s.parse::<ItemFn>()
            .map(Item::ItemFn)
            .or_else(|_| s.parse::<ItemData>().map(Item::ItemData))
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct TypeReference {
    pub ref_token: Ref,
    pub elem: Box<Type>,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct TypeArray {
    pub lbracket: Lbracket,
    pub elem: Box<Type>,
    pub semi_token: Semicolon,
    pub len: Lit,
    pub rbracket: Rbracket,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum Type {
    Array(TypeArray),
    Reference(TypeReference),
    Verbatim(Lit),
}

impl Parse for TypeArray {
    fn parse(s: &mut TokenStream) -> Result<TypeArray, ParseError> {
        Ok(TypeArray {
            lbracket: s.parse()?,
            elem: s.parse()?,
            semi_token: s.parse()?,
            len: s.parse()?,
            rbracket: s.parse()?,
        })
    }
}

impl Parse for Type {
    fn parse(s: &mut TokenStream) -> Result<Type, ParseError> {
        let mut refs = s.parse_rep0::<Ref>()?;
        let mut res = s
            .parse::<TypeArray>()
            .map(|x| Type::Array(x))
            .or_else(|_| s.parse::<Lit>().map(|x| Type::Verbatim(x)))?;
        while let Some(r) = refs.pop() {
            res = Type::Reference(TypeReference {
                ref_token: r,
                elem: Box::new(res),
            });
        }
        Ok(res)
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum FnArg {
    Unit(Unit),
    Typed(PatType),
}

impl Parse for FnArg {
    fn parse(s: &mut TokenStream) -> Result<FnArg, ParseError> {
        s.parse::<Unit>()
            .map(FnArg::Unit)
            .or_else(|_| s.parse::<PatType>().map(FnArg::Typed))
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct PatType {
    pub lparen: Lparen,
    pub pat: Box<Lit>,
    pub colon_token: Colon,
    pub ty: Box<Type>,
    pub rparen: Rparen,
}

impl Parse for PatType {
    fn parse(s: &mut TokenStream) -> Result<PatType, ParseError> {
        Ok(PatType {
            lparen: s.parse()?,
            pat: Box::new(s.parse()?),
            colon_token: s.parse()?,
            ty: Box::new(s.parse()?),
            rparen: s.parse()?,
        })
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct Attribute {
    pub pound: Pound,
    pub lbracket: Lbracket,
    pub lit: Lit,
    pub rbracket: Rbracket,
}

impl Parse for Attribute {
    fn parse(s: &mut TokenStream) -> Result<Attribute, ParseError> {
        Ok(Attribute {
            pound: s.parse()?,
            lbracket: s.parse()?,
            lit: s.parse()?,
            rbracket: s.parse()?,
        })
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct ItemFn {
    pub attrs: Vec<Attribute>,
    pub fn_token: Fn,
    pub name: Lit,
    pub inputs: Vec<FnArg>,
    pub ret_ty: Option<(Colon, Type)>,
    pub eq: Eq,
    pub expr: Box<Expr>,
}

impl Parse for ItemFn {
    fn parse(s: &mut TokenStream) -> Result<ItemFn, ParseError> {
        Ok(ItemFn {
            attrs: s.parse_rep0()?,
            fn_token: s.parse()?,
            name: s.parse()?,
            inputs: s.parse_rep1()?,
            ret_ty: s.parse::<(Colon, Type)>().map(Some).unwrap_or(None),
            eq: s.parse()?,
            expr: Box::new(s.parse()?),
        })
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct ItemData {
    pub data_token: Data,
    pub name: Lit,
    pub eq_token: Eq,
    pub variants: Punctuated<Variant, Or>,
    pub semicolon_token: Semicolon,
}

impl Parse for ItemData {
    fn parse(s: &mut TokenStream) -> Result<ItemData, ParseError> {
        Ok(ItemData {
            data_token: s.parse()?,
            name: s.parse()?,
            eq_token: s.parse()?,
            variants: s.parse()?,
            semicolon_token: s.parse()?,
        })
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct Variant {
    pub constructor: Lit,
    pub fields: Vec<PatType>,
}

impl Parse for Variant {
    fn parse(s: &mut TokenStream) -> Result<Variant, ParseError> {
        Ok(Variant {
            constructor: s.parse()?,
            fields: s.parse_rep0()?,
        })
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum Expr {
    ExprBlock(Block),
    ExprCall(ExprCall),
    ExprLit(ExprLit),
    ExprUnit(ExprUnit),
    ExprTuple(ExprTuple),
    ExprAssign(ExprAssign),
    If(ExprIf),
    Paren(ExprParen),
    Binary(ExprBinary),
    Match(ExprMatch),
    Data(ExprData),
    Unary(ExprUnary),
    Index(ExprIndex),
    Repeat(ExprRepeat),
    Loop(ExprLoop),
    Break(ExprBreak),
}

trait IntoExpr {
    fn into_expr(self) -> Expr;
}

impl Parse for Expr {
    fn parse(s: &mut TokenStream) -> Result<Expr, ParseError> {
        fn precedence_climbing(s: &mut TokenStream, prev_pred: i64) -> Result<Expr, ParseError> {
            let mut expr = s.parse::<Term>()?.into_expr();
            let mut prev_offset = s.offset();
            while let Ok(op) = s.parse::<BinOp>() {
                let pred = op.precedence();
                if pred <= prev_pred {
                    s.set_offset(prev_offset);
                    break;
                }
                let right = precedence_climbing(s, pred)?;
                expr = Expr::Binary(ExprBinary {
                    left: Box::new(expr),
                    op,
                    right: Box::new(right),
                });
                prev_offset = s.offset();
            }
            Ok(expr)
        }
        let expr = precedence_climbing(s, -1)?;
        if !s.is_empty() {
            if let Ok(eq_token) = s.parse() {
                let right = s.parse()?;
                return Ok(Expr::ExprAssign(ExprAssign {
                    left: Box::new(expr),
                    eq_token,
                    right: Box::new(right),
                }));
            }
        }
        eprintln!("s = {:?}, expr = {:?}", s, expr);
        Ok(expr)
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
struct Arg {
    expr: Expr,
}

impl IntoExpr for Arg {
    fn into_expr(self) -> Expr {
        self.expr
    }
}

impl Parse for Arg {
    fn parse(s: &mut TokenStream) -> Result<Arg, ParseError> {
        let mut expr = s
            .parse::<ExprUnit>()
            .map(|x| x.into_expr())
            .or_else(|_| s.parse::<ExprParen>().map(Expr::Paren))
            .or_else(|_| s.parse::<ExprTuple>().map(Expr::ExprTuple))
            .or_else(|_| s.parse::<ExprUnaryForArg>().map(|x| x.into_expr()))
            .or_else(|_| s.parse::<ExprRepeat>().map(|x| x.into_expr()))
            .or_else(|_| s.parse::<ExprLit>().map(Expr::ExprLit))?;
        while !s.is_empty() {
            if let Token::Lbracket(lbracket) = s[0].clone() {
                s.add_offset(1);
                let index = s.parse::<Expr>()?;
                let rbracket = s.parse::<Rbracket>()?;
                expr = Expr::Index(ExprIndex {
                    expr: Box::new(expr),
                    lbracket,
                    index: Box::new(index),
                    rbracket,
                });
            } else {
                break;
            }
        }
        Ok(Arg { expr })
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
struct Term {
    expr: Expr,
}

impl IntoExpr for Term {
    fn into_expr(self) -> Expr {
        self.expr
    }
}

impl Parse for Term {
    fn parse(s: &mut TokenStream) -> Result<Term, ParseError> {
        let expr = s
            .parse::<ExprData>()
            .map(|x| x.into_expr())
            .or_else(|_| s.parse::<ExprUnary>().map(|x| x.into_expr()))
            .or_else(|_| s.parse::<ExprCall>().map(|x| x.into_expr()))
            .or_else(|_| s.parse::<ExprMatch>().map(|x| x.into_expr()))
            .or_else(|_| s.parse::<ExprIf>().map(|x| x.into_expr()))
            .or_else(|_| s.parse::<ExprLoop>().map(|x| x.into_expr()))
            .or_else(|_| s.parse::<ExprBreak>().map(|x| x.into_expr()))
            .or_else(|_| s.parse::<Block>().map(|x| x.into_expr()))
            .or_else(|_| s.parse::<Arg>().map(|x| x.into_expr()))?;
        Ok(Term { expr })
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct ExprUnit {
    unit_token: Unit,
}

impl IntoExpr for ExprUnit {
    fn into_expr(self) -> Expr {
        Expr::ExprUnit(self)
    }
}

impl Parse for ExprUnit {
    fn parse(s: &mut TokenStream) -> Result<ExprUnit, ParseError> {
        Ok(ExprUnit {
            unit_token: s.parse()?,
        })
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum UnOp {
    Ref(Ref),
    Deref(token::Deref),
    Neg(Neg),
}

impl Parse for UnOp {
    fn parse(s: &mut TokenStream) -> Result<UnOp, ParseError> {
        s.parse::<Neg>()
            .map(UnOp::Neg)
            .or_else(|_| s.parse::<Ref>().map(UnOp::Ref))
            .or_else(|_| s.parse::<token::Deref>().map(UnOp::Deref))
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct ExprUnary {
    pub op: UnOp,
    pub expr: Box<Expr>,
}

impl IntoExpr for ExprUnary {
    fn into_expr(self) -> Expr {
        Expr::Unary(self)
    }
}

impl Parse for ExprUnary {
    fn parse(s: &mut TokenStream) -> Result<ExprUnary, ParseError> {
        Ok(ExprUnary {
            op: s.parse()?,
            expr: Box::new(s.parse::<Term>()?.into_expr()),
        })
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct ExprLoop {
    pub loop_token: Loop,
    pub body: Block,
}

impl IntoExpr for ExprLoop {
    fn into_expr(self) -> Expr {
        Expr::Loop(self)
    }
}

impl Parse for ExprLoop {
    fn parse(s: &mut TokenStream) -> Result<ExprLoop, ParseError> {
        Ok(ExprLoop {
            loop_token: s.parse()?,
            body: s.parse()?,
        })
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct ExprBreak {
    pub break_token: Break,
}

impl IntoExpr for ExprBreak {
    fn into_expr(self) -> Expr {
        Expr::Break(self)
    }
}

impl Parse for ExprBreak {
    fn parse(s: &mut TokenStream) -> Result<ExprBreak, ParseError> {
        Ok(ExprBreak {
            break_token: s.parse()?,
        })
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct ExprUnaryForArg {
    pub expr_unary: ExprUnary,
}

impl IntoExpr for ExprUnaryForArg {
    fn into_expr(self) -> Expr {
        Expr::Unary(self.expr_unary)
    }
}

impl Parse for ExprUnaryForArg {
    fn parse(s: &mut TokenStream) -> Result<ExprUnaryForArg, ParseError> {
        Ok(ExprUnaryForArg {
            expr_unary: ExprUnary {
                op: s.parse()?,
                expr: Box::new(s.parse::<Arg>()?.into_expr()),
            },
        })
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct Block {
    pub lbrace: Lbrace,
    pub stmts: Vec<Stmt>,
    pub rbrace: Rbrace,
}

impl IntoExpr for Block {
    fn into_expr(self) -> Expr {
        Expr::ExprBlock(self)
    }
}

impl Parse for Block {
    fn parse(s: &mut TokenStream) -> Result<Block, ParseError> {
        Ok(Block {
            lbrace: s.parse()?,
            stmts: s.parse_rep0()?,
            rbrace: s.parse()?,
        })
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct ExprCall {
    pub f: Lit,
    pub args: Vec<Expr>,
}

impl IntoExpr for ExprCall {
    fn into_expr(self) -> Expr {
        Expr::ExprCall(self)
    }
}

impl Parse for ExprCall {
    fn parse(s: &mut TokenStream) -> Result<ExprCall, ParseError> {
        Ok(ExprCall {
            f: s.parse()?,
            args: s
                .parse_rep1::<Arg>()?
                .into_iter()
                .map(|x| x.into_expr())
                .collect(),
        })
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct ExprData {
    pub data_name: Lit,
    pub colon2_token: Colon2,
    pub constructor_name: Lit,
    pub args: Vec<Expr>,
}

impl IntoExpr for ExprData {
    fn into_expr(self) -> Expr {
        Expr::Data(self)
    }
}

impl Parse for ExprData {
    fn parse(s: &mut TokenStream) -> Result<ExprData, ParseError> {
        Ok(ExprData {
            data_name: s.parse()?,
            colon2_token: s.parse()?,
            constructor_name: s.parse()?,
            args: s
                .parse_rep0::<Arg>()?
                .into_iter()
                .map(|x| x.into_expr())
                .collect(),
        })
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct ExprLit {
    pub lit: Lit,
}

impl IntoExpr for ExprLit {
    fn into_expr(self) -> Expr {
        Expr::ExprLit(self)
    }
}

impl Parse for ExprLit {
    fn parse(s: &mut TokenStream) -> Result<ExprLit, ParseError> {
        Ok(ExprLit { lit: s.parse()? })
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct ExprTuple {
    pub lparen: Lparen,
    pub exprs: Punctuated<Expr, Camma>,
    pub rparen: Rparen,
}

impl IntoExpr for ExprTuple {
    fn into_expr(self) -> Expr {
        Expr::ExprTuple(self)
    }
}

impl Parse for ExprTuple {
    fn parse(s: &mut TokenStream) -> Result<ExprTuple, ParseError> {
        Ok(ExprTuple {
            lparen: s.parse()?,
            exprs: s.parse()?,
            rparen: s.parse()?,
        })
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct ExprAssign {
    pub left: Box<Expr>,
    pub eq_token: Eq,
    pub right: Box<Expr>,
}

impl Parse for ExprAssign {
    fn parse(s: &mut TokenStream) -> Result<ExprAssign, ParseError> {
        Ok(ExprAssign {
            left: Box::new(s.parse::<Arg>()?.into_expr()),
            eq_token: s.parse()?,
            right: Box::new(s.parse()?),
        })
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum Stmt {
    Local(Local),
    StmtSemi(StmtSemi),
    Expr(Expr),
}

impl Parse for Stmt {
    fn parse(s: &mut TokenStream) -> Result<Stmt, ParseError> {
        s.parse::<Local>()
            .map(Stmt::Local)
            .or_else(|_| s.parse::<StmtSemi>().map(Stmt::StmtSemi))
            .or_else(|_| s.parse::<Expr>().map(Stmt::Expr))
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct Local {
    pub let_token: Let,
    pub ident: Lit,
    pub init: Option<(Eq, Box<Expr>)>,
    pub semi_token: Semicolon,
}

impl Parse for Local {
    fn parse(s: &mut TokenStream) -> Result<Local, ParseError> {
        Ok(Local {
            let_token: s.parse()?,
            ident: s.parse()?,
            init: s.parse()?,
            semi_token: s.parse()?,
        })
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct StmtSemi {
    pub expr: Box<Expr>,
    pub semicolon: Semicolon,
}

impl Parse for StmtSemi {
    fn parse(s: &mut TokenStream) -> Result<StmtSemi, ParseError> {
        Ok(StmtSemi {
            expr: Box::new(s.parse()?),
            semicolon: s.parse()?,
        })
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum BinOp {
    Add(Add),
    Sub(Sub),
    Mul(Mul),
    Div(Div),
    Rem(Rem),
    Eq(EqEq),
    Ne(Ne),
    Lt(Lt),
    Le(Le),
    Gt(Gt),
    Ge(Ge),
}

impl BinOp {
    pub fn precedence(&self) -> i64 {
        match self {
            BinOp::Add(_) => 20,
            BinOp::Sub(_) => 20,
            BinOp::Mul(_) => 30,
            BinOp::Div(_) => 30,
            BinOp::Rem(_) => 30,
            BinOp::Eq(_) => 10,
            BinOp::Ne(_) => 10,
            BinOp::Lt(_) => 10,
            BinOp::Le(_) => 10,
            BinOp::Gt(_) => 10,
            BinOp::Ge(_) => 10,
        }
    }
}

impl Parse for BinOp {
    fn parse(s: &mut TokenStream) -> Result<BinOp, ParseError> {
        if s.is_empty() {
            return Err(ParseError::ParseError);
        }
        match s[0].clone() {
            Token::Add(add) => {
                s.add_offset(1);
                Ok(BinOp::Add(add))
            }
            Token::Sub(sub) => {
                s.add_offset(1);
                Ok(BinOp::Sub(sub))
            }
            Token::Mul(mul) => {
                s.add_offset(1);
                Ok(BinOp::Mul(mul))
            }
            Token::Div(div) => {
                s.add_offset(1);
                Ok(BinOp::Div(div))
            }
            Token::Rem(rem) => {
                s.add_offset(1);
                Ok(BinOp::Rem(rem))
            }
            Token::EqEq(eqeq) => {
                s.add_offset(1);
                Ok(BinOp::Eq(eqeq))
            }
            Token::Ne(ne) => {
                s.add_offset(1);
                Ok(BinOp::Ne(ne))
            }
            Token::Lt(lt) => {
                s.add_offset(1);
                Ok(BinOp::Lt(lt))
            }
            Token::Le(le) => {
                s.add_offset(1);
                Ok(BinOp::Le(le))
            }
            Token::Gt(gt) => {
                s.add_offset(1);
                Ok(BinOp::Gt(gt))
            }
            Token::Ge(ge) => {
                s.add_offset(1);
                Ok(BinOp::Ge(ge))
            }
            _ => Err(ParseError::ParseError),
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct ExprIf {
    pub if_token: If,
    pub cond: Box<Expr>,
    pub then_branch: Block,
    pub else_branch: (Else, Box<Expr>),
}

impl IntoExpr for ExprIf {
    fn into_expr(self) -> Expr {
        Expr::If(self)
    }
}

impl Parse for ExprIf {
    fn parse(s: &mut TokenStream) -> Result<ExprIf, ParseError> {
        Ok(ExprIf {
            if_token: s.parse()?,
            cond: Box::new(s.parse()?),
            then_branch: s.parse()?,
            else_branch: {
                if let Ok(x) = s.parse::<(Else, ExprIf)>() {
                    (x.0, Box::new(x.1.into_expr()))
                } else if let Ok(x) = s.parse::<(Else, Block)>() {
                    (x.0, Box::new(x.1.into_expr()))
                } else {
                    return Err(ParseError::ParseError);
                }
            },
        })
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct ExprMatch {
    pub match_token: Match,
    pub expr: Box<Expr>,
    pub lbrace: Lbrace,
    pub arms: Vec<Arm>,
    pub rbrace: Rbrace,
}

impl IntoExpr for ExprMatch {
    fn into_expr(self) -> Expr {
        Expr::Match(self)
    }
}

impl Parse for ExprMatch {
    fn parse(s: &mut TokenStream) -> Result<ExprMatch, ParseError> {
        Ok(ExprMatch {
            match_token: s.parse()?,
            expr: Box::new(s.parse()?),
            lbrace: s.parse()?,
            arms: s.parse_rep0()?,
            rbrace: s.parse()?,
        })
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct Arm {
    pub data_name: Lit,
    pub colon2_token: Colon2,
    pub constructor_name: Lit,
    pub elems: Vec<Lit>,
    pub fat_arrow_token: FatArrow,
    pub expr: Box<Expr>,
    pub semicolon_token: Option<Semicolon>,
}

impl Parse for Arm {
    fn parse(s: &mut TokenStream) -> Result<Arm, ParseError> {
        Ok(Arm {
            data_name: s.parse()?,
            colon2_token: s.parse()?,
            constructor_name: s.parse()?,
            elems: s.parse_rep0()?,
            fat_arrow_token: s.parse()?,
            expr: Box::new(s.parse()?),
            semicolon_token: s.parse()?,
        })
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct ExprParen {
    pub lparen: Lparen,
    pub expr: Box<Expr>,
    pub rparen: Rparen,
}

impl IntoExpr for ExprParen {
    fn into_expr(self) -> Expr {
        Expr::Paren(self)
    }
}

impl Parse for ExprParen {
    fn parse(s: &mut TokenStream) -> Result<ExprParen, ParseError> {
        Ok(ExprParen {
            lparen: s.parse()?,
            expr: Box::new(s.parse()?),
            rparen: s.parse()?,
        })
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct ExprBinary {
    pub left: Box<Expr>,
    pub op: BinOp,
    pub right: Box<Expr>,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct ExprIndex {
    pub expr: Box<Expr>,
    pub lbracket: Lbracket,
    pub index: Box<Expr>,
    pub rbracket: Rbracket,
}

impl IntoExpr for ExprIndex {
    fn into_expr(self) -> Expr {
        Expr::Index(self)
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct ExprRepeat {
    pub lbracket: Lbracket,
    pub expr: Box<Expr>,
    pub semi_token: Semicolon,
    pub len: Box<Lit>,
    pub rbracket: Rbracket,
}

impl Parse for ExprRepeat {
    fn parse(s: &mut TokenStream) -> Result<ExprRepeat, ParseError> {
        Ok(ExprRepeat {
            lbracket: s.parse()?,
            expr: s.parse()?,
            semi_token: s.parse()?,
            len: s.parse()?,
            rbracket: s.parse()?,
        })
    }
}

impl IntoExpr for ExprRepeat {
    fn into_expr(self) -> Expr {
        Expr::Repeat(self)
    }
}
