use std::collections::{HashMap, HashSet};

use crate::*;

impl IR {
    pub fn inlining(&mut self) {
        let max_depth = 10;
        for _ in 0..max_depth {
            let updated = self.inlining_inner();
            if !updated {
                return;
            }
        }
        panic!("inlining depth >= {}", max_depth);
    }
    fn inlining_inner(&mut self) -> bool {
        let mut updated = false;
        for &function_item_id in &self.functions {
            let mut fun_def = self.fun_defs[function_item_id].clone();
            let mut stmts = IRStmts::new();
            for stmt in &fun_def.stmts.stmts {
                match stmt {
                    IRStmt::Call(idt, f, args) => {
                        if !self.fun_defs[f].inline {
                            stmts.push(IRStmt::Call(*idt, *f, args.clone()));
                            continue;
                        }
                        eprintln!("idt = {:?}, f = {:?}, args = {:?}", idt, f, args);
                        // args
                        let mut var_map: HashMap<Id<VariableInfo>, Id<VariableInfo>> =
                            HashMap::new();
                        let mut label_map: HashMap<Id<Label>, Id<Label>> = HashMap::new();
                        let mut arg_set: HashSet<Id<VariableInfo>> = HashSet::new();
                        for (arg_from, arg_to) in self.fun_defs[f].args.iter().zip(args) {
                            var_map.insert(*arg_from, *arg_to);
                            arg_set.insert(*arg_from);
                        }
                        // variables
                        for (id, info) in self.fun_defs[f].variables.as_ref().to_ref_vec().iter() {
                            if var_map.contains_key(id) {
                                // TODO: merge info?
                            } else {
                                let new_id = fun_def.variables.as_mut().insert((**info).clone());
                                var_map.insert(*id, new_id);
                            }
                        }
                        // labels
                        for (id, info) in self.fun_defs[f].label_table.to_ref_vec().iter() {
                            let new_id = fun_def.label_table.insert((**info).clone());
                            label_map.insert(*id, new_id);
                        }

                        for stmt in &self.fun_defs[f].stmts.stmts {
                            match stmt {
                                IRStmt::Let(id) => {
                                    stmts.push(IRStmt::Let(var_map[id]));
                                }
                                IRStmt::LetWithName(id, s) => {
                                    stmts.push(IRStmt::LetWithName(var_map[id], s.clone()));
                                }
                                IRStmt::MovI(id1, id2) => {
                                    stmts.push(IRStmt::MovI(var_map[id1], var_map[id2]));
                                }
                                IRStmt::MovV(id, v) => {
                                    stmts.push(IRStmt::MovV(var_map[id], v.clone()));
                                }
                                IRStmt::III(inst, id1, id2, id3) => {
                                    stmts.push(IRStmt::III(
                                        *inst,
                                        var_map[id1],
                                        var_map[id2],
                                        var_map[id3],
                                    ));
                                }
                                IRStmt::Release(id) => {
                                    if !arg_set.contains(id) {
                                        stmts.push(IRStmt::Release(var_map[id]));
                                    }
                                }
                                IRStmt::Ret(id) => {
                                    stmts.push(IRStmt::MovI(*idt, var_map[id]));
                                    stmts.push(IRStmt::Release(var_map[id]));
                                }
                                IRStmt::MakeRef(id1, id2) => {
                                    stmts.push(IRStmt::MakeRef(var_map[id1], var_map[id2]));
                                }
                                IRStmt::GetArrayElemAddr(id1, id2, id3) => {
                                    stmts.push(IRStmt::GetArrayElemAddr(
                                        var_map[id1],
                                        var_map[id2],
                                        var_map[id3],
                                    ));
                                }
                                IRStmt::MovRI(id1, id2) => {
                                    stmts.push(IRStmt::MovRI(var_map[id1], var_map[id2]));
                                }
                                IRStmt::X64Inst(inst) => match inst {
                                    X64Inst::MovRV(reg, v) => {
                                        stmts
                                            .push(IRStmt::X64Inst(X64Inst::MovRV(*reg, v.clone())));
                                    }
                                    X64Inst::MovRIR(reg1, id, reg2) => {
                                        stmts.push(IRStmt::X64Inst(X64Inst::MovRIR(
                                            *reg1,
                                            var_map[id],
                                            *reg2,
                                        )));
                                    }
                                    X64Inst::Syscall => {
                                        stmts.push(IRStmt::X64Inst(X64Inst::Syscall));
                                    }
                                    other => {
                                        panic!("other = {:?}", other);
                                    }
                                },
                                IRStmt::SetLabel(label) => {
                                    stmts.push(IRStmt::SetLabel(label_map[label]));
                                }
                                IRStmt::IfElse(id, label1, label2) => {
                                    stmts.push(IRStmt::IfElse(
                                        var_map[id],
                                        label_map[label1],
                                        label_map[label2],
                                    ));
                                }
                                IRStmt::JmpL(label) => {
                                    stmts.push(IRStmt::JmpL(label_map[label]));
                                }
                                IRStmt::ConvertItoI(id1, size2, size1, id2) => {
                                    stmts.push(IRStmt::ConvertItoI(
                                        var_map[id1],
                                        *size2,
                                        *size1,
                                        var_map[id2],
                                    ));
                                }
                                IRStmt::Deref(id1, id2) => {
                                    stmts.push(IRStmt::Deref(var_map[id1], var_map[id2]));
                                }
                                other => {
                                    panic!("other = {:?}", other);
                                } /*
                                  other => {
                                      stmts.push(other.clone());
                                  }
                                  */
                            }
                        }

                        updated = true;
                    }
                    other => {
                        stmts.push(other.clone());
                    }
                }
            }
            eprintln!("-- inlining --");
            for stmt in stmts.stmts.iter() {
                eprintln!("{:?}", stmt);
            }
            eprintln!();
            fun_def.stmts = stmts;
            *self.fun_defs.get_mut(function_item_id).unwrap() = fun_def;
        }
        updated
    }
}
