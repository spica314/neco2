use crate::*;

impl IR {
    pub fn infer_types(&mut self) {
        self.infer_types_inner();
        //eprintln!("[1]item_ty = {:?}", self.item_ty);
        for &function_id in &self.functions {
            self.fun_defs
                .get_mut(function_id)
                .unwrap()
                .variables
                .convert_integer_to(Ty::I(64));
            self.fun_defs
                .get_mut(function_id)
                .unwrap()
                .variables
                .convert_float_to(Ty::F(11, 52));
        }
        // self.variables.convert_integer_to(Ty::I(64));
        //eprintln!("[2]item_ty = {:?}", self.item_ty);
        self.infer_types_inner();
        //eprintln!("[3]item_ty = {:?}", self.item_ty);
        eprintln!("infered");
        for &function_id in &self.functions {
            eprintln!(
                "{:?} : {:?}",
                function_id, self.fun_defs[function_id].variables
            );
        }
    }
    fn infer_types_inner(&mut self) {
        for &function_item_id in &self.functions {
            let mut fun_def = self.fun_defs[function_item_id].clone();
            loop {
                eprintln!("{:?}", fun_def.variables);
                let mut updated = false;
                for inst in &fun_def.stmts.stmts {
                    eprintln!("{:?}", inst);
                    match inst {
                        IRStmt::III(inst, id1, id2, id3) => {
                            if inst.is_arithmetic() {
                                updated |= fun_def.variables.same3(*id1, *id2, *id3).unwrap();
                            } else if inst.is_comparison() {
                                updated |= fun_def.variables.set_type(*id1, Ty::Bool).unwrap();
                                updated |= fun_def.variables.same2(*id2, *id3).unwrap();
                            }
                        }
                        IRStmt::Call(id_t, id_f, args) => {
                            let f_def = &self.fun_defs[id_f];
                            assert_eq!(args.len(), f_def.args.len());
                            for (arg1, arg2) in args.iter().zip(f_def.args.iter()) {
                                updated |= fun_def
                                    .variables
                                    .set_type(*arg1, f_def.variables.as_ref()[arg2].ty.clone())
                                    .unwrap();
                            }
                            updated |= fun_def
                                .variables
                                .set_type(*id_t, f_def.ret_ty.clone())
                                .unwrap();
                        }
                        IRStmt::IfElse(id_c, _, _) => {
                            updated |= fun_def.variables.set_type(*id_c, Ty::Bool).unwrap();
                        }
                        IRStmt::Let(_) => {}
                        IRStmt::Release(_) => {}
                        IRStmt::HeapAlloc(_) => {}
                        IRStmt::HeapFree(_) => {}
                        IRStmt::SetLabel(_) => {}
                        IRStmt::Ret(item_id) => {
                            updated |= fun_def
                                .variables
                                .set_type(*item_id, fun_def.ret_ty.clone())
                                .unwrap();
                        }
                        IRStmt::JmpL(_) => {}
                        IRStmt::MovV(item_id, value) => match value {
                            Value::Integer(_) => {
                                updated |=
                                    fun_def.variables.set_type(*item_id, Ty::Integer).unwrap();
                            }
                            Value::True => {
                                updated |= fun_def.variables.set_type(*item_id, Ty::Bool).unwrap();
                            }
                            Value::False => {
                                updated |= fun_def.variables.set_type(*item_id, Ty::Bool).unwrap();
                            }
                            Value::I(size, _) => {
                                updated |=
                                    fun_def.variables.set_type(*item_id, Ty::I(*size)).unwrap();
                            }
                            Value::Float(_) => {
                                updated |= fun_def.variables.set_type(*item_id, Ty::Float).unwrap();
                            }
                            Value::F((exp_bits, frac_bits), _) => {
                                updated |= fun_def
                                    .variables
                                    .set_type(*item_id, Ty::F(*exp_bits, *frac_bits))
                                    .unwrap();
                            }
                            other => unimplemented!("other = {:?}", other),
                        },
                        IRStmt::MovI(item_id1, item_id2) => {
                            // TODO?: deref? item_id1
                            updated |= fun_def.variables.same2(*item_id1, *item_id2).unwrap();
                        }
                        IRStmt::Deref(item_id1, item_id2) => {
                            updated |= fun_def
                                .variables
                                .a_is_ref_of_b(*item_id2, *item_id1)
                                .unwrap();
                        }
                        IRStmt::Construct(res, variant, args) => {
                            let variant_def = &self.variant_table[variant];
                            for (field, item_id) in variant_def.fields.iter().zip(args.iter()) {
                                updated |= fun_def
                                    .variables
                                    .set_type(*item_id, field.1.clone())
                                    .unwrap();
                            }
                            let data_id = self.data_of_variant[variant];
                            updated |= fun_def.variables.set_type(*res, Ty::Data(data_id)).unwrap();
                        }
                        IRStmt::Destruct(res, variant, from) => {
                            let variant_def = &self.variant_table[variant];
                            for (field, item_id) in variant_def.fields.iter().zip(res.iter()) {
                                updated |= fun_def
                                    .variables
                                    .set_type(*item_id, field.1.clone())
                                    .unwrap();
                            }
                            let data_id = self.data_of_variant[variant];
                            updated |= fun_def
                                .variables
                                .set_type(*from, Ty::Data(data_id))
                                .unwrap();
                        }
                        IRStmt::MatchJmp(_, _) => {
                            // maybe do nothing
                        }
                        IRStmt::X64Inst(inst) => match inst {
                            X64Inst::MovIRV(item_id, _, value) => {
                                updated |=
                                    fun_def.variables.set_type(*item_id, value.ty()).unwrap();
                            }
                            X64Inst::MovRV(_, _) => {}
                            X64Inst::MovRIR(_, _, _) => {}
                            X64Inst::Syscall => {}
                            X64Inst::MovsdRIR(_, _, _) => {}
                            X64Inst::MovssRIR(_, _, _) => {}
                            X64Inst::MovIRR(_, _, _) => {}
                            X64Inst::Cvttsd2si(_, _) => {}
                            X64Inst::Cvttss2si(_, _) => {}
                            inst => panic!("{:?}", inst),
                        },
                        IRStmt::MakeRef(id1, id2) => {
                            updated |= fun_def.variables.a_is_ref_of_b(*id1, *id2).unwrap();
                        }
                        IRStmt::MovRI(id1, id2) => {
                            updated |= fun_def.variables.a_is_ref_of_b(*id1, *id2).unwrap();
                        }
                        IRStmt::GetArrayElemAddr(id1, id2, _id3) => {
                            updated |= fun_def
                                .variables
                                .a_is_ref_of_element_of_ref_array_b(*id1, *id2)
                                .unwrap();
                        }
                        IRStmt::InitCUDA => {}
                        IRStmt::CallCudaKernel(f, (arg, _), _, _) => {
                            let f_def = &self.fun_defs[f];
                            assert_eq!(f_def.args.len(), 1);
                            if let Ty::Ref(inner_ty) = &f_def.variables.as_ref()[f_def.args[0]].ty {
                                updated |= fun_def
                                    .variables
                                    .set_type(*arg, inner_ty.as_ref().clone())
                                    .unwrap();
                            } else {
                                panic!();
                            }
                        }
                        IRStmt::ConvertFtoI(id1, (exp_bits, frac_bits), int_bits, id2) => {
                            updated |= fun_def.variables.set_type(*id1, Ty::I(*int_bits)).unwrap();
                            updated |= fun_def
                                .variables
                                .set_type(*id2, Ty::F(*exp_bits, *frac_bits))
                                .unwrap();
                        }
                        IRStmt::ConvertItoF(id1, int_bits, (exp_bits, frac_bits), id2) => {
                            updated |= fun_def
                                .variables
                                .set_type(*id1, Ty::F(*exp_bits, *frac_bits))
                                .unwrap();
                            updated |= fun_def.variables.set_type(*id2, Ty::I(*int_bits)).unwrap();
                        }
                        IRStmt::ConvertItoI(id1, id2_bits, id1_bits, id2) => {
                            updated |= fun_def.variables.set_type(*id1, Ty::I(*id1_bits)).unwrap();
                            updated |= fun_def.variables.set_type(*id2, Ty::I(*id2_bits)).unwrap();
                        }
                        IRStmt::CallCudaKernelByName(f, arg, _, _) => {
                            panic!("CallCudaKernelByName in infer_types");
                        }
                        IRStmt::EnterScope => {
                            panic!("EnterScope in infer_types");
                        }
                        IRStmt::LeaveScope => {
                            panic!("LeaveScope in infer_types");
                        }
                        IRStmt::HeapAllocByName(_) => {
                            panic!("HeapAllocByName in infer_types");
                        }
                        IRStmt::LetWithName(_, _) => {
                            panic!("LetWithName in infer_types");
                        }
                        IRStmt::GetVariableByName(_, _) => {
                            panic!("GetVariableByName in infer_types");
                        }
                        IRStmt::GetReferenceByName(_, _) => {
                            panic!("GetReferenceByName in infer_types");
                        }
                        IRStmt::CallByName(_, _, _) => {
                            panic!("CallByName in infer_types");
                        }
                        IRStmt::ConstructByName(_, _, _) => {
                            panic!("ConstructByName in infer_types");
                        }
                        IRStmt::DestructByName(_, _, _) => {
                            panic!("DestructByName in infer_types");
                        }
                        IRStmt::MatchJmpByName(_, _) => {
                            panic!("MatchJmpByName in infer_types");
                        }
                        IRStmt::PtxInst(inst) => {
                            // TODO: do something?
                            match inst {
                                PtxInst::MovISpecial(ptx_ty, id, _) => {
                                    if ptx_ty == &PtxTy::S32 {
                                        updated |=
                                            fun_def.variables.set_type(*id, Ty::I(32)).unwrap();
                                    } else {
                                        panic!();
                                    }
                                }
                                other => {}
                            }
                        }
                    }
                }
                if !updated {
                    break;
                }
            }
            *self.fun_defs.get_mut(function_item_id).unwrap() = fun_def;
        }
        // eprintln!("item_ty = {:?}", self.item_ty);
    }
}
