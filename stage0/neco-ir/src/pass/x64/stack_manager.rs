use crate::*;

#[derive(Debug)]
pub struct StackManager {
    local_addr_map: HashMap<Id<VariableInfo>, i64>,
    allocated_size_map: HashMap<Id<VariableInfo>, i64>,
    next_local_addr: i64,
    next_arg_addr: i64,
    local_allocated_ids_stack: Vec<Id<VariableInfo>>,
}

impl StackManager {
    pub fn new() -> StackManager {
        StackManager {
            local_addr_map: HashMap::new(),
            allocated_size_map: HashMap::new(),
            next_local_addr: 0,
            next_arg_addr: 16,
            local_allocated_ids_stack: vec![],
        }
    }
    pub fn record_args(&mut self, args: &[(Id<VariableInfo>, i64)]) {
        assert!(self.next_arg_addr == 16);
        for &(id, size) in args.iter().rev() {
            let size = std::cmp::max(size, 8);
            while size != 0 && self.next_arg_addr % size != 0 {
                self.next_arg_addr += 1;
            }
            self.local_addr_map.insert(id, self.next_arg_addr);
            self.next_arg_addr += size;
        }
    }
    pub fn record_args_and_ret(
        &mut self,
        args: &[(Id<VariableInfo>, i64)],
        ret: (Id<VariableInfo>, i64),
    ) {
        assert!(self.next_arg_addr == 16);
        for &(id, size) in args.iter().rev() {
            let size = std::cmp::max(size, 8);
            while size != 0 && self.next_arg_addr % size != 0 {
                self.next_arg_addr += 1;
            }
            self.local_addr_map.insert(id, self.next_arg_addr);
            self.next_arg_addr += size;
        }
        {
            let (id, size) = ret;
            let size = std::cmp::max(size, 8);
            while size != 0 && self.next_arg_addr % size != 0 {
                self.next_arg_addr += 1;
            }
            self.local_addr_map.insert(id, self.next_arg_addr);
        }
    }
    pub fn record_local_and_get_allocation_size(&mut self, id: Id<VariableInfo>, size: i64) -> i64 {
        let size = std::cmp::max(size, 8);
        let old = self.next_local_addr;
        self.next_local_addr -= size;
        while size != 0 && self.next_local_addr % size != 0 {
            self.next_local_addr -= 1;
        }
        self.local_addr_map.insert(id, self.next_local_addr);
        let allocated_size = old - self.next_local_addr;
        self.allocated_size_map.insert(id, allocated_size);
        self.local_allocated_ids_stack.push(id);
        allocated_size
    }
    pub fn get_local_addr(&mut self, id: Id<VariableInfo>) -> i64 {
        *self.local_addr_map.get(&id).unwrap()
    }
    pub fn release_and_get_allocated_size(&mut self, id: Id<VariableInfo>) -> i64 {
        let top_id = self.local_allocated_ids_stack.pop().unwrap();
        assert_eq!(top_id, id);
        let res = *self.allocated_size_map.get(&id).unwrap();
        self.next_local_addr += res;
        res
    }
    pub fn get_next_local_addr(&self) -> i64 {
        self.next_local_addr
    }
}
