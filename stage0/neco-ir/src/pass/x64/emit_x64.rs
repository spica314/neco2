use crate::*;

impl IR {
    /// x64のアセンブリを生成
    pub fn emit_x64(&self) -> String {
        let mut res = String::new();
        res.push_str("    .intel_syntax noprefix\n");

        // C Runtime を使わない場合は _start を作成
        if !self.config.use_crt {
            res.push_str("    .globl _start\n");
            res.push_str("_start:\n");
            res.push_str("    pushfq\n");
            res.push_str("    pop rax\n");
            res.push_str("    mov rdi, 0x0000000000040000\n");
            res.push_str("    or rax, rdi\n");
            res.push_str("    push rax\n");
            res.push_str("    popfq\n");
            res.push_str("    call main\n");
        }

        // variables for CUDA
        {
            let s = r#"
    .text
    .globl	__cu_device
    .bss
    .align 4
    .type	__cu_device, @object
    .size	__cu_device, 4
__cu_device:
    .zero	4
    .globl	__cu_context
    .align 8
    .type	__cu_context, @object
    .size	__cu_context, 8
__cu_context:
    .zero	8
    .globl	__cu_module
    .align 8
    .type	__cu_module, @object
    .size	__cu_module, 8
__cu_module:
    .zero	8
    .globl	__cu_function
    .align 8
    .type	__cu_function, @object
    .size	__cu_function, 8
__cu_function:
    .zero	8
    .globl	__cu_device_ptr
    .align 8
    .type	__cu_device_ptr, @object
    .size	__cu_device_ptr, 8
__cu_device_ptr:
    .zero	8
"#;
            res.push_str(s);
        }

        // consts
        res.push_str("    .section .rodata\n");
        let escape = |s: &str| {
            let mut res = String::new();
            for c in s.chars() {
                if c == '\n' {
                    res.push_str("\\n");
                } else {
                    res.push(c);
                }
            }
            res
        };
        for (id, value) in self.consts.iter() {
            match value {
                ConstValue::ConstString(s) => {
                    res.push_str(&format!(".LC{}:\n", id));
                    res.push_str(&format!("    .string \"{}\"\n", escape(s)));
                }
            }
        }
        for item_id in &self.functions {
            res.push_str(&format!(".FNAME{}:\n", item_id.id()));
            res.push_str(&format!(
                "    .string \"{}\"\n",
                escape(&self.fun_defs[item_id].name)
            ));
        }

        res.push_str("    .text\n");
        for item_id in &self.functions {
            let fun_def = &self.fun_defs[item_id];
            if fun_def.inline {
                continue;
            }
            if !(fun_def.target_arch == None || fun_def.target_arch == Some(TargetArch::X64)) {
                continue;
            }
            res.push_str(&format!("    .globl {}\n", fun_def.name));
            res.push_str(&format!("{}:\n", fun_def.name));

            // C Runtimeを使う場合はアラインメントをチェックするフラグをmainの先頭で立てる
            if self.config.use_crt && fun_def.name == "main" {
                res.push_str("    pushfq\n");
                res.push_str("    pop rax\n");
                res.push_str("    mov rdi, 0x0000000000040000\n");
                res.push_str("    or rax, rdi\n");
                res.push_str("    push rax\n");
                res.push_str("    popfq\n");
            }

            for stmt in &fun_def.stmts.stmts {
                match stmt {
                    IRStmt::SetLabel(id) => {
                        res.push_str(&format!(".{}_{}:", fun_def.name, id.id()));
                    }
                    IRStmt::InitCUDA => {
                        res.push_str("    pushfq\n");
                        res.push_str("    pop rax\n");
                        res.push_str("    mov rdi, 0x0000000000040000\n");
                        res.push_str("    xor rax, rdi\n");
                        res.push_str("    push rax\n");
                        res.push_str("    popfq\n");
                        let s = r#"
    # cuInit(0)
    mov     edi, 0
    call    cuInit@PLT
    # cuDeviceGet(&cu_device, 0)
    mov	    esi, 0
    lea	    rdi, __cu_device[rip]
    call    cuDeviceGet@PLT
    # cuCtxCreate_v2(&cu_context, 0, cu_device)
    mov	    eax, DWORD PTR __cu_device[rip]
    mov	    edx, eax
    mov	    esi, 0
    lea	    rdi, __cu_context[rip]
    call    cuCtxCreate_v2@PLT
                        "#;
                        res.push_str(s);
                        res.push_str("    pushfq\n");
                        res.push_str("    pop rax\n");
                        res.push_str("    mov rdi, 0x0000000000040000\n");
                        res.push_str("    or rax, rdi\n");
                        res.push_str("    push rax\n");
                        res.push_str("    popfq\n");
                    }
                    IRStmt::CallCudaKernel(id_f, (id_arg, offset), grid, block) => {
                        let var = fun_def.variables.get_ty(*id_arg).unwrap();
                        let memsize = self.memsize(var);
                        res.push_str("    pushfq\n");
                        res.push_str("    pop rax\n");
                        res.push_str("    mov rdi, 0x0000000000040000\n");
                        res.push_str("    xor rax, rdi\n");
                        res.push_str("    push rax\n");
                        res.push_str("    popfq\n");
                        let s = format!(
                            r#"
    # cuModuleLoadData(&cu_module, "ptx data")
    lea     rsi, __kernel[rip]
    lea     rdi, __cu_module[rip]
    call    cuModuleLoadData@PLT
                        "#
                        );
                        res.push_str(&s);
                        let s = format!(
                            r#"
    # cuModuleGetFunction(&cu_function, cu_module, "f")
    lea     rdi, __cu_function[rip]
    mov     rsi, QWORD PTR __cu_module[rip]
    lea     rdx, .FNAME{}[rip]
    call    cuModuleGetFunction@PLT
                        "#,
                            id_f.id()
                        );
                        res.push_str(&s);

                        let s = format!(
                            r#"
    # cuMemAlloc_v2(&cu_deviceptr, memsize)
    lea     rdi, __cu_device_ptr[rip]
    mov     esi, {}
    call    cuMemAlloc_v2@PLT
                        "#,
                            memsize
                        );
                        res.push_str(&s);

                        let s = format!(
                            r#"
    # cuMemcpyHtoD_v2(device_ptr, &xs, memsize)
    mov     rdi, QWORD PTR __cu_device_ptr[rip]
    lea     rsi, {}[rbp]
    mov     edx, {}
    call    cuMemcpyHtoD_v2@PLT
                        "#,
                            offset, memsize
                        );
                        res.push_str(&s);

                        let s = format!(
                            r#"
    # void *args[] = {{&device_ptr}}; // (escape)
    # cuLaunchKernel(cu_function, gx, gy, gz, bx, by, bz, 0, 0, args, 0)
    lea     r10, __cu_device_ptr[rip]
    push    r10
    mov     r11, rsp
    push    0
    push    r11
    push    0
    push    0
    push    {}
    mov     r9d, {}
    mov     r8d, {}
    mov     ecx, {}
    mov     edx, {}
    mov     esi, {}
    mov     rdi, QWORD PTR __cu_function[rip]
    call    cuLaunchKernel@PLT
                        "#,
                            block.2, block.1, block.0, grid.2, grid.1, grid.0
                        );
                        res.push_str(&s);

                        let s = format!(
                            r#"
    # cuMemcpyDtoH_v2(&xs, device_ptr, memsize)
    lea     rdi, {}[rbp]
    mov     rsi, QWORD PTR __cu_device_ptr[rip]
    mov     edx, {}
    call    cuMemcpyDtoH_v2@PLT
    add     rsp, 48
                        "#,
                            offset, memsize
                        );
                        res.push_str(&s);
                        res.push_str("    pushfq\n");
                        res.push_str("    pop rax\n");
                        res.push_str("    mov rdi, 0x0000000000040000\n");
                        res.push_str("    or rax, rdi\n");
                        res.push_str("    push rax\n");
                        res.push_str("    popfq\n");
                    }
                    IRStmt::X64Inst(inst) => match inst {
                        X64Inst::Push(reg) => {
                            res.push_str(&format!("    push {}", reg.as_str()));
                        }
                        X64Inst::MovRR(reg1, reg2) => {
                            res.push_str(&format!("    mov {}, {}", reg1.as_str(), reg2.as_str()));
                        }
                        X64Inst::MovORV(offset, reg1, value) => {
                            let size = match value {
                                Value::Integer(_) => panic!("inst = {:?}", inst),
                                Value::I(size, _v) => match size {
                                    8 => "BYTE PTR ",
                                    32 => "DWORD PTR ",
                                    64 => "QWORD PTR ",
                                    other => unimplemented!("size = {}", other),
                                },
                                Value::Float(_) => {
                                    // TODO FIX?
                                    "QWORD PTR "
                                }
                                Value::F(_, _) => {
                                    // TODO FIX?
                                    "QWORD PTR "
                                }
                                Value::True => "BYTE PTR ",
                                Value::False => "BYTE PTR ",
                                Value::Label(_l) => unimplemented!(),
                            };
                            res.push_str(&format!(
                                "    mov {} {}[{}], {}",
                                size,
                                offset,
                                reg1.as_str(),
                                &value.to_x64_string()
                            ))
                        }
                        X64Inst::MovORR(offset, reg1, reg2) => {
                            res.push_str(&format!(
                                "    mov {}[{}], {}",
                                offset,
                                reg1.as_str(),
                                reg2.as_str()
                            ));
                        }
                        X64Inst::MovsdORR(offset, reg1, reg2) => {
                            res.push_str(&format!(
                                "    movsd {}[{}], {}",
                                offset,
                                reg1.as_str(),
                                reg2.as_str()
                            ));
                        }
                        X64Inst::MovssORR(offset, reg1, reg2) => {
                            res.push_str(&format!(
                                "    movss {}[{}], {}",
                                offset,
                                reg1.as_str(),
                                reg2.as_str()
                            ));
                        }
                        X64Inst::MovROR(reg1, offset, reg2) => {
                            res.push_str(&format!(
                                "    mov {}, {}[{}]",
                                reg1.as_str(),
                                offset,
                                reg2.as_str()
                            ));
                        }
                        X64Inst::MovsdROR(reg1, offset, reg2) => {
                            res.push_str(&format!(
                                "    movsd {}, {}[{}]",
                                reg1.as_str(),
                                offset,
                                reg2.as_str()
                            ));
                        }
                        X64Inst::MovssROR(reg1, offset, reg2) => {
                            res.push_str(&format!(
                                "    movss {}, {}[{}]",
                                reg1.as_str(),
                                offset,
                                reg2.as_str()
                            ));
                        }
                        X64Inst::AddRV(reg1, v) => {
                            res.push_str(&format!("    add {}, {}", reg1.as_str(), v));
                        }
                        X64Inst::AddRR(reg1, reg2) => {
                            res.push_str(&format!("    add {}, {}", reg1.as_str(), reg2.as_str()));
                        }
                        X64Inst::AddsdRR(reg1, reg2) => {
                            res.push_str(&format!(
                                "    addsd {}, {}",
                                reg1.as_str(),
                                reg2.as_str()
                            ));
                        }
                        X64Inst::AddssRR(reg1, reg2) => {
                            res.push_str(&format!(
                                "    addss {}, {}",
                                reg1.as_str(),
                                reg2.as_str()
                            ));
                        }
                        X64Inst::SubRR(reg1, reg2) => {
                            res.push_str(&format!("    sub {}, {}", reg1.as_str(), reg2.as_str()));
                        }
                        X64Inst::SubsdRR(reg1, reg2) => {
                            res.push_str(&format!(
                                "    subsd {}, {}",
                                reg1.as_str(),
                                reg2.as_str()
                            ));
                        }
                        X64Inst::SubssRR(reg1, reg2) => {
                            res.push_str(&format!(
                                "    subss {}, {}",
                                reg1.as_str(),
                                reg2.as_str()
                            ));
                        }
                        X64Inst::ImulRR(reg1, reg2) => {
                            res.push_str(&format!("    imul {}, {}", reg1.as_str(), reg2.as_str()));
                        }
                        X64Inst::MulsdRR(reg1, reg2) => {
                            res.push_str(&format!(
                                "    mulsd {}, {}",
                                reg1.as_str(),
                                reg2.as_str()
                            ));
                        }
                        X64Inst::MulssRR(reg1, reg2) => {
                            res.push_str(&format!(
                                "    mulss {}, {}",
                                reg1.as_str(),
                                reg2.as_str()
                            ));
                        }
                        X64Inst::DivsdRR(reg1, reg2) => {
                            res.push_str(&format!(
                                "    divsd {}, {}",
                                reg1.as_str(),
                                reg2.as_str()
                            ));
                        }
                        X64Inst::DivssRR(reg1, reg2) => {
                            res.push_str(&format!(
                                "    divss {}, {}",
                                reg1.as_str(),
                                reg2.as_str()
                            ));
                        }
                        X64Inst::SubRV(reg1, v) => {
                            res.push_str(&format!("    sub {}, {}", reg1.as_str(), v));
                        }
                        X64Inst::Cvttsd2si(reg1, reg2) => {
                            res.push_str(&format!(
                                "    cvttsd2si {}, {}",
                                reg1.as_str(),
                                reg2.as_str()
                            ));
                        }
                        X64Inst::Cvttss2si(reg1, reg2) => {
                            res.push_str(&format!(
                                "    cvttss2si {}, {}",
                                reg1.as_str(),
                                reg2.as_str()
                            ));
                        }
                        X64Inst::LeaROR(reg1, offset, reg2) => {
                            res.push_str(&format!(
                                "    lea {}, {}[{}]",
                                reg1.as_str(),
                                offset,
                                reg2.as_str()
                            ));
                        }
                        X64Inst::CmpRV(reg1, v) => {
                            res.push_str(&format!(
                                "    cmp {}, {}",
                                reg1.as_str(),
                                &v.to_x64_string()
                            ));
                        }
                        X64Inst::CmpRR(reg1, reg2) => {
                            res.push_str(&format!("    cmp {}, {}", reg1.as_str(), reg2.as_str()));
                        }
                        X64Inst::JeL(id) => {
                            res.push_str(&format!("    je .{}_{}", fun_def.name, id.id()));
                        }
                        X64Inst::JneL(id) => {
                            res.push_str(&format!("    jne .{}_{}", fun_def.name, id.id()));
                        }
                        X64Inst::JlL(id) => {
                            res.push_str(&format!("    jl .{}_{}", fun_def.name, id.id()));
                        }
                        X64Inst::JleL(id) => {
                            res.push_str(&format!("    jle .{}_{}", fun_def.name, id.id()));
                        }
                        X64Inst::JgL(id) => {
                            res.push_str(&format!("    jg .{}_{}", fun_def.name, id.id()));
                        }
                        X64Inst::JgeL(id) => {
                            res.push_str(&format!("    jge .{}_{}", fun_def.name, id.id()));
                        }
                        X64Inst::JmpL(id) => {
                            res.push_str(&format!("    jmp .{}_{}", fun_def.name, id.id()));
                        }
                        X64Inst::Cqo => {
                            res.push_str("    cqo");
                        }
                        X64Inst::Idiv(reg) => {
                            res.push_str(&format!("    idiv {}", reg.as_str()));
                        }
                        X64Inst::Call(id) => {
                            let name = &self.fun_defs[*id].name;
                            res.push_str(&format!("    call {}", name));
                        }
                        X64Inst::Movsx(reg1, reg2) => {
                            res.push_str(&format!(
                                "    movsx {}, {}",
                                reg1.as_str(),
                                reg2.as_str()
                            ));
                        }
                        X64Inst::Pop(reg) => {
                            res.push_str(&format!("    pop {}", reg.as_str()));
                        }
                        X64Inst::Ret => {
                            res.push_str("    ret");
                        }
                        X64Inst::MovRV(reg, v) => {
                            res.push_str(&format!(
                                "    mov {}, {}",
                                reg.as_str(),
                                v.to_x64_string()
                            ));
                        }
                        X64Inst::Syscall => {
                            res.push_str("    syscall");
                        }
                        other => panic!("{:?}", other),
                    },
                    other => panic!("{:?}", other),
                }
                res.push('\n');
            }
        }
        res
    }
}
