use super::stack_manager::*;
use crate::*;

macro_rules! new_label {
    ($ir:ident) => {{
        $ir.label_table.insert(Label {})
    }};
}

impl IR {
    /// generalのIRStmtをx64固有のIRStmtに変換
    pub fn convert_to_x64(&mut self) {
        for &fun_id in &self.functions {
            let mut stack_manager = StackManager::new();
            let mut fun_def = self.fun_defs[fun_id].clone();

            if !(fun_def.target_arch == None || fun_def.target_arch == Some(TargetArch::X64)) {
                continue;
            }

            //self.scope = Some(Scope::new());
            let mut stmts = IRStmts::new();

            // prologue
            stmts.push(IRStmt::X64Inst(X64Inst::Push(X64Reg::RBP)));
            stmts.push(IRStmt::X64Inst(X64Inst::MovRR(X64Reg::RBP, X64Reg::RSP)));

            let offset_of_ref_to_return = {
                let mut arg_offset = 16;
                for &arg_id in fun_def.args.iter().rev() {
                    let ty = &fun_def.variables.as_ref().get(arg_id).unwrap().ty;
                    let memsize = self.memsize(&ty);
                    let m = stack_manager.record_local_and_get_allocation_size(arg_id, memsize);
                    stmts.push(IRStmt::X64Inst(X64Inst::SubRV(X64Reg::RSP, m)));
                    let real_offset = stack_manager.get_local_addr(arg_id);

                    stmts.push(IRStmt::X64Inst(X64Inst::MovROR(
                        X64Reg::RSI,
                        arg_offset,
                        X64Reg::RBP,
                    )));
                    stmts.push(IRStmt::X64Inst(X64Inst::LeaROR(
                        X64Reg::RDI,
                        real_offset,
                        X64Reg::RBP,
                    )));

                    // TODO: more short or loop
                    for i in 0..memsize {
                        stmts.push(IRStmt::X64Inst(X64Inst::MovROR(X64Reg::AL, i, X64Reg::RSI)));
                        stmts.push(IRStmt::X64Inst(X64Inst::MovORR(i, X64Reg::RDI, X64Reg::AL)));
                    }

                    arg_offset += 8;
                }
                (16 + fun_def.args.len() * 8) as i64
            };
            eprintln!("offset_of_ref_to_return = {}", offset_of_ref_to_return);

            for stmt in &fun_def.stmts.stmts {
                match stmt {
                    IRStmt::Let(id) => {
                        eprintln!("id = {:?}", id);
                        let ty = &fun_def.variables.as_ref().get(*id).unwrap().ty;
                        let memsize = self.memsize(&ty);
                        let size = stack_manager.record_local_and_get_allocation_size(*id, memsize);
                        stmts.push(IRStmt::X64Inst(X64Inst::SubRV(X64Reg::RSP, size)));
                    }
                    _ => {}
                }
            }
            eprintln!("next = {}", stack_manager.get_next_local_addr());
            if stack_manager.get_next_local_addr() % 16 != 0 {
                assert!((stack_manager.get_next_local_addr() - 8) % 16 == 0);
                eprintln!("fix");
                stmts.push(IRStmt::X64Inst(X64Inst::SubRV(X64Reg::RSP, 8)));
            }

            for stmt in fun_def.stmts.clone().stmts {
                let context = Context {
                    ir: &self,
                    fun_def: &mut fun_def,
                    stack_manager: &mut stack_manager,
                    stmts: &mut stmts,
                    offset_of_ref_to_return,
                };
                match stmt {
                    IRStmt::Let(_) => {}
                    IRStmt::HeapAlloc(id) => {
                        convert_to_x64_heap_alloc(context, id);
                    }
                    IRStmt::HeapFree(_id) => {
                        // do nothing now
                    }
                    IRStmt::III(inst, id1, id2, id3) => {
                        convert_to_x64_iii(context, inst, id1, id2, id3);
                    }
                    IRStmt::Ret(id) => {
                        convert_to_x64_ret(context, id);
                    }
                    IRStmt::Deref(id1, id2) => {
                        convert_to_x64_deref(context, id1, id2);
                    }
                    IRStmt::Call(idt, idf, args) => {
                        convert_to_x64_call(context, idt, idf, args);
                    }
                    IRStmt::IfElse(id1, label_t, label_f) => {
                        convert_to_x64_if_else(context, id1, label_t, label_f);
                    }
                    IRStmt::JmpL(id1) => {
                        convert_to_x64_jmp_l(context, id1);
                    }
                    IRStmt::Release(id) => {
                        convert_to_x64_release(context, id);
                    }
                    IRStmt::SetLabel(id) => {
                        convert_to_x64_set_label(context, id);
                    }
                    IRStmt::MovV(item_id, value) => {
                        convert_to_x64_mov_v(context, item_id, value);
                    }
                    IRStmt::MovI(id1, id2) => {
                        convert_to_x64_mov_i(context, id1, id2);
                    }
                    IRStmt::MovRI(id1, id2) => {
                        convert_to_x64_mov_ri(context, id1, id2);
                    }
                    IRStmt::Construct(to, variant_id, args) => {
                        convert_to_x64_construct(context, to, variant_id, args);
                    }
                    IRStmt::Destruct(to, variant, from) => {
                        convert_to_x64_destruct(context, to, variant, from);
                    }
                    IRStmt::MatchJmp(item, pairs_of_variant_and_label) => {
                        convert_to_x64_match_jmp(context, item, pairs_of_variant_and_label);
                    }
                    IRStmt::X64Inst(inst) => {
                        convert_to_x64_x64_inst(context, inst);
                    }
                    IRStmt::MakeRef(id1, id2) => {
                        convert_to_x64_make_ref(context, id1, id2);
                    }
                    IRStmt::GetArrayElemAddr(id1, id2, id3) => {
                        convert_to_x64_get_array_elem_addr(context, id1, id2, id3);
                    }
                    IRStmt::InitCUDA => {
                        convert_to_x64_init_cuda(context);
                    }
                    IRStmt::CallCudaKernel(f, arg, grid, block) => {
                        convert_to_x64_call_cuda_kernel(context, f, arg, grid, block);
                    }
                    IRStmt::ConvertFtoI(id1, (exp_bits, frac_bits), int_bits, id2) => {
                        convert_to_x64_convert_f_to_i(
                            context,
                            id1,
                            (exp_bits, frac_bits),
                            int_bits,
                            id2,
                        );
                    }
                    IRStmt::ConvertItoF(id1, int_bits, (exp_bits, frac_bits), id2) => {
                        convert_to_x64_convert_i_to_f(
                            context,
                            id1,
                            int_bits,
                            (exp_bits, frac_bits),
                            id2,
                        );
                    }
                    IRStmt::ConvertItoI(id1, id2_bits, id1_bits, id2) => {
                        convert_to_x64_convert_i_to_i(context, id1, id2_bits, id1_bits, id2);
                    }
                    IRStmt::CallCudaKernelByName(_, _, _, _) => {
                        panic!("IRStmt::CallCudaKernelByName in convert_to_x64");
                    }
                    IRStmt::EnterScope => {
                        panic!("IRStmt::EnterScope in convert_to_x64");
                    }
                    IRStmt::LeaveScope => {
                        panic!("IRStmt::LeaveScope in convert_to_x64");
                    }
                    IRStmt::LetWithName(_, _) => {
                        panic!("IRStmt::LetWithName in convert_to_x64");
                    }
                    IRStmt::GetVariableByName(_, _) => {
                        panic!("IRStmt::GetVariableByName in convert_to_x64");
                    }
                    IRStmt::GetReferenceByName(_, _) => {
                        panic!("IRStmt::GetVariableByName in convert_to_x64");
                    }
                    IRStmt::CallByName(_, _, _) => {
                        panic!("IRStmt::CallByName in convert_to_x64");
                    }
                    IRStmt::ConstructByName(_, _, _) => {
                        panic!("ConstructByName in convert_to_x64");
                    }
                    IRStmt::DestructByName(_, _, _) => {
                        panic!("DestructByName in convert_to_x64");
                    }
                    IRStmt::MatchJmpByName(_, _) => {
                        panic!("MatchJmpByName in convert_to_x64");
                    }
                    IRStmt::HeapAllocByName(_) => {
                        panic!("HeapAllocByName in convert_to_x64");
                    }
                    IRStmt::PtxInst(_) => {
                        panic!("PtxInst in convert_to_x64");
                    }
                }
            }
            stmts.push(IRStmt::X64Inst(X64Inst::MovRR(X64Reg::RSP, X64Reg::RBP)));
            stmts.push(IRStmt::X64Inst(X64Inst::Pop(X64Reg::RBP)));
            stmts.push(IRStmt::X64Inst(X64Inst::Ret));

            eprintln!("stack_manager = {:?}", stack_manager);

            //self.scope = None;
            fun_def.stmts = stmts;
            *self.fun_defs.get_mut(fun_id).unwrap() = fun_def;
            // self.fun_defs.get_mut(fun_id).unwrap().stmts = stmts;
        }
    }
}

struct Context<'a> {
    ir: &'a IR,
    fun_def: &'a mut Function,
    stack_manager: &'a mut StackManager,
    stmts: &'a mut IRStmts,
    offset_of_ref_to_return: i64,
}

fn convert_to_x64_iii(
    context: Context,
    inst: IRInst,
    id1: Id<VariableInfo>,
    id2: Id<VariableInfo>,
    id3: Id<VariableInfo>,
) {
    let Context {
        fun_def,
        stack_manager,
        stmts,
        ..
    } = context;
    if inst.is_comparison() {
        /*
                mov rax, -?[rbp]
                mov rdi, -?[rbp]
                cmp rax, v
                je/jl/jl/jle/jg/jge .label1
                jmp .label2
            .label1:
                mov ?[rbp], byte 1
                jmp .label3
            .label2:
                mov ?[rbp], byte 0
            .label3:
        */
        let label1_id = new_label!(fun_def);
        let label2_id = new_label!(fun_def);
        let label3_id = new_label!(fun_def);
        let id1_offset = stack_manager.get_local_addr(id1);
        let id2_offset = stack_manager.get_local_addr(id2);
        let id3_offset = stack_manager.get_local_addr(id3);
        stmts.push(IRStmt::X64Inst(X64Inst::MovROR(
            X64Reg::RAX,
            id2_offset,
            X64Reg::RBP,
        )));
        stmts.push(IRStmt::X64Inst(X64Inst::MovROR(
            X64Reg::RDI,
            id3_offset,
            X64Reg::RBP,
        )));
        stmts.push(IRStmt::X64Inst(X64Inst::CmpRR(X64Reg::RAX, X64Reg::RDI)));
        match inst {
            IRInst::Eq => {
                stmts.push(IRStmt::X64Inst(X64Inst::JeL(label1_id)));
            }
            IRInst::Ne => {
                stmts.push(IRStmt::X64Inst(X64Inst::JneL(label1_id)));
            }
            IRInst::Lt => {
                stmts.push(IRStmt::X64Inst(X64Inst::JlL(label1_id)));
            }
            IRInst::Le => {
                stmts.push(IRStmt::X64Inst(X64Inst::JleL(label1_id)));
            }
            IRInst::Gt => {
                stmts.push(IRStmt::X64Inst(X64Inst::JgL(label1_id)));
            }
            IRInst::Ge => {
                stmts.push(IRStmt::X64Inst(X64Inst::JgeL(label1_id)));
            }
            _ => panic!(),
        }
        stmts.push(IRStmt::X64Inst(X64Inst::JmpL(label2_id)));
        stmts.push(IRStmt::SetLabel(label1_id));
        stmts.push(IRStmt::X64Inst(X64Inst::MovORV(
            id1_offset,
            X64Reg::RBP,
            Value::True,
        )));
        stmts.push(IRStmt::X64Inst(X64Inst::JmpL(label3_id)));
        stmts.push(IRStmt::SetLabel(label2_id));
        stmts.push(IRStmt::X64Inst(X64Inst::MovORV(
            id1_offset,
            X64Reg::RBP,
            Value::False,
        )));
        stmts.push(IRStmt::SetLabel(label3_id));
    } else if inst.is_arithmetic() {
        let ty = fun_def.variables.get_ty(id1).unwrap();
        if ty.is_integer() {
            /*
                mov rax, ?[rbp]
                mov rdi, ?[rbp]
                add/sub/imul/cqo-idiv rax, rdi
                mov ?[rbp], rax
            */
            eprintln!("id1 = {:?}, id2 = {:?}, id3 = {:?}", id1, id2, id3);
            let id1_offset = stack_manager.get_local_addr(id1);
            let id2_offset = stack_manager.get_local_addr(id2);
            let id3_offset = stack_manager.get_local_addr(id3);
            stmts.push(IRStmt::X64Inst(X64Inst::MovROR(
                X64Reg::RAX,
                id2_offset,
                X64Reg::RBP,
            )));
            stmts.push(IRStmt::X64Inst(X64Inst::MovROR(
                X64Reg::RDI,
                id3_offset,
                X64Reg::RBP,
            )));
            match inst {
                IRInst::Add => {
                    stmts.push(IRStmt::X64Inst(X64Inst::AddRR(X64Reg::RAX, X64Reg::RDI)));
                }
                IRInst::Sub => {
                    stmts.push(IRStmt::X64Inst(X64Inst::SubRR(X64Reg::RAX, X64Reg::RDI)));
                }
                IRInst::Mul => {
                    stmts.push(IRStmt::X64Inst(X64Inst::ImulRR(X64Reg::RAX, X64Reg::RDI)));
                }
                IRInst::Div => {
                    stmts.push(IRStmt::X64Inst(X64Inst::Cqo));
                    stmts.push(IRStmt::X64Inst(X64Inst::Idiv(X64Reg::RDI)));
                }
                IRInst::Mod => {
                    stmts.push(IRStmt::X64Inst(X64Inst::Cqo));
                    stmts.push(IRStmt::X64Inst(X64Inst::Idiv(X64Reg::RDI)));
                    stmts.push(IRStmt::X64Inst(X64Inst::MovRR(X64Reg::RAX, X64Reg::RDX)));
                }
                _ => panic!(),
            }
            stmts.push(IRStmt::X64Inst(X64Inst::MovORR(
                id1_offset,
                X64Reg::RBP,
                X64Reg::RAX,
            )));
        } else if ty.is_float() {
            if &Ty::F(11, 52) == ty {
                /*
                    movsd __xmm0, ?[rbp]
                    movsd __xmm1, ?[rbp]
                    addsd/subsd/mulsd/divsd __xmm0, __xmm1
                    movsd ?[rbp], __xmm0
                */
                eprintln!("id1 = {:?}, id2 = {:?}, id3 = {:?}", id1, id2, id3);
                let id1_offset = stack_manager.get_local_addr(id1);
                let id2_offset = stack_manager.get_local_addr(id2);
                let id3_offset = stack_manager.get_local_addr(id3);
                stmts.push(IRStmt::X64Inst(X64Inst::MovsdROR(
                    X64Reg::Xmm0,
                    id2_offset,
                    X64Reg::RBP,
                )));
                stmts.push(IRStmt::X64Inst(X64Inst::MovsdROR(
                    X64Reg::Xmm1,
                    id3_offset,
                    X64Reg::RBP,
                )));
                match inst {
                    IRInst::Add => {
                        stmts.push(IRStmt::X64Inst(X64Inst::AddsdRR(
                            X64Reg::Xmm0,
                            X64Reg::Xmm1,
                        )));
                    }
                    IRInst::Sub => {
                        stmts.push(IRStmt::X64Inst(X64Inst::SubsdRR(
                            X64Reg::Xmm0,
                            X64Reg::Xmm1,
                        )));
                    }
                    IRInst::Mul => {
                        stmts.push(IRStmt::X64Inst(X64Inst::MulsdRR(
                            X64Reg::Xmm0,
                            X64Reg::Xmm1,
                        )));
                    }
                    IRInst::Div => {
                        stmts.push(IRStmt::X64Inst(X64Inst::DivsdRR(
                            X64Reg::Xmm0,
                            X64Reg::Xmm1,
                        )));
                    }
                    _ => panic!(),
                }
                stmts.push(IRStmt::X64Inst(X64Inst::MovsdORR(
                    id1_offset,
                    X64Reg::RBP,
                    X64Reg::Xmm0,
                )));
            } else if &Ty::F(8, 23) == ty {
                /*
                    movss __xmm0, ?[rbp]
                    movss __xmm1, ?[rbp]
                    addss/subss/mulss/divss __xmm0, __xmm1
                    movss ?[rbp], __xmm0
                */
                eprintln!("id1 = {:?}, id2 = {:?}, id3 = {:?}", id1, id2, id3);
                let id1_offset = stack_manager.get_local_addr(id1);
                let id2_offset = stack_manager.get_local_addr(id2);
                let id3_offset = stack_manager.get_local_addr(id3);
                stmts.push(IRStmt::X64Inst(X64Inst::MovssROR(
                    X64Reg::Xmm0,
                    id2_offset,
                    X64Reg::RBP,
                )));
                stmts.push(IRStmt::X64Inst(X64Inst::MovssROR(
                    X64Reg::Xmm1,
                    id3_offset,
                    X64Reg::RBP,
                )));
                match inst {
                    IRInst::Add => {
                        stmts.push(IRStmt::X64Inst(X64Inst::AddssRR(
                            X64Reg::Xmm0,
                            X64Reg::Xmm1,
                        )));
                    }
                    IRInst::Sub => {
                        stmts.push(IRStmt::X64Inst(X64Inst::SubssRR(
                            X64Reg::Xmm0,
                            X64Reg::Xmm1,
                        )));
                    }
                    IRInst::Mul => {
                        stmts.push(IRStmt::X64Inst(X64Inst::MulssRR(
                            X64Reg::Xmm0,
                            X64Reg::Xmm1,
                        )));
                    }
                    IRInst::Div => {
                        stmts.push(IRStmt::X64Inst(X64Inst::DivssRR(
                            X64Reg::Xmm0,
                            X64Reg::Xmm1,
                        )));
                    }
                    _ => panic!(),
                }
                stmts.push(IRStmt::X64Inst(X64Inst::MovssORR(
                    id1_offset,
                    X64Reg::RBP,
                    X64Reg::Xmm0,
                )));
            } else {
                panic!();
            }
        } else {
            panic!();
        }
    } else {
        panic!();
    }
}

fn convert_to_x64_heap_alloc(context: Context, id: Id<VariableInfo>) {
    let Context {
        ir,
        fun_def,
        stack_manager,
        stmts,
        ..
    } = context;
    /*
        mov rax, 12
        mov rdi, 0
        syscall
        mov ?[rbp], rax
        mov rdi, rax
        add rdi, (inner_size+15)/16*16
        mov rax, 12
        syscall
    */
    stmts.push(IRStmt::X64Inst(X64Inst::MovRV(
        X64Reg::RAX,
        Value::Integer(12),
    )));
    stmts.push(IRStmt::X64Inst(X64Inst::MovRV(
        X64Reg::RDI,
        Value::Integer(0),
    )));
    stmts.push(IRStmt::X64Inst(X64Inst::Syscall));
    let offset = stack_manager.get_local_addr(id);
    stmts.push(IRStmt::X64Inst(X64Inst::MovORR(
        offset,
        X64Reg::RBP,
        X64Reg::RAX,
    )));
    stmts.push(IRStmt::X64Inst(X64Inst::MovRR(X64Reg::RDI, X64Reg::RAX)));
    let ty = &fun_def.variables.as_ref().get(id).unwrap().ty;
    let alloc_size = match ty {
        Ty::Ref(inner_ty) => {
            let memsize = ir.memsize(inner_ty);
            (memsize + 15) / 16 * 16
        }
        other => panic!("ty = {:?}", other),
    };
    stmts.push(IRStmt::X64Inst(X64Inst::AddRV(X64Reg::RDI, alloc_size)));
    stmts.push(IRStmt::X64Inst(X64Inst::MovRV(
        X64Reg::RAX,
        Value::Integer(12),
    )));
    stmts.push(IRStmt::X64Inst(X64Inst::Syscall));
}

fn convert_to_x64_ret(context: Context, id: Id<VariableInfo>) {
    let Context {
        ir,
        fun_def,
        stack_manager,
        stmts,
        offset_of_ref_to_return,
    } = context;
    match ir.memsize(&fun_def.ret_ty) {
        8 => {
            /*
                mov rax, ?[rbp]
                mov ?[rbp], rax
            */
            let id_offset = stack_manager.get_local_addr(id);
            //let ret_offset = stack_manager.get_local_addr(return_id);
            stmts.push(IRStmt::X64Inst(X64Inst::MovROR(
                X64Reg::RDI,
                offset_of_ref_to_return,
                X64Reg::RBP,
            )));
            stmts.push(IRStmt::X64Inst(X64Inst::MovROR(
                X64Reg::RAX,
                id_offset,
                X64Reg::RBP,
            )));
            stmts.push(IRStmt::X64Inst(X64Inst::MovORR(
                0,
                X64Reg::RDI,
                X64Reg::RAX,
            )));
        }
        0 => {
            // do nothing
        }
        _ => {
            unimplemented!();
        }
    }
    stmts.push(IRStmt::X64Inst(X64Inst::MovRR(X64Reg::RSP, X64Reg::RBP)));
    stmts.push(IRStmt::X64Inst(X64Inst::Pop(X64Reg::RBP)));
    stmts.push(IRStmt::X64Inst(X64Inst::Ret));
}

fn convert_to_x64_deref(context: Context, id1: Id<VariableInfo>, id2: Id<VariableInfo>) {
    let Context {
        ir,
        fun_def,
        stack_manager,
        stmts,
        ..
    } = context;
    /*
        mov rax, ?[rbp]
        mov rax, [rax]
        mov ?[rbp], rax
    */
    let id1_offset = stack_manager.get_local_addr(id1);
    let id2_offset = stack_manager.get_local_addr(id2);
    let ty = fun_def.variables.get_ty(id1).unwrap();
    let memsize = ir.memsize(ty);
    if memsize == 8 {
        stmts.push(IRStmt::X64Inst(X64Inst::MovROR(
            X64Reg::RAX,
            id2_offset,
            X64Reg::RBP,
        )));
        stmts.push(IRStmt::X64Inst(X64Inst::MovROR(
            X64Reg::RAX,
            0,
            X64Reg::RAX,
        )));
        stmts.push(IRStmt::X64Inst(X64Inst::MovORR(
            id1_offset,
            X64Reg::RBP,
            X64Reg::RAX,
        )));
    } else if memsize == 4 {
        stmts.push(IRStmt::X64Inst(X64Inst::MovROR(
            X64Reg::RAX,
            id2_offset,
            X64Reg::RBP,
        )));
        stmts.push(IRStmt::X64Inst(X64Inst::MovROR(
            X64Reg::EAX,
            0,
            X64Reg::RAX,
        )));
        stmts.push(IRStmt::X64Inst(X64Inst::MovORR(
            id1_offset,
            X64Reg::RBP,
            X64Reg::EAX,
        )));
    } else if memsize == 1 {
        stmts.push(IRStmt::X64Inst(X64Inst::MovROR(
            X64Reg::RAX,
            id2_offset,
            X64Reg::RBP,
        )));
        stmts.push(IRStmt::X64Inst(X64Inst::MovROR(X64Reg::AL, 0, X64Reg::RAX)));
        stmts.push(IRStmt::X64Inst(X64Inst::MovORR(
            id1_offset,
            X64Reg::RBP,
            X64Reg::AL,
        )));
    } else {
        unimplemented!("memsize = {}", memsize);
    }
}

fn convert_to_x64_call(
    context: Context,
    idt: Id<VariableInfo>,
    idf: Id<Function>,
    args: Vec<Id<VariableInfo>>,
) {
    let Context {
        stack_manager,
        stmts,
        ..
    } = context;
    // 16byte境界に調整
    if (args.len() + 1) % 2 != 0 {
        stmts.push(IRStmt::X64Inst(X64Inst::SubRV(X64Reg::RSP, 0)));
    }

    // 戻り値のアドレスをpush
    {
        let offset = stack_manager.get_local_addr(idt);
        stmts.push(IRStmt::X64Inst(X64Inst::LeaROR(
            X64Reg::RAX,
            offset,
            X64Reg::RBP,
        )));
        stmts.push(IRStmt::X64Inst(X64Inst::Push(X64Reg::RAX)));
    }

    // 引数のアドレスをpush
    for &arg_id in &args {
        let offset = stack_manager.get_local_addr(arg_id);
        stmts.push(IRStmt::X64Inst(X64Inst::LeaROR(
            X64Reg::RAX,
            offset,
            X64Reg::RBP,
        )));
        stmts.push(IRStmt::X64Inst(X64Inst::Push(X64Reg::RAX)));
    }

    stmts.push(IRStmt::X64Inst(X64Inst::Call(idf)));

    let s = (args.len() + 1 + 1) / 2 * 2 * 8;
    stmts.push(IRStmt::X64Inst(X64Inst::AddRV(X64Reg::RSP, s as i64)));
}

fn convert_to_x64_if_else(
    context: Context,
    id1: Id<VariableInfo>,
    label_t: Id<Label>,
    label_f: Id<Label>,
) {
    let Context {
        stack_manager,
        stmts,
        ..
    } = context;
    /*
        mov al, ?[rbp]
        movsx rax, al
        cmp rax, 0
        je label_f
        jmp label_t
    */
    let id1_offset = stack_manager.get_local_addr(id1);
    stmts.push(IRStmt::X64Inst(X64Inst::MovROR(
        X64Reg::AL,
        id1_offset,
        X64Reg::RBP,
    )));
    stmts.push(IRStmt::X64Inst(X64Inst::Movsx(X64Reg::RAX, X64Reg::AL)));
    stmts.push(IRStmt::X64Inst(X64Inst::CmpRV(X64Reg::RAX, Value::I(8, 0))));
    stmts.push(IRStmt::X64Inst(X64Inst::JeL(label_f)));
    stmts.push(IRStmt::X64Inst(X64Inst::JmpL(label_t)));
}

fn convert_to_x64_jmp_l(context: Context, id: Id<Label>) {
    let Context { stmts, .. } = context;
    stmts.push(IRStmt::X64Inst(X64Inst::JmpL(id)));
}

fn convert_to_x64_release(context: Context, id: Id<VariableInfo>) {
    let Context { .. } = context;
    // do nothing ?
}

fn convert_to_x64_set_label(context: Context, id: Id<Label>) {
    let Context { stmts, .. } = context;
    stmts.push(IRStmt::SetLabel(id));
}

fn convert_to_x64_mov_v(context: Context, id: Id<VariableInfo>, value: Value) {
    let Context {
        fun_def,
        stack_manager,
        stmts,
        ..
    } = context;
    let offset = stack_manager.get_local_addr(id);
    if let Value::Integer(v) = value {
        let ty = &fun_def.variables.as_ref().get(id).unwrap().ty;
        let (value, size) = match ty {
            Ty::I(size) => (Value::I(*size, v), *size),
            _ => unimplemented!(),
        };
        if size == 64 {
            stmts.push(IRStmt::X64Inst(X64Inst::MovRV(X64Reg::R10, value)));
            stmts.push(IRStmt::X64Inst(X64Inst::MovORR(
                offset,
                X64Reg::RBP,
                X64Reg::R10,
            )));
        } else {
            stmts.push(IRStmt::X64Inst(X64Inst::MovORV(
                offset,
                X64Reg::RBP,
                value.clone(),
            )));
        }
    } else if let Value::I(size, v) = value {
        if size == 64 {
            stmts.push(IRStmt::X64Inst(X64Inst::MovRV(X64Reg::R10, value.clone())));
            stmts.push(IRStmt::X64Inst(X64Inst::MovORR(
                offset,
                X64Reg::RBP,
                X64Reg::R10,
            )));
        } else {
            stmts.push(IRStmt::X64Inst(X64Inst::MovORV(
                offset,
                X64Reg::RBP,
                value.clone(),
            )));
        }
    } else if let Value::Float(s) = value {
        stmts.push(IRStmt::X64Inst(X64Inst::MovRV(
            X64Reg::R10,
            Value::Float(s),
        )));
        stmts.push(IRStmt::X64Inst(X64Inst::MovORR(
            offset,
            X64Reg::RBP,
            X64Reg::R10,
        )));
    } else if let Value::F((11, 52), _) = value {
        stmts.push(IRStmt::X64Inst(X64Inst::MovRV(X64Reg::R10, value.clone())));
        stmts.push(IRStmt::X64Inst(X64Inst::MovORR(
            offset,
            X64Reg::RBP,
            X64Reg::R10,
        )));
    } else if let Value::F((8, 23), _) = value {
        stmts.push(IRStmt::X64Inst(X64Inst::MovRV(X64Reg::R10D, value.clone())));
        stmts.push(IRStmt::X64Inst(X64Inst::MovORR(
            offset,
            X64Reg::RBP,
            X64Reg::R10D,
        )));
    } else {
        panic!();
    }
}

fn convert_to_x64_mov_i(context: Context, id1: Id<VariableInfo>, id2: Id<VariableInfo>) {
    let Context {
        ir,
        fun_def,
        stack_manager,
        stmts,
        ..
    } = context;
    let ty = &fun_def.variables.as_ref().get(id1).unwrap().ty;
    let id1_offset = stack_manager.get_local_addr(id1);
    let id2_offset = stack_manager.get_local_addr(id2);
    let memsize = ir.memsize(&ty);
    if memsize == 0 {
        // do nothing
    } else if memsize == 1 {
        stmts.push(IRStmt::X64Inst(X64Inst::MovROR(
            X64Reg::AL,
            id2_offset,
            X64Reg::RBP,
        )));
        stmts.push(IRStmt::X64Inst(X64Inst::MovORR(
            id1_offset,
            X64Reg::RBP,
            X64Reg::AL,
        )));
    } else if memsize == 8 {
        stmts.push(IRStmt::X64Inst(X64Inst::MovROR(
            X64Reg::RAX,
            id2_offset,
            X64Reg::RBP,
        )));
        stmts.push(IRStmt::X64Inst(X64Inst::MovORR(
            id1_offset,
            X64Reg::RBP,
            X64Reg::RAX,
        )));
    } else {
        // TODO: more short or loop
        for i in 0..memsize {
            stmts.push(IRStmt::X64Inst(X64Inst::MovROR(
                X64Reg::AL,
                id2_offset + i,
                X64Reg::RBP,
            )));
            stmts.push(IRStmt::X64Inst(X64Inst::MovORR(
                id1_offset + i,
                X64Reg::RBP,
                X64Reg::AL,
            )));
        }
    }
}

fn convert_to_x64_mov_ri(context: Context, id1: Id<VariableInfo>, id2: Id<VariableInfo>) {
    let Context {
        ir,
        fun_def,
        stack_manager,
        stmts,
        ..
    } = context;
    let ty = &fun_def.variables.as_ref().get(id2).unwrap().ty;
    let id1_offset = stack_manager.get_local_addr(id1);
    let id2_offset = stack_manager.get_local_addr(id2);
    let memsize = ir.memsize(&ty);
    stmts.push(IRStmt::X64Inst(X64Inst::MovROR(
        X64Reg::RDX,
        id1_offset,
        X64Reg::RBP,
    )));
    if memsize == 0 {
        // do nothing
    } else if memsize == 1 {
        stmts.push(IRStmt::X64Inst(X64Inst::MovROR(
            X64Reg::AL,
            id2_offset,
            X64Reg::RBP,
        )));
        stmts.push(IRStmt::X64Inst(X64Inst::MovORR(0, X64Reg::RDX, X64Reg::AL)));
    } else if memsize == 8 {
        stmts.push(IRStmt::X64Inst(X64Inst::MovROR(
            X64Reg::RAX,
            id2_offset,
            X64Reg::RBP,
        )));
        stmts.push(IRStmt::X64Inst(X64Inst::MovORR(
            0,
            X64Reg::RDX,
            X64Reg::RAX,
        )));
    } else {
        // TODO: more short or loop
        for i in 0..memsize {
            stmts.push(IRStmt::X64Inst(X64Inst::MovROR(
                X64Reg::AL,
                id2_offset + i,
                X64Reg::RBP,
            )));
            stmts.push(IRStmt::X64Inst(X64Inst::MovORR(i, X64Reg::RDX, X64Reg::AL)));
        }
    }
}

fn convert_to_x64_construct(
    context: Context,
    to: Id<VariableInfo>,
    variant_id: Id<VariantInfo>,
    args: Vec<Id<VariableInfo>>,
) {
    let Context {
        ir,
        stack_manager,
        stmts,
        ..
    } = context;
    let addr_to = stack_manager.get_local_addr(to);
    let mut offset = 0;
    let variant_def = &ir.variant_table[variant_id];
    for (field, arg) in variant_def.fields.iter().zip(args.iter()) {
        let arg_addr = stack_manager.get_local_addr(*arg);
        stmts.push(IRStmt::X64Inst(X64Inst::MovROR(
            X64Reg::RAX,
            arg_addr,
            X64Reg::RBP,
        )));
        stmts.push(IRStmt::X64Inst(X64Inst::MovORR(
            addr_to + offset,
            X64Reg::RBP,
            X64Reg::RAX,
        )));
        offset += ir.memsize(&field.1);
    }
    let mut ok = false;
    let data_id = ir.data_of_variant[&variant_id];
    for (i, variant_id2) in ir.data_table[&data_id].variants.iter().enumerate() {
        if variant_id == *variant_id2 {
            stmts.push(IRStmt::X64Inst(X64Inst::MovRV(
                X64Reg::RAX,
                Value::I(64, i as i64),
            )));
            stmts.push(IRStmt::X64Inst(X64Inst::MovORR(
                addr_to + offset,
                X64Reg::RBP,
                X64Reg::RAX,
            )));
            ok = true;
            break;
        }
    }
    if !ok {
        panic!("unmatched: variant = {:?}", variant_id);
    }
}

fn convert_to_x64_destruct(
    context: Context,
    to: Vec<Id<VariableInfo>>,
    variant: Id<VariantInfo>,
    from: Id<VariableInfo>,
) {
    let Context {
        ir,
        stack_manager,
        stmts,
        ..
    } = context;
    let addr_from = stack_manager.get_local_addr(from);
    let mut offset = 0;
    let variant_def = &ir.variant_table[variant];
    for (field, to) in variant_def.fields.iter().zip(to.iter()) {
        let addr_to = stack_manager.get_local_addr(*to);
        stmts.push(IRStmt::X64Inst(X64Inst::MovROR(
            X64Reg::RAX,
            addr_from + offset,
            X64Reg::RBP,
        )));
        stmts.push(IRStmt::X64Inst(X64Inst::MovORR(
            addr_to,
            X64Reg::RBP,
            X64Reg::RAX,
        )));
        offset += ir.memsize(&field.1);
    }
}

fn convert_to_x64_match_jmp(
    context: Context,
    item: Id<VariableInfo>,
    pairs_of_variant_and_label: Vec<(Id<VariantInfo>, Id<Label>)>,
) {
    let Context {
        ir,
        fun_def,
        stack_manager,
        stmts,
        ..
    } = context;
    // prepare variant id
    /*
        mov rax, ?[rbp]
    */
    let addr_of_item = stack_manager.get_local_addr(item);
    let ty_of_item = &fun_def.variables.as_ref().get(item).unwrap().ty;
    stmts.push(IRStmt::X64Inst(X64Inst::MovROR(
        X64Reg::RAX,
        addr_of_item + ir.memsize(&ty_of_item) - 8,
        X64Reg::RBP,
    )));
    for (i, variant_and_label) in pairs_of_variant_and_label.iter().enumerate() {
        /*
            cmp rax, ?
            je label
        */
        stmts.push(IRStmt::X64Inst(X64Inst::CmpRV(
            X64Reg::RAX,
            Value::I(64, i as i64),
        )));
        stmts.push(IRStmt::X64Inst(X64Inst::JeL(variant_and_label.1)));
    }
}

fn convert_to_x64_x64_inst(context: Context, inst: X64Inst) {
    let Context {
        stack_manager,
        stmts,
        ..
    } = context;
    match inst {
        X64Inst::MovRV(reg, v) => {
            stmts.push(IRStmt::X64Inst(X64Inst::MovRV(reg, v.clone())));
        }
        X64Inst::Cvttsd2si(reg1, reg2) => {
            stmts.push(IRStmt::X64Inst(X64Inst::Cvttsd2si(reg1, reg2)));
        }
        X64Inst::Cvttss2si(reg1, reg2) => {
            stmts.push(IRStmt::X64Inst(X64Inst::Cvttss2si(reg1, reg2)));
        }
        X64Inst::Syscall => {
            stmts.push(IRStmt::X64Inst(X64Inst::Syscall));
        }
        X64Inst::MovIRV(item_id, reg, value) => {
            let addr = stack_manager.get_local_addr(item_id);
            stmts.push(IRStmt::X64Inst(X64Inst::MovORV(addr, reg, value.clone())));
        }
        X64Inst::MovRIR(reg1, item_id, reg2) => {
            let addr = stack_manager.get_local_addr(item_id);
            stmts.push(IRStmt::X64Inst(X64Inst::MovROR(reg1, addr, reg2)));
        }
        X64Inst::MovIRR(item_id, reg1, reg2) => {
            let addr = stack_manager.get_local_addr(item_id);
            stmts.push(IRStmt::X64Inst(X64Inst::MovORR(addr, reg1, reg2)));
        }
        X64Inst::MovsdRIR(reg1, item_id, reg2) => {
            let addr = stack_manager.get_local_addr(item_id);
            stmts.push(IRStmt::X64Inst(X64Inst::MovsdROR(reg1, addr, reg2)));
        }
        X64Inst::MovssRIR(reg1, item_id, reg2) => {
            let addr = stack_manager.get_local_addr(item_id);
            stmts.push(IRStmt::X64Inst(X64Inst::MovssROR(reg1, addr, reg2)));
        }
        inst => {
            panic!("unimplemented: {:?}", inst);
        }
    }
}

fn convert_to_x64_make_ref(context: Context, id1: Id<VariableInfo>, id2: Id<VariableInfo>) {
    let Context {
        stack_manager,
        stmts,
        ..
    } = context;
    // id1: &&T
    // id2: &T
    let addr_1 = stack_manager.get_local_addr(id1);
    let addr_2 = stack_manager.get_local_addr(id2);
    stmts.push(IRStmt::X64Inst(X64Inst::LeaROR(
        X64Reg::RAX,
        addr_2,
        X64Reg::RBP,
    )));
    stmts.push(IRStmt::X64Inst(X64Inst::MovORR(
        addr_1,
        X64Reg::RBP,
        X64Reg::RAX,
    )));
}

fn convert_to_x64_get_array_elem_addr(
    context: Context,
    id1: Id<VariableInfo>,
    id2: Id<VariableInfo>,
    id3: Id<VariableInfo>,
) {
    let Context {
        ir,
        fun_def,
        stack_manager,
        stmts,
        ..
    } = context;
    let addr_1 = stack_manager.get_local_addr(id1);
    let addr_2 = stack_manager.get_local_addr(id2);
    let addr_3 = stack_manager.get_local_addr(id3);
    let ty = &fun_def.variables.as_ref().get(id1).unwrap().ty;
    let memsize = ir.memsize(ty);
    assert!(memsize == 8);
    let inner_memsize = if let Ty::Ref(inner) = ty {
        ir.memsize(inner)
    } else {
        panic!();
    };
    stmts.push(IRStmt::X64Inst(X64Inst::MovROR(
        X64Reg::RAX,
        addr_3,
        X64Reg::RBP,
    )));
    stmts.push(IRStmt::X64Inst(X64Inst::MovRV(
        X64Reg::RDI,
        Value::I(64, inner_memsize),
    )));
    stmts.push(IRStmt::X64Inst(X64Inst::ImulRR(X64Reg::RAX, X64Reg::RDI)));
    stmts.push(IRStmt::X64Inst(X64Inst::MovROR(
        X64Reg::RDI,
        addr_2,
        X64Reg::RBP,
    )));
    stmts.push(IRStmt::X64Inst(X64Inst::AddRR(X64Reg::RAX, X64Reg::RDI)));
    stmts.push(IRStmt::X64Inst(X64Inst::MovORR(
        addr_1,
        X64Reg::RBP,
        X64Reg::RAX,
    )));
}

fn convert_to_x64_init_cuda(context: Context) {
    let Context { stmts, .. } = context;
    stmts.push(IRStmt::InitCUDA);
}

fn convert_to_x64_call_cuda_kernel(
    context: Context,
    f: Id<Function>,
    arg: (Id<VariableInfo>, i64),
    grid: (usize, usize, usize),
    block: (usize, usize, usize),
) {
    let Context {
        stack_manager,
        stmts,
        ..
    } = context;
    let offset = stack_manager.get_local_addr(arg.0);
    stmts.push(IRStmt::CallCudaKernel(f, (arg.0, offset), grid, block));
}

fn convert_to_x64_convert_f_to_i(
    context: Context,
    id1: Id<VariableInfo>,
    (exp_bits, frac_bits): (i64, i64),
    int_bits: i64,
    id2: Id<VariableInfo>,
) {
    let Context {
        stack_manager,
        stmts,
        ..
    } = context;
    if exp_bits == 8 && frac_bits == 23 && int_bits == 32 {
        let addr_1 = stack_manager.get_local_addr(id1);
        let addr_2 = stack_manager.get_local_addr(id2);
        stmts.push(IRStmt::X64Inst(X64Inst::MovssROR(
            X64Reg::Xmm0,
            addr_2,
            X64Reg::RBP,
        )));
        stmts.push(IRStmt::X64Inst(X64Inst::Cvttss2si(
            X64Reg::RAX,
            X64Reg::Xmm0,
        )));
        stmts.push(IRStmt::X64Inst(X64Inst::MovORR(
            addr_1,
            X64Reg::RBP,
            X64Reg::RAX,
        )));
    } else {
        unimplemented!("exp_bits = {}, frac_bits = {}", exp_bits, frac_bits);
    }
}

fn convert_to_x64_convert_i_to_f(
    context: Context,
    id1: Id<VariableInfo>,
    int_bits: i64,
    (exp_bits, frac_bits): (i64, i64),
    id2: Id<VariableInfo>,
) {
    let Context {
        stack_manager,
        stmts,
        ..
    } = context;
    if exp_bits == 8 && frac_bits == 23 && int_bits == 32 {
        let addr_1 = stack_manager.get_local_addr(id1);
        let addr_2 = stack_manager.get_local_addr(id2);
        stmts.push(IRStmt::X64Inst(X64Inst::MovssROR(
            X64Reg::RAX,
            addr_2,
            X64Reg::RBP,
        )));
        stmts.push(IRStmt::X64Inst(X64Inst::Cvtsi2ssl(
            X64Reg::Xmm0,
            X64Reg::RAX,
        )));
        stmts.push(IRStmt::X64Inst(X64Inst::MovssORR(
            addr_1,
            X64Reg::RBP,
            X64Reg::Xmm0,
        )));
    } else {
        unimplemented!("exp_bits = {}, frac_bits = {}", exp_bits, frac_bits);
    }
}

fn convert_to_x64_convert_i_to_i(
    context: Context,
    id1: Id<VariableInfo>,
    id2_bits: i64,
    id1_bits: i64,
    id2: Id<VariableInfo>,
) {
    let Context {
        stack_manager,
        stmts,
        ..
    } = context;
    if id2_bits == 32 && id1_bits == 8 {
        let addr_1 = stack_manager.get_local_addr(id1);
        let addr_2 = stack_manager.get_local_addr(id2);
        stmts.push(IRStmt::X64Inst(X64Inst::MovROR(
            X64Reg::EAX,
            addr_2,
            X64Reg::RBP,
        )));
        stmts.push(IRStmt::X64Inst(X64Inst::MovORR(
            addr_1,
            X64Reg::RBP,
            X64Reg::AL,
        )));
    } else if id2_bits == 8 && id1_bits == 32 {
        let addr_1 = stack_manager.get_local_addr(id1);
        let addr_2 = stack_manager.get_local_addr(id2);
        stmts.push(IRStmt::X64Inst(X64Inst::MovRV(
            X64Reg::RAX,
            Value::Integer(0),
        )));
        stmts.push(IRStmt::X64Inst(X64Inst::MovROR(
            X64Reg::AL,
            addr_2,
            X64Reg::RBP,
        )));
        stmts.push(IRStmt::X64Inst(X64Inst::MovORR(
            addr_1,
            X64Reg::RBP,
            X64Reg::EAX,
        )));
    } else if id2_bits == 64 && id1_bits == 8 {
        let addr_1 = stack_manager.get_local_addr(id1);
        let addr_2 = stack_manager.get_local_addr(id2);
        stmts.push(IRStmt::X64Inst(X64Inst::MovROR(
            X64Reg::RAX,
            addr_2,
            X64Reg::RBP,
        )));
        stmts.push(IRStmt::X64Inst(X64Inst::MovORR(
            addr_1,
            X64Reg::RBP,
            X64Reg::AL,
        )));
    } else if id2_bits == 32 && id1_bits == 64 {
        let addr_1 = stack_manager.get_local_addr(id1);
        let addr_2 = stack_manager.get_local_addr(id2);
        stmts.push(IRStmt::X64Inst(X64Inst::MovRV(
            X64Reg::RAX,
            Value::Integer(0),
        )));
        stmts.push(IRStmt::X64Inst(X64Inst::MovROR(
            X64Reg::EAX,
            addr_2,
            X64Reg::RBP,
        )));
        stmts.push(IRStmt::X64Inst(X64Inst::MovORR(
            addr_1,
            X64Reg::RBP,
            X64Reg::RAX,
        )));
    } else {
        unimplemented!("id1_bits = {}, id2_bits = {}", id1_bits, id2_bits);
    }
}
