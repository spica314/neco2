use crate::*;

#[derive(Debug, Clone)]
struct Scope2 {
    parent: Option<Box<Scope2>>,
    var_map: Vec<(String, Id<VariableInfo>)>,
    fun_map: Vec<(String, Id<Function>)>,
    data_map: Vec<(String, Id<DataInfo>)>,
    variant_map: Vec<(String, Id<VariantInfo>)>,
}

impl Scope2 {
    fn new() -> Scope2 {
        Scope2 {
            parent: None,
            var_map: vec![],
            fun_map: vec![],
            data_map: vec![],
            variant_map: vec![],
        }
    }
    /// 親のスコープを指定してnew
    fn new_with_parent(parent: Scope2) -> Scope2 {
        Scope2 {
            parent: Some(Box::new(parent)),
            var_map: vec![],
            fun_map: vec![],
            data_map: vec![],
            variant_map: vec![],
        }
    }
    /// 名前とId<VariableInfo>の対応を登録
    fn push_var<S: AsRef<str>>(&mut self, name: S, id: Id<VariableInfo>) {
        self.var_map.push((name.as_ref().to_string(), id));
    }
    fn push_fun<S: AsRef<str>>(&mut self, name: S, id: Id<Function>) {
        self.fun_map.push((name.as_ref().to_string(), id));
    }
    fn push_data<S: AsRef<str>>(&mut self, name: S, id: Id<DataInfo>) {
        self.data_map.push((name.as_ref().to_string(), id));
    }
    fn push_variant(&mut self, data_name: &str, variant_name: &str, id: Id<VariantInfo>) {
        self.variant_map
            .push((format!("{}::{}", data_name, variant_name), id));
    }
    /// varの検索，親のスコープもたどる
    fn get_var_id(&self, name: &str) -> Option<Id<VariableInfo>> {
        for (s, id) in self.var_map.iter().rev() {
            if s == name {
                return Some(*id);
            }
        }
        if let Some(parent) = &self.parent {
            return parent.get_var_id(name);
        }
        None
    }
    /// funの検索，親のスコープもたどる
    fn get_fun_id(&self, name: &str) -> Option<Id<Function>> {
        for (s, id) in self.fun_map.iter().rev() {
            if s == name {
                return Some(*id);
            }
        }
        if let Some(parent) = &self.parent {
            return parent.get_fun_id(name);
        }
        None
    }
    /// dataの検索，親のスコープもたどる
    fn get_data_id(&self, name: &str) -> Option<Id<DataInfo>> {
        for (s, id) in self.data_map.iter().rev() {
            if s == name {
                return Some(*id);
            }
        }
        if let Some(parent) = &self.parent {
            return parent.get_data_id(name);
        }
        None
    }
    /// variantの検索，親のスコープもたどる
    fn get_variant_id(&self, name: &str) -> Option<Id<VariantInfo>> {
        for (s, id) in self.variant_map.iter().rev() {
            if s == name {
                return Some(*id);
            }
        }
        if let Some(parent) = &self.parent {
            return parent.get_variant_id(name);
        }
        None
    }
    /// 親のスコープを返す
    fn pop(mut self) -> (Option<Scope2>, Vec<(String, Id<VariableInfo>)>) {
        if let Some(scope) = self.parent.take() {
            (Some(*scope), self.var_map)
        } else {
            (None, self.var_map)
        }
    }
}

impl IR {
    pub fn resolve_name(&mut self) {
        self.resolve_name_inner();
    }
    fn resolve_name_inner(&mut self) {
        // prepare root scope
        let mut scope = Scope2::new();
        for &id in &self.functions {
            let fun_def = &self.fun_defs[id];
            scope.push_fun(fun_def.name.clone(), id);
        }
        for &id in &self.data {
            let data_def = &self.data_table[id];
            scope.push_data(&data_def.name, id);
            for &vid in &data_def.variants {
                let variant_def = &self.variant_table[vid];
                scope.push_variant(&data_def.name, &variant_def.name, vid);
            }
        }
        // resolve
        for id in &self.functions {
            let fun_def = self.fun_defs.get_mut(*id).unwrap();
            scope = Scope2::new_with_parent(scope);
            for &arg in &fun_def.args {
                let arg_name = fun_def.variables.as_ref()[arg].name.as_ref().unwrap();
                eprintln!("arg = {:?}, arg_name = {:?}", arg, arg_name);
                scope.push_var(arg_name.clone(), arg);
            }

            let mut stmts2 = IRStmts::new();
            for stmt in &fun_def.stmts.stmts {
                match stmt {
                    IRStmt::EnterScope => {
                        scope = Scope2::new_with_parent(scope);
                    }
                    IRStmt::LeaveScope => {
                        let (parent_scope, var_map) = scope.pop();
                        scope = parent_scope.unwrap();
                        for &(_, id) in var_map.iter().rev() {
                            stmts2.push(IRStmt::Release(id));
                        }
                    }
                    IRStmt::Let(id) => {
                        scope.push_var("", *id);
                        stmts2.push(IRStmt::Let(*id));
                    }
                    IRStmt::LetWithName(id, name) => {
                        scope.push_var(name, *id);
                        stmts2.push(IRStmt::Let(*id));
                    }
                    IRStmt::HeapAllocByName(name) => {
                        let id = scope.get_var_id(name).unwrap();
                        stmts2.push(IRStmt::HeapAlloc(id));
                    }
                    IRStmt::GetVariableByName(id, name) => {
                        eprintln!("name = {:?}", name);
                        let id2 = scope.get_var_id(name).unwrap();
                        stmts2.push(IRStmt::MovI(*id, id2));
                    }
                    IRStmt::GetReferenceByName(id, name) => {
                        let id2 = scope.get_var_id(name).unwrap();
                        stmts2.push(IRStmt::MakeRef(*id, id2));
                    }
                    IRStmt::CallByName(res, name, args) => {
                        let id = scope.get_fun_id(name).unwrap();
                        stmts2.push(IRStmt::Call(*res, id, args.clone()));
                    }
                    IRStmt::CallCudaKernelByName(f, arg, grid, block) => {
                        let f_id = scope.get_fun_id(f).unwrap();
                        let arg_id = scope.get_var_id(arg).unwrap();
                        stmts2.push(IRStmt::CallCudaKernel(f_id, (arg_id, 0), *grid, *block));
                    }
                    IRStmt::X64Inst(inst) => {
                        let inst2 = match inst {
                            X64Inst::MovIRVByName(name, reg, v) => {
                                let id = scope.get_var_id(name).unwrap();
                                X64Inst::MovIRV(id, *reg, v.clone())
                            }
                            X64Inst::MovRIRByName(reg1, name, reg2) => {
                                let id = scope.get_var_id(name).unwrap();
                                X64Inst::MovRIR(*reg1, id, *reg2)
                            }
                            X64Inst::MovIRRByName(name, reg1, reg2) => {
                                let id = scope.get_var_id(name).unwrap();
                                X64Inst::MovIRR(id, *reg1, *reg2)
                            }
                            X64Inst::MovsdRIRByName(reg1, name, reg2) => {
                                let id = scope.get_var_id(name).unwrap();
                                X64Inst::MovsdRIR(*reg1, id, *reg2)
                            }
                            X64Inst::MovssRIRByName(reg1, name, reg2) => {
                                let id = scope.get_var_id(name).unwrap();
                                X64Inst::MovssRIR(*reg1, id, *reg2)
                            }
                            inst => inst.clone(),
                        };
                        stmts2.push(IRStmt::X64Inst(inst2));
                    }
                    IRStmt::ConstructByName(res, name, args) => {
                        let variant_id = scope.get_variant_id(name).unwrap();
                        stmts2.push(IRStmt::Construct(*res, variant_id, args.clone()));
                    }
                    IRStmt::DestructByName(res, name, x) => {
                        let variant_id = scope.get_variant_id(name).unwrap();
                        stmts2.push(IRStmt::Destruct(res.clone(), variant_id, *x));
                    }
                    IRStmt::MatchJmpByName(x, js) => {
                        let mut js2 = vec![];
                        for (name, label) in js {
                            let variant_id = scope.get_variant_id(name).unwrap();
                            js2.push((variant_id, *label));
                        }
                        stmts2.push(IRStmt::MatchJmp(*x, js2));
                    }
                    stmt => {
                        stmts2.push(stmt.clone());
                    }
                }
            }
            fun_def.stmts = stmts2;
            // ignore var_map (args)
            scope = scope.pop().0.unwrap();
        }
    }
}
