use crate::*;

pub fn ty_to_ptx_ty(ty: &Ty) -> PtxTy {
    match ty {
        Ty::Ref(inner) => PtxTy::U64,
        Ty::I(32) => PtxTy::S32,
        Ty::U(64) => PtxTy::U64,
        Ty::F(8, 23) => PtxTy::F32,
        other => {
            unimplemented!("other = {:?}", other);
        }
    }
}
