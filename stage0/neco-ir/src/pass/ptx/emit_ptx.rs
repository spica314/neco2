use std::collections::HashSet;

use crate::*;

use super::ty_to_ptx_ty::{self, *};

impl IR {
    /// ptxの生成
    pub fn emit_ptx(&self) -> String {
        let mut res = String::new();
        res.push_str(&format!(".version 7.1\n"));
        res.push_str(&format!(".target sm_52\n"));
        res.push_str(&format!(".address_size 64\n"));
        for &fun_id in &self.functions {
            let mut fun_def = self.fun_defs[fun_id].clone();
            if fun_def.target_arch != Some(TargetArch::Ptx) {
                continue;
            }
            res.push_str(&format!(".visible .entry {}(\n", fun_def.name));
            let mut arg_set = HashSet::new();
            for &arg in &fun_def.args {
                let ty = fun_def.variables.get_ty(arg).unwrap();
                let ptx_ty = ty_to_ptx_ty(ty);
                res.push_str(&format!(
                    "    .param .{} param_{},\n",
                    ptx_ty.as_str(),
                    arg.id()
                ));
                arg_set.insert(arg);
            }
            // pop ,\n
            res.pop();
            res.pop();
            res.push('\n');
            res.push_str(")\n");
            res.push_str("{\n");
            for stmt in &fun_def.stmts.stmts {
                match stmt {
                    IRStmt::Let(id) => {
                        let ty = fun_def.variables.get_ty(*id).unwrap();
                        if ty == &Ty::Unit {
                            continue;
                        }
                        let ptx_ty = ty_to_ptx_ty(ty);
                        res.push_str(&format!("    .reg .{} %r{};\n", ptx_ty.as_str(), id.id()));
                    }
                    IRStmt::PtxInst(inst) => match inst {
                        PtxInst::MovII(ptx_ty, id1, id2) => {
                            res.push_str(&format!(
                                "    mov.{} %r{}, %r{};\n",
                                ptx_ty.as_str(),
                                id1.id(),
                                id2.id()
                            ));
                        }
                        PtxInst::MovIV(ptx_ty, id, v) => {
                            res.push_str(&format!(
                                "    mov.{} %r{}, {};\n",
                                ptx_ty.as_str(),
                                id.id(),
                                v.to_ptx_string()
                            ));
                        }
                        PtxInst::MovISpecial(ptx_ty, id, special) => {
                            res.push_str(&format!(
                                "    mov.{} %r{}, {};\n",
                                ptx_ty.as_str(),
                                id.id(),
                                special.as_str()
                            ));
                        }
                        PtxInst::LdParam(ptx_ty, id1, id2) => {
                            res.push_str(&format!(
                                "    ld.param.{} %r{}, [param_{}];\n",
                                ptx_ty.as_str(),
                                id1.id(),
                                id2.id()
                            ));
                        }
                        PtxInst::LdGlobal(ptx_ty, id1, id2) => {
                            res.push_str(&format!(
                                "    ld.global.{} %r{}, [%r{}];\n",
                                ptx_ty.as_str(),
                                id1.id(),
                                id2.id()
                            ));
                        }
                        PtxInst::StGlobal(ptx_ty, id1, id2) => {
                            res.push_str(&format!(
                                "    st.global.{} [%r{}], %r{};\n",
                                ptx_ty.as_str(),
                                id1.id(),
                                id2.id()
                            ));
                        }
                        PtxInst::CvtaToGlobal(ptx_ty, id1, id2) => {
                            res.push_str(&format!(
                                "    cvta.to.global.{} %r{}, %r{};\n",
                                ptx_ty.as_str(),
                                id1.id(),
                                id2.id()
                            ));
                        }
                        PtxInst::Ret => res.push_str("    ret;\n"),
                        PtxInst::AddIII(ptx_ty, id1, id2, id3) => {
                            res.push_str(&format!(
                                "    add.{} %r{}, %r{}, %r{};\n",
                                ptx_ty.as_str(),
                                id1.id(),
                                id2.id(),
                                id3.id()
                            ));
                        }
                        PtxInst::SubIII(ptx_ty, id1, id2, id3) => {
                            res.push_str(&format!(
                                "    sub.{} %r{}, %r{}, %r{};\n",
                                ptx_ty.as_str(),
                                id1.id(),
                                id2.id(),
                                id3.id()
                            ));
                        }
                        PtxInst::MulIII(ptx_ty, id1, id2, id3) => {
                            if ptx_ty.is_float() {
                                res.push_str(&format!(
                                    "    mul.{} %r{}, %r{}, %r{};\n",
                                    ptx_ty.as_str(),
                                    id1.id(),
                                    id2.id(),
                                    id3.id()
                                ));
                            } else {
                                res.push_str(&format!(
                                    "    mul.lo.{} %r{}, %r{}, %r{};\n",
                                    ptx_ty.as_str(),
                                    id1.id(),
                                    id2.id(),
                                    id3.id()
                                ));
                            }
                        }
                        PtxInst::DivIII(ptx_ty, id1, id2, id3) => {
                            if ptx_ty.is_float() {
                                res.push_str(&format!(
                                    "    div.approx.{} %r{}, %r{}, %r{};\n",
                                    ptx_ty.as_str(),
                                    id1.id(),
                                    id2.id(),
                                    id3.id()
                                ));
                            } else {
                                res.push_str(&format!(
                                    "    div.{} %r{}, %r{}, %r{};\n",
                                    ptx_ty.as_str(),
                                    id1.id(),
                                    id2.id(),
                                    id3.id()
                                ));
                            }
                        }
                        PtxInst::RemIII(ptx_ty, id1, id2, id3) => {
                            res.push_str(&format!(
                                "    rem.{} %r{}, %r{}, %r{};\n",
                                ptx_ty.as_str(),
                                id1.id(),
                                id2.id(),
                                id3.id()
                            ));
                        }
                        PtxInst::CvtII(ptx_ty1, ptx_ty2, rm, id1, id2) => {
                            if let Some(rm) = rm {
                                res.push_str(&format!(
                                    "    cvt.{}.{}.{} %r{}, %r{};\n",
                                    ptx_ty1.as_str(),
                                    ptx_ty2.as_str(),
                                    rm.as_str(),
                                    id1.id(),
                                    id2.id()
                                ));
                            } else {
                                res.push_str(&format!(
                                    "    cvt.{}.{} %r{}, %r{};\n",
                                    ptx_ty1.as_str(),
                                    ptx_ty2.as_str(),
                                    id1.id(),
                                    id2.id()
                                ));
                            }
                        }
                        other => {
                            unimplemented!("other = {:?}", other);
                        }
                    },
                    other => {
                        unimplemented!("other = {:?}", other);
                    }
                }
            }
            res.push_str("}\n");
        }
        res
    }
}
