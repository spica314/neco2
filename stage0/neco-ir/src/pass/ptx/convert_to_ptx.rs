use crate::*;

use super::ty_to_ptx_ty::*;

impl IR {
    /// ptxコードへの変換
    pub fn convert_to_ptx(&mut self) {
        for &fun_id in &self.functions {
            let mut fun_def = self.fun_defs[fun_id].clone();
            if fun_def.target_arch != Some(TargetArch::Ptx) {
                continue;
            }
            let mut variables = fun_def.variables.clone();
            let mut stmts = IRStmts::new();

            for &arg in &fun_def.args {
                let ty = fun_def.variables.get_ty(arg).unwrap();
                let ptx_ty = ty_to_ptx_ty(ty);
                stmts.push(IRStmt::Let(arg));
                let id = variables.as_mut().insert(VariableInfo {
                    name: None,
                    ty: Ty::U(64),
                });
                stmts.push(IRStmt::Let(id));
                stmts.push(IRStmt::PtxInst(PtxInst::LdParam(ptx_ty, id, arg)));
                stmts.push(IRStmt::PtxInst(PtxInst::CvtaToGlobal(ptx_ty, arg, id)));
            }

            for stmt in &fun_def.stmts.stmts {
                eprintln!("stmt = {:?}", stmt);
                match stmt {
                    IRStmt::Let(id) => {
                        stmts.push(IRStmt::Let(*id));
                    }
                    IRStmt::MovI(id1, id2) => {
                        let ty = variables.get_ty(*id1).unwrap();
                        if ty == &Ty::Unit {
                            continue;
                        }
                        let ptx_ty = ty_to_ptx_ty(ty);
                        stmts.push(IRStmt::PtxInst(PtxInst::MovII(ptx_ty, *id1, *id2)));
                        /*
                        if arg_set.contains(id2) {
                            arg_set.insert(*id1);
                        }
                        */
                    }
                    IRStmt::MovV(id1, v) => {
                        let ty = variables.get_ty(*id1).unwrap();
                        if ty == &Ty::Unit {
                            continue;
                        }
                        let ptx_ty = ty_to_ptx_ty(ty);
                        stmts.push(IRStmt::PtxInst(PtxInst::MovIV(ptx_ty, *id1, v.clone())));
                    }
                    IRStmt::Deref(id1, id2) => {
                        let ty = variables.get_ty(*id1).unwrap().clone();
                        let ptx_ty = ty_to_ptx_ty(&ty);
                        stmts.push(IRStmt::PtxInst(PtxInst::LdGlobal(ptx_ty, *id1, *id2)));
                    }
                    IRStmt::MovRI(id1, id2) => {
                        let ty = variables.get_ty(*id2).unwrap().clone();
                        let ptx_ty = ty_to_ptx_ty(&ty);
                        stmts.push(IRStmt::PtxInst(PtxInst::StGlobal(ptx_ty, *id1, *id2)));
                    }
                    IRStmt::Release(id) => {
                        // TODO: destructor?
                    }
                    IRStmt::Ret(id) => {
                        let ty = variables.get_ty(*id).unwrap().clone();
                        assert_eq!(ty, Ty::Unit);
                        stmts.push(IRStmt::PtxInst(PtxInst::Ret));
                    }
                    IRStmt::III(inst, id1, id2, id3) => {
                        if inst.is_arithmetic() {
                            let ty = fun_def.variables.get_ty(*id1).unwrap();
                            let ptx_ty = ty_to_ptx_ty(ty);
                            match inst {
                                IRInst::Add => {
                                    stmts.push(IRStmt::PtxInst(PtxInst::AddIII(
                                        ptx_ty, *id1, *id2, *id3,
                                    )));
                                }
                                IRInst::Sub => {
                                    stmts.push(IRStmt::PtxInst(PtxInst::SubIII(
                                        ptx_ty, *id1, *id2, *id3,
                                    )));
                                }
                                IRInst::Mul => {
                                    stmts.push(IRStmt::PtxInst(PtxInst::MulIII(
                                        ptx_ty, *id1, *id2, *id3,
                                    )));
                                }
                                IRInst::Div => {
                                    stmts.push(IRStmt::PtxInst(PtxInst::DivIII(
                                        ptx_ty, *id1, *id2, *id3,
                                    )));
                                }
                                IRInst::Mod => {
                                    stmts.push(IRStmt::PtxInst(PtxInst::RemIII(
                                        ptx_ty, *id1, *id2, *id3,
                                    )));
                                }
                                other => panic!("other = {:?}", other),
                            }
                        } else {
                            unimplemented!();
                        }
                    }
                    IRStmt::PtxInst(inst) => match inst {
                        PtxInst::MovISpecial(ptx_ty, id, special) => {
                            stmts.push(IRStmt::PtxInst(PtxInst::MovISpecial(
                                *ptx_ty, *id, *special,
                            )));
                        }
                        other => {
                            unimplemented!("other = {:?}", other);
                        }
                    },
                    IRStmt::GetArrayElemAddr(id1, id2, id3) => {
                        let ty = variables.get_ty(*id1).unwrap();
                        let ty = match ty {
                            Ty::Ref(ty2) => ty2.as_ref().clone(),
                            _ => panic!(),
                        };
                        let ptx_ty = ty_to_ptx_ty(&ty);
                        let memsize = ptx_ty.memsize();
                        assert!(variables.get_ty(*id3).unwrap() == &Ty::I(32));
                        /*
                            id1 = &id2[id3]
                            id6 = id3 (ty->ty)
                            id4 = memsize
                            id5 = id6 * id4
                            id1 = id2 + id5
                        */
                        let id4 = variables.as_mut().insert(VariableInfo {
                            name: None,
                            ty: Ty::U(64),
                        });
                        stmts.push(IRStmt::Let(id4));
                        let id5 = variables.as_mut().insert(VariableInfo {
                            name: None,
                            ty: Ty::U(64),
                        });
                        stmts.push(IRStmt::Let(id5));
                        let id6 = variables.as_mut().insert(VariableInfo {
                            name: None,
                            ty: Ty::U(64),
                        });
                        stmts.push(IRStmt::Let(id6));
                        stmts.push(IRStmt::PtxInst(PtxInst::CvtII(
                            PtxTy::U64,
                            PtxTy::S32,
                            None,
                            id6,
                            *id3,
                        )));
                        stmts.push(IRStmt::PtxInst(PtxInst::MovIV(
                            PtxTy::U64,
                            id4,
                            Value::I(64, memsize),
                        )));
                        stmts.push(IRStmt::PtxInst(PtxInst::MulIII(PtxTy::U64, id5, id6, id4)));
                        stmts.push(IRStmt::PtxInst(PtxInst::AddIII(
                            PtxTy::U64,
                            *id1,
                            *id2,
                            id5,
                        )));
                    }
                    IRStmt::ConvertItoF(id1, int_bits, (exp_bits, frac_bits), id2) => {
                        if *int_bits == 32 && *exp_bits == 8 && *frac_bits == 23 {
                            let ty1 = PtxTy::F32;
                            let ty2 = PtxTy::S32;
                            stmts.push(IRStmt::PtxInst(PtxInst::CvtII(
                                ty1,
                                ty2,
                                Some(PtxRoundingModifier::RN),
                                *id1,
                                *id2,
                            )));
                        } else {
                            unimplemented!()
                        }
                    }
                    IRStmt::ConvertFtoI(id1, (exp_bits, frac_bits), int_bits, id2) => {
                        if *int_bits == 32 && *exp_bits == 8 && *frac_bits == 23 {
                            let ty1 = PtxTy::S32;
                            let ty2 = PtxTy::F32;
                            stmts.push(IRStmt::PtxInst(PtxInst::CvtII(
                                ty1,
                                ty2,
                                Some(PtxRoundingModifier::RNI),
                                *id1,
                                *id2,
                            )));
                        } else {
                            unimplemented!()
                        }
                    }
                    other => {
                        panic!("other = {:?}", other);
                    }
                }
            }
            eprintln!("--");
            for stmt in &stmts.stmts {
                eprintln!("{:?}", stmt);
            }
            fun_def.stmts = stmts;
            fun_def.variables = variables;
            *self.fun_defs.get_mut(fun_id).unwrap() = fun_def;
        }
    }
}
