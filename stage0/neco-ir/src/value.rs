use crate::*;
use neco_common::bitset::*;

#[derive(Debug, Clone)]
pub enum Value {
    Label(Id<Label>),
    Integer(i64),
    Float(String),
    /// bits, value
    I(i64, i64),
    /// (exponent bits, fraction bits), original string
    F((i64, i64), (i64, i64, BitSet)),
    True,
    False,
}

impl Value {
    pub fn string_to_float_value(exp_bits: i64, frac_bits: i64, s: &str) -> Value {
        eprintln!(
            "exp_bits = {}, frac_bits = {}, s = {}",
            exp_bits, frac_bits, s
        );
        assert!(exp_bits <= 40);
        // assert!(exp_bits == 11 && frac_bits == 52);
        let cs: Vec<_> = s.chars().collect();
        if cs.iter().all(|&c| c == '.' || c == '0') {
            return Value::F(
                (exp_bits, frac_bits),
                (1, 0, BitSet::new(frac_bits as usize)),
            );
        }
        let sign = if cs[0] == '-' { -1 } else { 1 };
        fn num_digits(x: u64) -> u64 {
            let mut res = 0;
            let mut x = x;
            while x != 0 {
                res += 1;
                x /= 10;
            }
            res
        }
        fn num_bits(x: u64) -> u64 {
            let mut res = 0;
            let mut x = x;
            while x != 0 {
                res += 1;
                x /= 2;
            }
            res
        }
        let mut upper10 = 0u64;
        let mut lower10 = 0u64;
        let mut i = 0;
        while i < cs.len() {
            if cs[i] == '.' {
                i += 1;
                break;
            }
            upper10 = upper10 * 10 + cs[i] as u64 - '0' as u64;
            i += 1;
        }
        while i < cs.len() {
            lower10 = lower10 * 10 + cs[i] as u64 - '0' as u64;
            i += 1;
        }
        let mut upper2: u64 = upper10;
        let mut lower2: BitSet = {
            let mut res = BitSet::new(frac_bits as usize);
            let mut v = lower10;
            let lower10_digits = num_digits(lower10);
            for i in 0..frac_bits {
                v *= 2;
                if num_digits(v) > lower10_digits {
                    res.set(i as usize);
                    v -= 10u64.pow(lower10_digits as u32);
                    // assert_eq!(num_digits(v), lower10_digits);
                }
            }
            res
        };
        let mut exp = 0;
        while upper2 >= 2 {
            lower2.shift_to_msb(1);
            if upper2 & 1 != 0 {
                lower2.set(0);
            }
            upper2 >>= 1;
            exp += 1;
        }
        while upper2 == 0 {
            upper2 <<= 1;
            if lower2.get(0) {
                upper2 |= 1;
            }
            lower2.shift_to_lsb(1);
            exp -= 1;
        }
        Value::F((exp_bits, frac_bits), (sign, exp, lower2))
    }
    pub fn to_x64_string(&self) -> String {
        match self {
            Value::Integer(v) => format!("{}", v),
            Value::I(_size, v) => format!("{}", v),
            Value::True => "1".to_string(),
            Value::False => "0".to_string(),
            Value::Label(_l) => unimplemented!(),
            Value::Float(s) => {
                // TODO check bit size
                Value::string_to_float_value(11, 52, s).to_x64_string()
            }
            Value::F((exp_bits, frac_bits), (sign, exp, frac)) => {
                assert!(1 + exp_bits + frac_bits <= 64);
                let mut res = 0u64;
                if *sign == -1 {
                    res |= 1u64 << 63;
                }
                // assert!(-1022 <= *exp && *exp <= 1023);
                // let exp = exp + 1023;
                assert!(
                    (-(2i64.pow(*exp_bits as u32 - 1)) + 2 <= *exp
                        && *exp <= 2i64.pow(*exp_bits as u32 - 1) - 1)
                );
                let exp = exp + 2i64.pow(*exp_bits as u32 - 1) - 1;
                for i in 0..*exp_bits {
                    if exp & (1 << i) != 0 {
                        res |= 1u64 << (i + frac_bits);
                    }
                }
                for i in 0..*frac_bits {
                    if frac.get(i as usize) {
                        res |= 1u64 << (frac_bits - i - 1);
                    }
                }
                format!("{}", res)
            }
        }
    }
    pub fn to_ptx_string(&self) -> String {
        match self {
            Value::Integer(v) => format!("{}", v),
            Value::I(_size, v) => format!("{}", v),
            Value::True => "1".to_string(),
            Value::False => "0".to_string(),
            Value::Label(_l) => unimplemented!(),
            Value::Float(s) => {
                // TODO check bit size
                Value::string_to_float_value(11, 52, s).to_x64_string()
            }
            Value::F((exp_bits, frac_bits), (sign, exp, frac)) => {
                assert!(1 + exp_bits + frac_bits <= 64);
                let mut res = 0u64;
                if *sign == -1 {
                    res |= 1u64 << 63;
                }
                // assert!(-1022 <= *exp && *exp <= 1023);
                // let exp = exp + 1023;
                assert!(
                    (-(2i64.pow(*exp_bits as u32 - 1)) + 2 <= *exp
                        && *exp <= 2i64.pow(*exp_bits as u32 - 1) - 1)
                );
                let exp = exp + 2i64.pow(*exp_bits as u32 - 1) - 1;
                for i in 0..*exp_bits {
                    if exp & (1 << i) != 0 {
                        res |= 1u64 << (i + frac_bits);
                    }
                }
                for i in 0..*frac_bits {
                    if frac.get(i as usize) {
                        res |= 1u64 << (frac_bits - i - 1);
                    }
                }
                format!("0F{:08X}", res)
            }
        }
    }
    pub fn ty(&self) -> Ty {
        match self {
            Value::Integer(_) => panic!(),
            Value::I(size, _) => Ty::I(*size),
            Value::True => Ty::Bool,
            Value::False => Ty::Bool,
            Value::Label(_l) => unimplemented!(),
            Value::Float(_) => panic!(),
            Value::F((expr_bits, frac_bits), _) => Ty::F(*expr_bits, *frac_bits),
        }
    }
}
