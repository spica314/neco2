use super::*;

#[derive(Debug, Clone)]
pub struct VariableTable {
    table: Table<VariableInfo>,
}

impl VariableTable {
    pub fn new() -> VariableTable {
        VariableTable {
            table: Table::new(),
        }
    }
    pub fn convert_integer_to(&mut self, ty: Ty) {
        for (_, v) in self.table.iter_mut() {
            if let Ty::Integer = v.ty {
                v.ty = ty.clone();
            }
        }
    }
    pub fn convert_float_to(&mut self, ty: Ty) {
        for (_, v) in self.table.iter_mut() {
            if let Ty::Float = v.ty {
                v.ty = ty.clone();
            }
        }
    }
    pub fn get_ty(&self, id: Id<VariableInfo>) -> Option<&Ty> {
        if let Some(variable) = self.table.get(id) {
            Some(&variable.ty)
        } else {
            None
        }
    }
    pub fn set_type(&mut self, id: Id<VariableInfo>, ty: Ty) -> Result<bool, ()> {
        if let Some(variable) = self.table.get_mut(id) {
            let variable_ty = variable.ty.clone();
            if let Ok(b) = Self::judge_update(&variable_ty, &ty) {
                if b {
                    variable.ty = ty;
                    Ok(true)
                } else {
                    Ok(false)
                }
            } else {
                Err(())
            }
        } else {
            panic!("no such variable: id = {:?}", id);
        }
    }
    /// 指定した3つのId<VariableInfo>が指す型が同じになるように推論
    pub fn same3(
        &mut self,
        id1: Id<VariableInfo>,
        id2: Id<VariableInfo>,
        id3: Id<VariableInfo>,
    ) -> Result<bool, ()> {
        let mut res = false;
        res |= self.same2(id1, id2)?;
        res |= self.same2(id2, id3)?;
        res |= self.same2(id3, id1)?;
        res |= self.same2(id1, id2)?;
        Ok(res)
    }
    pub fn judge_update(ty1: &Ty, ty2: &Ty) -> Result<bool, ()> {
        if ty1 == ty2 {
            Ok(false)
        } else if let (Ty::Integer, Ty::I(_)) = (ty1, ty2) {
            Ok(true)
        } else if let (Ty::I(_), Ty::Integer) = (ty1, ty2) {
            Ok(false)
        } else if let (Ty::Float, Ty::F(_, _)) = (ty1, ty2) {
            Ok(true)
        } else if let (Ty::F(_, _), Ty::Float) = (ty1, ty2) {
            Ok(false)
        } else if let (Ty::Ref(ty1_), Ty::Ref(ty2_)) = (ty1, ty2) {
            Self::judge_update(ty1_, ty2_)
        } else if let (Ty::Array(ty1_, size1), Ty::Array(ty2_, size2)) = (ty1, ty2) {
            if size1 != size2 {
                Err(())
            } else {
                Self::judge_update(ty1_, ty2_)
            }
        } else if ty1 == &Ty::Infer && ty2 != &Ty::Infer {
            Ok(true)
        } else if ty2 == &Ty::Infer {
            Ok(false)
        } else {
            unimplemented!("ty1 = {:?}, ty2 = {:?}", ty1, ty2);
        }
    }
    /// 指定した2つのId<VariableInfo>が指す型が同じになるように推論
    pub fn same2(&mut self, id1: Id<VariableInfo>, id2: Id<VariableInfo>) -> Result<bool, ()> {
        let mut res = false;
        if let Some(var2) = self.table.get(id2) {
            let var2_ty = var2.ty.clone();
            if let Some(var1) = self.table.get_mut(id1) {
                if let Ok(b) = Self::judge_update(&var1.ty, &var2_ty) {
                    if b {
                        var1.ty = var2_ty.clone();
                        res = true;
                    }
                } else {
                    return Err(());
                }
            } else {
                panic!("no such variable: id = {:?}", id1);
            }
        } else {
            panic!("no such variable: id = {:?}", id2);
        }
        if let Some(var1) = self.table.get(id1) {
            let var1_ty = var1.ty.clone();
            if let Some(var2) = self.table.get_mut(id2) {
                if let Ok(b) = Self::judge_update(&var2.ty, &var1_ty) {
                    if b {
                        var2.ty = var1_ty.clone();
                        res = true;
                    }
                } else {
                    return Err(());
                }
            } else {
                panic!("no such variable: id = {:?}", id2);
            }
        } else {
            panic!("no such variable: id = {:?}", id1);
        }
        Ok(res)
    }
    pub fn a_is_ref_of_b(
        &mut self,
        id1: Id<VariableInfo>,
        id2: Id<VariableInfo>,
    ) -> Result<bool, ()> {
        // rewrite id1
        if let Some(var2) = self.table.get(id2) {
            let var2_ty = var2.ty.clone();
            if let Some(var1) = self.table.get_mut(id1) {
                if let Ty::Ref(ty1_inner) = &var1.ty {
                    if let Ok(b) = Self::judge_update(&ty1_inner, &var2_ty) {
                        if b {
                            var1.ty = Ty::Ref(Box::new(var2_ty.clone()));
                            return Ok(true);
                        }
                    } else {
                        return Err(());
                    }
                // continue
                } else if var1.ty == Ty::Infer {
                    var1.ty = Ty::Ref(Box::new(var2_ty));
                    return Ok(true);
                } else {
                    return Err(());
                }
            } else {
                panic!("no such variable: id = {:?}", id1);
            }
        } else {
            panic!("no such variable: id = {:?}", id2);
        }
        // rewrite id2
        if let Some(var1) = self.table.get(id1) {
            let var1_ty = var1.ty.clone();
            if let Ty::Ref(ty1_inner) = var1_ty {
                if let Some(var2) = self.table.get_mut(id2) {
                    if let Ok(b) = Self::judge_update(&var2.ty, &ty1_inner) {
                        if b {
                            var2.ty = *ty1_inner.clone();
                            return Ok(true);
                        }
                        Ok(false)
                    } else {
                        Err(())
                    }
                } else {
                    panic!("no such variable: id = {:?}", id2);
                }
            } else {
                Ok(false)
            }
        } else {
            panic!("no such variable: id = {:?}", id1);
        }
    }
    pub fn a_is_ref_of_element_of_ref_array_b(
        &mut self,
        id1: Id<VariableInfo>,
        id2: Id<VariableInfo>,
    ) -> Result<bool, ()> {
        // rewrite id1
        if let Some(var2) = self.table.get(id2) {
            let var2_ty = var2.ty.clone();
            if let Some(var1) = self.table.get_mut(id1) {
                if let Ty::Ref(var1_inner) = &var1.ty {
                    if let Ty::Ref(var2_ty_inner) = &var2_ty {
                        if let Ty::Array(var2_ty_inner2, _size) = var2_ty_inner.as_ref() {
                            if let Ok(b) = Self::judge_update(var1_inner, var2_ty_inner2) {
                                if b {
                                    var1.ty = Ty::Ref(var2_ty_inner2.clone());
                                    return Ok(true);
                                }
                            } else {
                                return Err(());
                            }
                        } else {
                            // do nothing?
                        }
                    } else {
                        // do nothing
                    }
                } else if var1.ty == Ty::Infer {
                    if let Ty::Ref(var2_ty_inner) = &var2_ty {
                        if let Ty::Array(var2_ty_inner2, _size) = var2_ty_inner.as_ref() {
                            var1.ty = Ty::Ref(var2_ty_inner2.clone());
                            return Ok(true);
                        } else {
                            // do nothing?
                        }
                    } else {
                        // do nothing ?
                    }
                }
            } else {
                panic!("no such variable: id = {:?}", id1);
            }
        } else {
            panic!("no such variable: id = {:?}", id2);
        }
        // rewrite id2
        if let Some(var1) = self.table.get(id1) {
            let var1_ty = var1.ty.clone();
            if let Ty::Ref(var1_ty_inner) = var1_ty {
                if let Some(var2) = self.table.get_mut(id2) {
                    if let Ty::Ref(var2_ty_inner) = &var2.ty {
                        if let Ty::Array(var2_ty_inner2, _size) = var2_ty_inner.as_ref() {
                            if let Ok(b) = Self::judge_update(var2_ty_inner2, &var1_ty_inner) {
                                if b {
                                    var2.ty =
                                        Ty::Ref(Box::new(Ty::Array(var1_ty_inner.clone(), *_size)));
                                    return Ok(true);
                                }
                            }
                        }
                    }
                } else {
                    panic!("no such variable: id = {:?}", id2);
                }
            } else {
                // do nothing?
            }
        } else {
            panic!("no such variable: id = {:?}", id1);
        }
        Ok(false)
    }
}

impl AsRef<Table<VariableInfo>> for VariableTable {
    fn as_ref(&self) -> &Table<VariableInfo> {
        &self.table
    }
}

impl AsMut<Table<VariableInfo>> for VariableTable {
    fn as_mut(&mut self) -> &mut Table<VariableInfo> {
        &mut self.table
    }
}
