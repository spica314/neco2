mod config;
mod function_builder;
mod pass;
mod value;
mod variable_table;

use neco_common::table::*;
use std::collections::HashMap;

pub use config::*;
pub use function_builder::*;
pub use value::*;
use variable_table::*;

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct DataInfo {
    pub name: String,
    pub variants: Vec<Id<VariantInfo>>,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct VariantInfo {
    pub name: String,
    pub fields: Vec<(String, Ty)>,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Label {}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct VariableInfo {
    name: Option<String>,
    ty: Ty,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum ConstValue {
    ConstString(String),
}

/// IR
#[derive(Debug, Clone)]
pub struct IR {
    // module
    functions: Vec<Id<Function>>,
    data: Vec<Id<DataInfo>>,
    // table
    fun_defs: Table<Function>,
    data_table: Table<DataInfo>,
    variant_table: Table<VariantInfo>,
    data_of_variant: HashMap<Id<VariantInfo>, Id<DataInfo>>,
    consts: Table<ConstValue>,
    // config
    config: IRConfig,
}

impl Default for IR {
    fn default() -> Self {
        Self::new()
    }
}

impl IR {
    pub fn new() -> IR {
        IR::new_with_config(IRConfig::new())
    }
    pub fn new_with_config(config: IRConfig) -> IR {
        IR {
            functions: vec![],
            data: vec![],
            fun_defs: Table::new(),
            data_table: Table::new(),
            variant_table: Table::new(),
            data_of_variant: HashMap::new(),
            consts: Table::new(),
            config,
        }
    }
    pub fn push_function(&mut self, function: Function) {
        let id = self.fun_defs.insert(function);
        self.functions.push(id);
    }
    /// DataInfoを追加
    pub fn define_data(&mut self, data_def: DataInfo) -> Id<DataInfo> {
        let data_id = self.data_table.insert(data_def);
        for &variant_id in &self.data_table[data_id].variants {
            self.data_of_variant.insert(variant_id, data_id);
        }
        self.data.push(data_id);
        data_id
    }
    /// VariantInfoを追加
    pub fn define_variant(&mut self, variant_def: VariantInfo) -> Id<VariantInfo> {
        self.variant_table.insert(variant_def)
    }
    /// 必要なバイト数を返す
    pub fn memsize(&self, ty: &Ty) -> i64 {
        match ty {
            Ty::None => 0,
            Ty::I(i) => ((i + 7) / 8),
            Ty::U(i) => ((i + 7) / 8),
            Ty::Bool => 1,
            Ty::Tuple(ts) => {
                if ts.is_empty() {
                    0
                } else {
                    unimplemented!()
                }
            }
            Ty::Unit => 0,
            Ty::Data(data_id) => {
                let data_id = *data_id;
                // TODO: Variant i8 i64 takes 16 byte
                let data_def = &self.data_table[data_id];
                let mut max_variant_memsize = 0;
                for &variant_id in &data_def.variants {
                    let mut variant_memsize = 0;
                    for field in &self.variant_table[variant_id].fields {
                        variant_memsize += self.memsize(&field.1);
                    }
                    max_variant_memsize = std::cmp::max(max_variant_memsize, variant_memsize);
                }
                let res = max_variant_memsize + 8;
                eprintln!("item_id = {:?}, res = {}", data_id, res);
                res
            }
            Ty::Array(ty, len) => {
                let elem_size = self.memsize(ty);
                len * elem_size
            }
            Ty::Ref(_) => 8,
            Ty::Integer => {
                panic!("memsize of Ty::Integer");
            }
            Ty::Infer => panic!("memsize of Ty::Infer"),
            Ty::F(expr_bits, frac_bits) => {
                let bits = *expr_bits + *frac_bits + 1;
                assert!(bits % 8 == 0);
                bits / 8
            }
            Ty::Float => {
                panic!("memsize of Ty::Float");
            }
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum Ty {
    None,
    I(i64),
    U(i64),
    /// expr_bits, frac_bits,
    F(i64, i64),
    Float,
    Integer,
    Bool,
    Tuple(Vec<Ty>),
    Unit,
    Data(Id<DataInfo>),
    Array(Box<Ty>, i64),
    Ref(Box<Ty>),
    Infer,
}

impl Ty {
    pub fn is_integer(&self) -> bool {
        match self {
            Ty::I(_) => true,
            Ty::Integer => true,
            _ => false,
        }
    }
    pub fn is_float(&self) -> bool {
        match self {
            Ty::F(_, _) => true,
            Ty::Float => true,
            _ => false,
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum TargetArch {
    X64,
    Ptx,
}

#[derive(Debug, Clone)]
pub struct Function {
    name: String,
    args: Vec<Id<VariableInfo>>,
    ret_ty: Ty,
    stmts: IRStmts,
    label_table: Table<Label>,
    variables: VariableTable,
    inline: bool,
    target_arch: Option<TargetArch>,
}

#[derive(Debug, Clone)]
struct IRStmts {
    pub stmts: Vec<IRStmt>,
}

impl IRStmts {
    pub fn new() -> IRStmts {
        IRStmts { stmts: vec![] }
    }
    pub fn push(&mut self, stmt: IRStmt) {
        self.stmts.push(stmt);
    }
}

#[derive(Debug, Clone, Copy)]
pub enum X64Reg {
    RAX,
    EAX,
    RBP,
    RSP,
    RDI,
    RSI,
    RDX,
    AL,
    Xmm0,
    Xmm1,
    R10,
    R10D,
}

impl X64Reg {
    /// レジスタ名を返す
    pub fn as_str(&self) -> &str {
        match self {
            X64Reg::RAX => "rax",
            X64Reg::EAX => "eax",
            X64Reg::RBP => "rbp",
            X64Reg::RSP => "rsp",
            X64Reg::RDI => "rdi",
            X64Reg::RSI => "rsi",
            X64Reg::RDX => "rdx",
            X64Reg::AL => "al",
            X64Reg::Xmm0 => "xmm0",
            X64Reg::Xmm1 => "xmm1",
            X64Reg::R10 => "r10",
            X64Reg::R10D => "r10d",
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub enum IRInst {
    Eq,
    Ne,
    Lt,
    Le,
    Gt,
    Ge,
    Add,
    Sub,
    Mul,
    Div,
    Mod,
}

impl IRInst {
    pub fn is_arithmetic(&self) -> bool {
        match self {
            IRInst::Add => true,
            IRInst::Sub => true,
            IRInst::Mul => true,
            IRInst::Div => true,
            IRInst::Mod => true,
            _ => false,
        }
    }
    pub fn is_comparison(&self) -> bool {
        match self {
            IRInst::Eq => true,
            IRInst::Ne => true,
            IRInst::Lt => true,
            IRInst::Le => true,
            IRInst::Gt => true,
            IRInst::Ge => true,
            _ => false,
        }
    }
}

// TODO: consistent naming
/// prefix
/// - no prefix: general instruction
/// - X64: x64 (x86_64) instruction
///
/// postfix(type)
/// - I: Id<VariableInfo>
/// - V: Value
/// - R: Register(X64Reg)
/// - O: Offset
/// - L: Label
#[derive(Debug, Clone)]
pub enum IRStmt {
    /* scope, name */
    EnterScope,
    LeaveScope,
    LetWithName(Id<VariableInfo>, String),
    GetVariableByName(Id<VariableInfo>, String),
    GetReferenceByName(Id<VariableInfo>, String),
    CallByName(Id<VariableInfo>, String, Vec<Id<VariableInfo>>),

    /* mem */
    Let(Id<VariableInfo>),
    Release(Id<VariableInfo>),
    HeapAlloc(Id<VariableInfo>),
    HeapFree(Id<VariableInfo>),
    HeapAllocByName(String),

    /* value op (based on addr) */
    III(IRInst, Id<VariableInfo>, Id<VariableInfo>, Id<VariableInfo>),

    Ret(Id<VariableInfo>),
    Call(Id<VariableInfo>, Id<Function>, Vec<Id<VariableInfo>>),
    MovV(Id<VariableInfo>, Value),
    MovI(Id<VariableInfo>, Id<VariableInfo>),
    /// res = Constructor args
    Construct(Id<VariableInfo>, Id<VariantInfo>, Vec<Id<VariableInfo>>),
    ConstructByName(Id<VariableInfo>, String, Vec<Id<VariableInfo>>),
    /// let [.0] = .2 as .1
    Destruct(Vec<Id<VariableInfo>>, Id<VariantInfo>, Id<VariableInfo>),
    DestructByName(Vec<Id<VariableInfo>>, String, Id<VariableInfo>),

    /* addr op ? */
    // *p = **q
    Deref(Id<VariableInfo>, Id<VariableInfo>),
    // *p = &*q
    MakeRef(Id<VariableInfo>, Id<VariableInfo>),

    /* array? */
    /*
    /// .0 = .1[.2]
    ReadArray(Id<VariableInfo>, Id<VariableInfo>, Id<VariableInfo>),
    /// .0[.1] = .2
    WriteArray(Id<VariableInfo>, Id<VariableInfo>, Id<VariableInfo>),
    */
    /// .0 = &.1[.2]
    GetArrayElemAddr(Id<VariableInfo>, Id<VariableInfo>, Id<VariableInfo>),

    /* ? */
    // **p = *q
    MovRI(Id<VariableInfo>, Id<VariableInfo>),
    IfElse(Id<VariableInfo>, Id<Label>, Id<Label>),
    JmpL(Id<Label>),
    SetLabel(Id<Label>),
    /// match 0 { xs[i].0 <ignore> => label(xs[i].1) }
    MatchJmp(Id<VariableInfo>, Vec<(Id<VariantInfo>, Id<Label>)>),
    MatchJmpByName(Id<VariableInfo>, Vec<(String, Id<Label>)>),

    /* convert */
    ConvertItoI(Id<VariableInfo>, i64, i64, Id<VariableInfo>),
    ConvertFtoI(Id<VariableInfo>, (i64, i64), i64, Id<VariableInfo>),
    ConvertItoF(Id<VariableInfo>, i64, (i64, i64), Id<VariableInfo>),

    /* cuda */
    InitCUDA,
    CallCudaKernel(
        Id<Function>,
        (Id<VariableInfo>, i64),
        (usize, usize, usize),
        (usize, usize, usize),
    ),
    CallCudaKernelByName(String, String, (usize, usize, usize), (usize, usize, usize)),

    /* inst */
    X64Inst(X64Inst),
    PtxInst(PtxInst),
}

/// x86_64 instructions
#[derive(Debug, Clone)]
pub enum X64Inst {
    /// `add reg, reg`
    AddRR(X64Reg, X64Reg),
    /// `push reg`
    Push(X64Reg),
    /// `mov reg, value`
    MovRV(X64Reg, Value),
    /// `mov reg, reg`
    MovRR(X64Reg, X64Reg),
    /// `mov offset[reg], value`
    MovORV(i64, X64Reg, Value),
    /// `mov offset[reg], reg`
    MovORR(i64, X64Reg, X64Reg),
    /// `mov reg, offset[reg]`
    MovROR(X64Reg, i64, X64Reg),
    /// `mov ?[reg], value`
    MovIRV(Id<VariableInfo>, X64Reg, Value),
    /// `mov ?[reg], reg`
    MovIRR(Id<VariableInfo>, X64Reg, X64Reg),
    /// `mov reg, ?[reg]`
    MovRIR(X64Reg, Id<VariableInfo>, X64Reg),
    /// `mov ?[reg], value`
    MovIRVByName(String, X64Reg, Value),
    /// `mov ?[reg], reg`
    MovIRRByName(String, X64Reg, X64Reg),
    /// `mov reg, ?[reg]`
    MovRIRByName(X64Reg, String, X64Reg),
    /// `add reg, value`
    AddRV(X64Reg, i64),
    /// `sub reg, value`
    SubRV(X64Reg, i64),
    /// `sub reg, reg`
    SubRR(X64Reg, X64Reg),
    /// `imul reg, reg`
    ImulRR(X64Reg, X64Reg),
    /// `lea reg, offset[reg]`
    LeaROR(X64Reg, i64, X64Reg),
    /// `cmp reg, value`
    CmpRV(X64Reg, Value),
    /// `cmp reg, reg`
    CmpRR(X64Reg, X64Reg),
    /// `je label`
    JeL(Id<Label>),
    /// `jne label`
    JneL(Id<Label>),
    /// `jl label`
    JlL(Id<Label>),
    /// `jle label`
    JleL(Id<Label>),
    /// `jg label`
    JgL(Id<Label>),
    /// `jge label`
    JgeL(Id<Label>),
    /// `jmp label`
    JmpL(Id<Label>),
    /// `cqo`
    Cqo,
    /// `idiv reg`
    Idiv(X64Reg),
    /// `call label`
    Call(Id<Function>),
    /// `movsx reg, reg`
    Movsx(X64Reg, X64Reg),
    /// `pop reg`
    Pop(X64Reg),
    /// `ret`
    Ret,
    /// `syscall`
    Syscall,
    /// `movsd reg, ?[reg]`
    MovsdRIRByName(X64Reg, String, X64Reg),
    MovsdRIR(X64Reg, Id<VariableInfo>, X64Reg),
    MovsdROR(X64Reg, i64, X64Reg),
    MovsdORR(i64, X64Reg, X64Reg),

    MovssRIRByName(X64Reg, String, X64Reg),
    MovssRIR(X64Reg, Id<VariableInfo>, X64Reg),
    MovssROR(X64Reg, i64, X64Reg),
    MovssORR(i64, X64Reg, X64Reg),
    /// cvttsd2si m64 xmm
    Cvttsd2si(X64Reg, X64Reg),
    /// cvttss2si m64 xmm
    Cvttss2si(X64Reg, X64Reg),
    /// cvtsi2ssl xmm m64
    Cvtsi2ssl(X64Reg, X64Reg),
    /// `mulsd reg, reg`
    MulsdRR(X64Reg, X64Reg),
    /// `addsd reg, reg`
    AddsdRR(X64Reg, X64Reg),
    /// `subsd reg, reg`
    SubsdRR(X64Reg, X64Reg),
    /// `divsd reg, reg`
    DivsdRR(X64Reg, X64Reg),

    /// `mulsd reg, reg`
    MulssRR(X64Reg, X64Reg),
    /// `addsd reg, reg`
    AddssRR(X64Reg, X64Reg),
    /// `subsd reg, reg`
    SubssRR(X64Reg, X64Reg),
    /// `divsd reg, reg`
    DivssRR(X64Reg, X64Reg),
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum PtxTy {
    S32,
    U64,
    B32,
    B64,
    F32,
}

impl PtxTy {
    pub fn as_str(&self) -> &str {
        match self {
            PtxTy::S32 => "s32",
            PtxTy::U64 => "u64",
            PtxTy::B32 => "b32",
            PtxTy::B64 => "b64",
            PtxTy::F32 => "f32",
        }
    }
    pub fn memsize(&self) -> i64 {
        match self {
            PtxTy::S32 => 4,
            PtxTy::U64 => 8,
            PtxTy::B32 => 4,
            PtxTy::B64 => 8,
            PtxTy::F32 => 4,
        }
    }
    pub fn is_float(&self) -> bool {
        match self {
            PtxTy::F32 => true,
            _ => false,
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub enum PtxSpecialRegister {
    TidX,
    TidY,
    TidZ,
    NtidX,
    NtidY,
    NtidZ,
    CtaidX,
    CtaidY,
    CtaidZ,
}

impl PtxSpecialRegister {
    pub fn as_str(&self) -> &str {
        match self {
            PtxSpecialRegister::TidX => "%tid.x",
            PtxSpecialRegister::TidY => "%tid.y",
            PtxSpecialRegister::TidZ => "%tid.z",
            PtxSpecialRegister::NtidX => "%ntid.x",
            PtxSpecialRegister::NtidY => "%ntid.y",
            PtxSpecialRegister::NtidZ => "%ntid.z",
            PtxSpecialRegister::CtaidX => "%ctaid.x",
            PtxSpecialRegister::CtaidY => "%ctaid.y",
            PtxSpecialRegister::CtaidZ => "%ctaid.z",
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub enum PtxRoundingModifier {
    RN,
    RNA,
    RZ,
    RM,
    RP,
    RNI,
}

impl PtxRoundingModifier {
    pub fn as_str(&self) -> &str {
        match self {
            PtxRoundingModifier::RN => "rn",
            PtxRoundingModifier::RNA => "rna",
            PtxRoundingModifier::RZ => "rz",
            PtxRoundingModifier::RM => "rm",
            PtxRoundingModifier::RP => "rp",
            PtxRoundingModifier::RNI => "rni",
        }
    }
}

#[derive(Debug, Clone)]
pub enum PtxInst {
    /// ld.param.ty %a [param_b]
    LdParam(PtxTy, Id<VariableInfo>, Id<VariableInfo>),
    /// cvta.to.global.ty %a, %b
    CvtaToGlobal(PtxTy, Id<VariableInfo>, Id<VariableInfo>),
    /// ld.global.ty %a, [%b]
    LdGlobal(PtxTy, Id<VariableInfo>, Id<VariableInfo>),
    /// st.global.ty [%a], %b
    StGlobal(PtxTy, Id<VariableInfo>, Id<VariableInfo>),
    /// mov.ty %a, %b
    MovII(PtxTy, Id<VariableInfo>, Id<VariableInfo>),
    /// mov.ty %a, v
    MovIV(PtxTy, Id<VariableInfo>, Value),
    /// mov.ty %a, %special
    MovISpecial(PtxTy, Id<VariableInfo>, PtxSpecialRegister),
    /// add.ty %a, %b, %c
    AddIII(PtxTy, Id<VariableInfo>, Id<VariableInfo>, Id<VariableInfo>),
    /// sub.ty %a, %b, %c
    SubIII(PtxTy, Id<VariableInfo>, Id<VariableInfo>, Id<VariableInfo>),
    /// mul.ty %a, %b, %c
    MulIII(PtxTy, Id<VariableInfo>, Id<VariableInfo>, Id<VariableInfo>),
    /// div.ty %a, %b, %c
    DivIII(PtxTy, Id<VariableInfo>, Id<VariableInfo>, Id<VariableInfo>),
    /// rem.ty %a, %b, %c
    RemIII(PtxTy, Id<VariableInfo>, Id<VariableInfo>, Id<VariableInfo>),
    /// cvt.ty1.ty2 %r1, %r2
    CvtII(
        PtxTy,
        PtxTy,
        Option<PtxRoundingModifier>,
        Id<VariableInfo>,
        Id<VariableInfo>,
    ),
    /// ret
    Ret,
}
