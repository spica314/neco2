#[derive(Default, Debug, Clone)]
pub struct IRConfig {
    pub use_crt: bool,
}

impl IRConfig {
    pub fn new() -> IRConfig {
        IRConfig { use_crt: false }
    }
}
