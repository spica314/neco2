use crate::*;

#[derive(Debug, Clone)]
pub struct FunctionBuilder {
    name: String,
    args: Vec<Id<VariableInfo>>,
    ret_ty: Ty,
    stmts: IRStmts,
    label_table: Table<Label>,
    variables: VariableTable,
    inline: bool,
    target_arch: Option<TargetArch>,
    loop_labels: Vec<(Id<Label>, Id<Label>)>,
}

impl FunctionBuilder {
    pub fn new(name: &str, args: &[(&str, Ty)], ret_ty: Ty) -> FunctionBuilder {
        let mut variables = VariableTable::new();
        let mut args2 = vec![];
        for (arg_name, arg_ty) in args {
            let id = variables.as_mut().insert(VariableInfo {
                name: Some(arg_name.to_string()),
                ty: arg_ty.clone(),
            });
            args2.push(id);
        }
        FunctionBuilder {
            name: name.to_string(),
            args: args2,
            ret_ty,
            stmts: IRStmts::new(),
            label_table: Table::new(),
            variables,
            inline: false,
            target_arch: None,
            loop_labels: vec![],
        }
    }
    pub fn set_inline(&mut self, inline: bool) {
        self.inline = inline;
    }
    pub fn set_target_arch(&mut self, target_arch: TargetArch) {
        self.target_arch = Some(target_arch);
    }
    pub fn into_function(self) -> Function {
        Function {
            name: self.name,
            args: self.args,
            ret_ty: self.ret_ty,
            stmts: self.stmts,
            label_table: self.label_table,
            variables: self.variables,
            inline: self.inline,
            target_arch: self.target_arch,
        }
    }
    /// 名前を指定せずにlocal let，新規Id<VariableInfo>を返す
    pub fn stmt_let_anonymous(&mut self) -> Id<VariableInfo> {
        let id = self.variables.as_mut().insert(VariableInfo {
            name: None,
            ty: Ty::Infer,
        });
        self.stmts.push(IRStmt::Let(id));
        id
    }
    /// 名前を指定してlocal let，新規Id<VariableInfo>を返す
    pub fn stmt_let(&mut self, name: &str) -> Id<VariableInfo> {
        let id = self.variables.as_mut().insert(VariableInfo {
            name: Some(name.to_string()),
            ty: Ty::Infer,
        });
        self.stmts.push(IRStmt::LetWithName(id, name.to_string()));
        id
    }
    /// 新規ラベル作成
    pub fn new_label(&mut self) -> Id<Label> {
        self.label_table.insert(Label {})
    }
    /// `item_id`に対応する型を設定
    pub fn set_type(&mut self, item_id: Id<VariableInfo>, ty: Ty) -> Result<bool, ()> {
        self.variables.set_type(item_id, ty)
    }
    /// ir_stmtを現在編集中の関数に追記
    pub fn push_stmt_ir(&mut self, ir_stmt: IRStmt) {
        self.stmts.push(ir_stmt);
    }
    /// loopの開始地点・終了地点のlabelをpush
    pub fn push_loop_label(&mut self, loop_begin_label: Id<Label>, loop_end_label: Id<Label>) {
        self.loop_labels.push((loop_begin_label, loop_end_label));
    }
    /// loopの開始地点・終了地点のlabelをpop
    pub fn pop_loop_label(&mut self) {
        self.loop_labels.pop();
    }
    /// 現在のloopの開始地点を取得
    pub fn get_loop_begin_label(&self) -> Id<Label> {
        self.loop_labels.last().unwrap().0
    }
    /// 現在のloopの終了地点を取得
    pub fn get_loop_end_label(&self) -> Id<Label> {
        self.loop_labels.last().unwrap().1
    }
}
