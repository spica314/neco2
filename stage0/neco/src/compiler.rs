use neco_common::table::*;
use neco_ir::*;
use neco_syn::token::*;
use neco_syn::*;
use std::path::Path;

pub struct Compiler {
    ir: IR,
}

impl Default for Compiler {
    fn default() -> Self {
        Self::new()
    }
}

impl Compiler {
    pub fn new() -> Compiler {
        Compiler { ir: IR::new() }
    }
    pub fn new_with_config(config: IRConfig) -> Compiler {
        Compiler {
            ir: IR::new_with_config(config),
        }
    }
    pub fn load<P: AsRef<Path>>(&mut self, path: P) {
        let s = std::fs::read_to_string(path).unwrap();
        let mut stream = lex(&s);
        eprintln!("stream = {:?}", stream);
        let file: File = stream.parse().unwrap();
        eprintln!("file = {:#?}", file);
        if !stream.is_empty() {
            panic!("failed to parse");
        }
        // gen_ir_record_file(&mut self.ir, &file);
        gen_ir_file(&mut self.ir, &file);
    }
    pub fn resolve_name(&mut self) {
        self.ir.resolve_name();
    }
    pub fn convert_to_x64(&mut self) {
        self.ir.convert_to_x64();
    }
    pub fn convert_to_ptx(&mut self) {
        self.ir.convert_to_ptx();
    }
    pub fn infer_types(&mut self) {
        self.ir.infer_types();
    }
    pub fn emit_x64(&mut self) -> String {
        self.ir.emit_x64()
    }
    pub fn emit_ptx(&mut self) -> String {
        self.ir.emit_ptx()
    }
    pub fn inlining(&mut self) {
        self.ir.inlining();
    }
}

fn convert_type_to_ty(ty: &Type) -> Ty {
    match ty {
        Type::Verbatim(token) => {
            if token.as_str() == "i64" {
                Ty::I(64)
            } else if token.as_str() == "i32" {
                Ty::I(32)
            } else if token.as_str() == "i8" {
                Ty::I(8)
            } else if token.as_str() == "f32" {
                Ty::F(8, 23)
            } else {
                unimplemented!("token.as_str() == {:?}", token.as_str());
            }
        }
        Type::Reference(type_reference) => {
            let inner = convert_type_to_ty(&type_reference.elem);
            Ty::Ref(Box::new(inner))
        }
        Type::Array(type_array) => {
            let elem = convert_type_to_ty(&type_array.elem);
            let len = type_array.len.as_str().parse::<i64>().unwrap();
            Ty::Array(Box::new(elem), len)
        }
    }
}

fn gen_ir_file(ir: &mut IR, file: &File) {
    for item in &file.items {
        gen_ir_item(ir, item);
    }
}

fn gen_ir_item(ir: &mut IR, item: &Item) {
    match item {
        Item::ItemFn(item_fn) => {
            gen_ir_item_fn(ir, item_fn);
        }
        Item::ItemData(item_data) => {
            gen_ir_item_data(ir, item_data);
        }
    }
}

fn gen_ir_item_data(ir: &mut IR, item_data: &ItemData) {
    // // do nothing?
    let name = item_data.name.as_str().to_string();
    let mut variants = vec![];
    for variant in item_data.variants.lefts() {
        let name = variant.constructor.as_str().to_string();
        let mut fields = vec![];
        for field in &variant.fields {
            let s = field.pat.as_str().to_string();
            let ty = convert_type_to_ty(&field.ty);
            fields.push((s, ty));
        }
        let item_id = ir.define_variant(VariantInfo { name, fields });
        variants.push(item_id);
    }
    let data_def = DataInfo { name, variants };
    ir.define_data(data_def);
}

fn gen_ir_item_fn(ir: &mut IR, item_fn: &ItemFn) {
    let ret_ty = if let Some(ret_ty) = &item_fn.ret_ty {
        convert_type_to_ty(&ret_ty.1)
    } else {
        Ty::Unit
    };
    let mut args = vec![];
    for arg in &item_fn.inputs {
        match arg {
            FnArg::Unit(_) => {
                args.push(("", Ty::Unit));
            }
            FnArg::Typed(pat_type) => {
                let ty = convert_type_to_ty(&pat_type.ty);
                args.push((pat_type.pat.as_str(), ty));
            }
        }
    }
    if args.is_empty() {
        args = vec![("", Ty::Unit)];
    }
    let mut builder = FunctionBuilder::new(&item_fn.name.as_str(), &args, ret_ty);

    for attr in &item_fn.attrs {
        let s = attr.lit.as_str();
        if s == "inline" {
            builder.set_inline(true);
        } else if s == "ptx" {
            builder.set_target_arch(TargetArch::Ptx);
        } else if s.starts_with("__") {
        } else {
            panic!();
        }
    }

    let res2 = builder.stmt_let_anonymous();
    builder.push_stmt_ir(IRStmt::EnterScope);
    let res = gen_ir_expr_value(ir, &mut builder, &item_fn.expr.as_ref());
    builder.push_stmt_ir(IRStmt::MovI(res2, res));
    builder.push_stmt_ir(IRStmt::LeaveScope);
    builder.push_stmt_ir(IRStmt::Ret(res2));

    ir.push_function(builder.into_function());
}

fn gen_ir_expr_value(ir: &mut IR, builder: &mut FunctionBuilder, expr: &Expr) -> Id<VariableInfo> {
    match expr {
        Expr::ExprBlock(expr_block) => gen_ir_expr_block_value(ir, builder, expr_block),
        Expr::If(expr_if) => gen_ir_expr_if_value(ir, builder, expr_if),
        Expr::ExprCall(expr_call) => gen_ir_expr_call_value(ir, builder, expr_call),
        Expr::ExprLit(expr_lit) => gen_ir_expr_lit_value(ir, builder, expr_lit),
        Expr::ExprUnit(expr_unit) => gen_ir_expr_unit_value(ir, builder, expr_unit),
        Expr::ExprTuple(expr_tuple) => gen_ir_expr_tuple_value(ir, builder, expr_tuple),
        Expr::ExprAssign(expr_assign) => gen_ir_expr_assign_value(ir, builder, expr_assign),
        Expr::Paren(expr_paren) => gen_ir_expr_paren_value(ir, builder, expr_paren),
        Expr::Binary(expr_binary) => gen_ir_expr_binary_value(ir, builder, expr_binary),
        Expr::Match(expr_match) => gen_ir_expr_match_value(ir, builder, expr_match),
        Expr::Data(expr_data) => gen_ir_expr_data_value(ir, builder, expr_data),
        Expr::Unary(expr_unary) => gen_ir_expr_unary_value(ir, builder, expr_unary),
        Expr::Index(expr_index) => gen_ir_expr_index_value(ir, builder, expr_index),
        Expr::Repeat(expr_repeat) => gen_ir_expr_repeat_value(ir, builder, expr_repeat),
        Expr::Loop(expr_loop) => gen_ir_expr_loop_value(ir, builder, expr_loop),
        Expr::Break(expr_break) => gen_ir_expr_break_value(ir, builder, expr_break),
    }
}

fn gen_ir_expr_block_value(
    ir: &mut IR,
    builder: &mut FunctionBuilder,
    expr_block: &Block,
) -> Id<VariableInfo> {
    // 返すitem_idはスコープの外でletされなければならない
    let res = builder.stmt_let_anonymous();
    builder.push_stmt_ir(IRStmt::EnterScope);
    let mut last_expr = None;
    for stmt in &expr_block.stmts {
        last_expr = gen_ir_stmt_value(ir, builder, stmt);
    }
    if let Some(item_id) = last_expr {
        builder.push_stmt_ir(IRStmt::MovI(res, item_id));
        builder.push_stmt_ir(IRStmt::LeaveScope);
        res
    } else {
        let item_id = builder.stmt_let_anonymous();
        builder.set_type(item_id, Ty::Unit).unwrap();
        builder.push_stmt_ir(IRStmt::MovI(res, item_id));
        builder.push_stmt_ir(IRStmt::LeaveScope);
        res
    }
}

fn gen_ir_expr_break_value(
    ir: &mut IR,
    builder: &mut FunctionBuilder,
    expr_break: &ExprBreak,
) -> Id<VariableInfo> {
    let end_label = builder.get_loop_end_label();
    builder.push_stmt_ir(IRStmt::JmpL(end_label));

    let res = builder.stmt_let_anonymous();
    builder.set_type(res, Ty::Unit).unwrap();
    res
}

fn gen_ir_expr_loop_value(
    ir: &mut IR,
    builder: &mut FunctionBuilder,
    expr_loop: &ExprLoop,
) -> Id<VariableInfo> {
    let loop_begin_label = builder.new_label();
    let loop_end_label = builder.new_label();

    builder.push_stmt_ir(IRStmt::SetLabel(loop_begin_label));
    builder.push_loop_label(loop_begin_label, loop_end_label);
    for stmt in &expr_loop.body.stmts {
        gen_ir_stmt_value(ir, builder, stmt);
    }
    builder.push_stmt_ir(IRStmt::JmpL(loop_begin_label));
    builder.pop_loop_label();
    builder.push_stmt_ir(IRStmt::SetLabel(loop_end_label));

    let res = builder.stmt_let_anonymous();
    builder.set_type(res, Ty::Unit).unwrap();
    res
}

fn gen_ir_expr_if_value(
    ir: &mut IR,
    builder: &mut FunctionBuilder,
    expr_if: &ExprIf,
) -> Id<VariableInfo> {
    let res = builder.stmt_let_anonymous();
    let label1 = builder.new_label();
    let label2 = builder.new_label();
    let label3 = builder.new_label();
    let cond = gen_ir_expr_value(ir, builder, &expr_if.cond);
    builder.push_stmt_ir(IRStmt::IfElse(cond, label1, label2));
    builder.push_stmt_ir(IRStmt::SetLabel(label1));
    builder.push_stmt_ir(IRStmt::EnterScope);
    let then_res = gen_ir_expr_block_value(ir, builder, &expr_if.then_branch);
    builder.push_stmt_ir(IRStmt::MovI(res, then_res));
    builder.push_stmt_ir(IRStmt::JmpL(label3));
    builder.push_stmt_ir(IRStmt::LeaveScope);
    builder.push_stmt_ir(IRStmt::SetLabel(label2));
    builder.push_stmt_ir(IRStmt::EnterScope);
    let else_res = gen_ir_expr_value(ir, builder, &expr_if.else_branch.1);
    builder.push_stmt_ir(IRStmt::MovI(res, else_res));
    builder.push_stmt_ir(IRStmt::LeaveScope);
    builder.push_stmt_ir(IRStmt::SetLabel(label3));
    res
}

fn expr_to_one_builtin_s(expr: &Expr) -> Option<&str> {
    match expr {
        Expr::ExprLit(expr_lit) => match &expr_lit.lit {
            Lit::Builtin(token) => Some(&token.as_str()),
            _ => None,
        },
        _ => None,
    }
}

fn expr_to_one_integer(expr: &Expr) -> Option<i64> {
    match expr {
        Expr::ExprLit(expr_lit) => match &expr_lit.lit {
            Lit::Num(token) => {
                if let Ok(v) = token.as_str().parse::<i64>() {
                    Some(v)
                } else {
                    None
                }
            }
            _ => None,
        },
        _ => None,
    }
}

fn expr_match_ref_ident(expr: &Expr) -> Option<&str> {
    match expr {
        Expr::Unary(expr_unary) => match expr_unary.op {
            UnOp::Ref(_) => match expr_unary.expr.as_ref() {
                Expr::ExprLit(expr_lit) => match &expr_lit.lit {
                    Lit::Verbatim(token) => Some(&token.s),
                    _ => None,
                },
                _ => None,
            },
            _ => None,
        },
        _ => None,
    }
}

fn expr_match_ident(expr: &Expr) -> Option<&str> {
    match expr {
        Expr::ExprLit(expr_lit) => match &expr_lit.lit {
            Lit::Verbatim(token) => Some(&token.s),
            _ => None,
        },
        _ => None,
    }
}

fn parse_builtin_x64_register(expr: &Expr) -> Option<X64Reg> {
    if let Some(s) = expr_to_one_builtin_s(expr) {
        let r = match s {
            "__rax" => X64Reg::RAX,
            "__eax" => X64Reg::EAX,
            "__rdi" => X64Reg::RDI,
            "__rsi" => X64Reg::RSI,
            "__rdx" => X64Reg::RDX,
            "__xmm0" => X64Reg::Xmm0,
            _ => panic!(),
        };
        Some(r)
    } else {
        None
    }
}

fn gen_ir_expr_call_value(
    ir: &mut IR,
    builder: &mut FunctionBuilder,
    expr_call: &ExprCall,
) -> Id<VariableInfo> {
    match &expr_call.f {
        Lit::Builtin(f) => {
            let arg0_reg = parse_builtin_x64_register(&expr_call.args[0]);
            let arg1_reg = if expr_call.args.len() >= 2 {
                parse_builtin_x64_register(&expr_call.args[1])
            } else {
                None
            };
            if f.as_str() == "__i32_to_f32" {
                let arg = gen_ir_expr_value(ir, builder, &expr_call.args[0]);
                let res = builder.stmt_let_anonymous();
                builder.push_stmt_ir(IRStmt::ConvertItoF(res, 32, (8, 23), arg));
                return res;
            }
            if f.as_str() == "__f32_to_i32" {
                let arg = gen_ir_expr_value(ir, builder, &expr_call.args[0]);
                let res = builder.stmt_let_anonymous();
                builder.push_stmt_ir(IRStmt::ConvertFtoI(res, (8, 23), 32, arg));
                return res;
            }
            if f.as_str() == "__i32_to_i8" {
                let arg = gen_ir_expr_value(ir, builder, &expr_call.args[0]);
                let res = builder.stmt_let_anonymous();
                builder.push_stmt_ir(IRStmt::ConvertItoI(res, 32, 8, arg));
                return res;
            }
            if f.as_str() == "__i64_to_i8" {
                let arg = gen_ir_expr_value(ir, builder, &expr_call.args[0]);
                let res = builder.stmt_let_anonymous();
                builder.push_stmt_ir(IRStmt::ConvertItoI(res, 64, 8, arg));
                return res;
            }
            if f.as_str() == "__i8_to_i32" {
                let arg = gen_ir_expr_value(ir, builder, &expr_call.args[0]);
                let res = builder.stmt_let_anonymous();
                builder.push_stmt_ir(IRStmt::ConvertItoI(res, 8, 32, arg));
                return res;
            }
            if f.as_str() == "__i32_to_i64" {
                let arg = gen_ir_expr_value(ir, builder, &expr_call.args[0]);
                let res = builder.stmt_let_anonymous();
                builder.push_stmt_ir(IRStmt::ConvertItoI(res, 32, 64, arg));
                return res;
            }
            if f.as_str() == "__mov" {
                assert!(expr_call.args.len() == 2);
                if let Some(reg0) = arg0_reg {
                    if let Some(v) = expr_to_one_integer(&expr_call.args[1]) {
                        builder
                            .push_stmt_ir(IRStmt::X64Inst(X64Inst::MovRV(reg0, Value::I(64, v))));
                    } else if let Some(ident_s) = expr_match_ident(&expr_call.args[1]) {
                        builder.push_stmt_ir(IRStmt::X64Inst(X64Inst::MovRIRByName(
                            reg0,
                            ident_s.to_string(),
                            X64Reg::RBP,
                        )));
                    } else if let Some(ident_s) = expr_match_ref_ident(&expr_call.args[1]) {
                        builder.push_stmt_ir(IRStmt::X64Inst(X64Inst::MovRIRByName(
                            reg0,
                            ident_s.to_string(),
                            X64Reg::RBP,
                        )));
                    } else {
                        unimplemented!();
                    }
                } else if let Some(ident_s) = expr_match_ref_ident(&expr_call.args[0]) {
                    if let Some(v) = expr_to_one_integer(&expr_call.args[1]) {
                        builder.push_stmt_ir(IRStmt::X64Inst(X64Inst::MovIRVByName(
                            ident_s.to_string(),
                            X64Reg::RBP,
                            Value::I(64, v),
                        )));
                    } else if let Some(reg1) = arg1_reg {
                        builder.push_stmt_ir(IRStmt::X64Inst(X64Inst::MovIRRByName(
                            ident_s.to_string(),
                            X64Reg::RBP,
                            reg1,
                        )));
                    } else {
                        unimplemented!();
                    }
                } else {
                    unimplemented!();
                }
            } else if f.as_str() == "__syscall" {
                assert!(expr_call.args.len() == 1);
                // TODO: check args[0] == ()
                builder.push_stmt_ir(IRStmt::X64Inst(X64Inst::Syscall));
            } else if f.as_str() == "__movsd" {
                if let Some(reg0) = arg0_reg {
                    if let Some(ident_s) = expr_match_ref_ident(&expr_call.args[1]) {
                        builder.push_stmt_ir(IRStmt::X64Inst(X64Inst::MovsdRIRByName(
                            reg0,
                            ident_s.to_string(),
                            X64Reg::RBP,
                        )));
                    } else {
                        unimplemented!();
                    }
                } else {
                    panic!();
                }
            } else if f.as_str() == "__movss" {
                if let Some(reg) = arg0_reg {
                    if let Some(ident_s) = expr_match_ref_ident(&expr_call.args[1]) {
                        builder.push_stmt_ir(IRStmt::X64Inst(X64Inst::MovssRIRByName(
                            reg,
                            ident_s.to_string(),
                            X64Reg::RBP,
                        )));
                    } else {
                        unimplemented!();
                    }
                }
            } else if f.as_str() == "__cvttsv2si" {
                if let Some(reg0) = arg0_reg {
                    if let Some(reg1) = arg1_reg {
                        builder.push_stmt_ir(IRStmt::X64Inst(X64Inst::Cvttsd2si(reg0, reg1)));
                    } else {
                        panic!();
                    }
                } else {
                    panic!();
                }
            } else if f.as_str() == "__cvttss2si" {
                if let Some(reg0) = arg0_reg {
                    if let Some(reg1) = arg1_reg {
                        builder.push_stmt_ir(IRStmt::X64Inst(X64Inst::Cvttss2si(reg0, reg1)));
                    } else {
                        panic!();
                    }
                } else {
                    panic!();
                }
            } else if f.as_str() == "__system_alloc" {
                if let Some(ident_s) = expr_match_ref_ident(&expr_call.args[0]) {
                    builder.push_stmt_ir(IRStmt::HeapAllocByName(ident_s.to_string()));
                } else {
                    panic!();
                }
            } else if f.as_str() == "__init_cuda" {
                builder.push_stmt_ir(IRStmt::InitCUDA);
            } else if f.as_str() == "__call_cuda" {
                assert!(expr_call.args.len() == 8);
                let f_s = expr_match_ident(&expr_call.args[0]).unwrap();
                let arg_s = expr_match_ref_ident(&expr_call.args[1]).unwrap();
                let grid_x = expr_to_one_integer(&expr_call.args[2]).unwrap() as usize;
                let grid_y = expr_to_one_integer(&expr_call.args[3]).unwrap() as usize;
                let grid_z = expr_to_one_integer(&expr_call.args[4]).unwrap() as usize;
                let grid = (grid_x, grid_y, grid_z);
                let block_x = expr_to_one_integer(&expr_call.args[5]).unwrap() as usize;
                let block_y = expr_to_one_integer(&expr_call.args[6]).unwrap() as usize;
                let block_z = expr_to_one_integer(&expr_call.args[7]).unwrap() as usize;
                let block = (block_x, block_y, block_z);
                builder.push_stmt_ir(IRStmt::CallCudaKernelByName(
                    f_s.to_string(),
                    arg_s.to_string(),
                    grid,
                    block,
                ));
            } else {
                unimplemented!("f = {:?}", f);
            }
            let item_id = builder.stmt_let_anonymous();
            builder.set_type(item_id, Ty::Unit).unwrap();
            item_id
        }
        Lit::Verbatim(f) => {
            let mut arg_item_ids = vec![];
            for arg in &expr_call.args {
                arg_item_ids.push(gen_ir_expr_value(ir, builder, arg));
            }
            let item_id = builder.stmt_let_anonymous();
            builder.push_stmt_ir(IRStmt::CallByName(item_id, f.s.clone(), arg_item_ids));
            item_id
        }
        Lit::Num(_) => {
            panic!("cannot apply to num");
        }
    }
}

fn analyze_num_lit(s: &str) -> Value {
    let mut res = 0;
    let cs: Vec<char> = s.chars().collect();
    if cs.iter().any(|&c| c == '.') {
        let mut s = String::new();
        let mut i = 0;
        while i < cs.len() {
            if cs[i] != 'f' {
                s.push(cs[i]);
                i += 1;
                continue;
            }
            assert!(cs[i] == 'f');
            i += 1;
            let mut size = 0;
            while i < cs.len() {
                assert!(cs[i].is_ascii_digit());
                size = 10 * size + cs[i] as i64 - '0' as i64;
                i += 1;
            }
            if size == 32 {
                return Value::string_to_float_value(8, 23, &s);
            } else if size == 64 {
                return Value::string_to_float_value(11, 52, &s);
            } else {
                panic!();
            }
        }
        return Value::Float(s.to_string());
    }
    let mut i = 0;
    while i < cs.len() {
        let c = cs[i];
        if c.is_ascii_digit() {
            let v = c as i64 - '0' as i64;
            res = 10 * res + v;
        } else if c == 'i' {
            i += 1;
            let mut size = 0;
            while i < cs.len() {
                let c = cs[i];
                if c.is_ascii_digit() {
                    let v = c as i64 - '0' as i64;
                    size = 10 * size + v;
                } else {
                    panic!();
                }
                i += 1;
            }
            return Value::I(size, res);
        } else {
            panic!("s = {}", s);
        }
        i += 1;
    }
    Value::Integer(res)
}

fn match_builtin_ptx_special_register(s: &str) -> Option<PtxSpecialRegister> {
    match s {
        "__tid_x" => Some(PtxSpecialRegister::TidX),
        "__tid_y" => Some(PtxSpecialRegister::TidY),
        "__tid_z" => Some(PtxSpecialRegister::TidZ),
        "__ntid_x" => Some(PtxSpecialRegister::NtidX),
        "__ntid_y" => Some(PtxSpecialRegister::NtidY),
        "__ntid_z" => Some(PtxSpecialRegister::NtidZ),
        "__ctaid_x" => Some(PtxSpecialRegister::CtaidX),
        "__ctaid_y" => Some(PtxSpecialRegister::CtaidY),
        "__ctaid_z" => Some(PtxSpecialRegister::CtaidZ),
        _ => None,
    }
}

fn gen_ir_expr_lit_value(
    _ir: &mut IR,
    builder: &mut FunctionBuilder,
    expr_lit: &ExprLit,
) -> Id<VariableInfo> {
    match &expr_lit.lit {
        Lit::Num(num) => {
            let item_id = builder.stmt_let_anonymous();
            let v = analyze_num_lit(num.as_str());
            builder.push_stmt_ir(IRStmt::MovV(item_id, v));
            item_id
        }
        Lit::Builtin(lit_builtin) => {
            let s = lit_builtin.as_str();
            if let Some(special) = match_builtin_ptx_special_register(s) {
                let res = builder.stmt_let_anonymous();
                builder.push_stmt_ir(IRStmt::PtxInst(PtxInst::MovISpecial(
                    PtxTy::S32,
                    res,
                    special,
                )));
                res
            } else {
                panic!("expr_lit = {:?}", expr_lit);
            }
        }
        Lit::Verbatim(token) => {
            let res = builder.stmt_let_anonymous();
            builder.push_stmt_ir(IRStmt::GetVariableByName(res, token.s.clone()));
            res
        }
    }
}

fn gen_ir_expr_unit_value(
    _ir: &mut IR,
    builder: &mut FunctionBuilder,
    _expr_unit: &ExprUnit,
) -> Id<VariableInfo> {
    let item_id = builder.stmt_let_anonymous();
    builder.set_type(item_id, Ty::Unit).unwrap();
    item_id
}

fn gen_ir_expr_tuple_value(
    _ir: &mut IR,
    builder: &mut FunctionBuilder,
    expr_tuple: &ExprTuple,
) -> Id<VariableInfo> {
    if !expr_tuple.exprs.is_empty() {
        unimplemented!()
    }
    let item_id = builder.stmt_let_anonymous();
    builder.set_type(item_id, Ty::Tuple(vec![])).unwrap();
    item_id
}

fn gen_ir_expr_assign_value(
    ir: &mut IR,
    builder: &mut FunctionBuilder,
    expr_assign: &ExprAssign,
) -> Id<VariableInfo> {
    let left_item_id = gen_ir_expr_addr(ir, builder, &expr_assign.left);
    let right_item_id = gen_ir_expr_value(ir, builder, &expr_assign.right);
    builder.push_stmt_ir(IRStmt::MovRI(left_item_id, right_item_id));
    // unit
    let item_id = builder.stmt_let_anonymous();
    builder.set_type(item_id, Ty::Unit).unwrap();
    item_id
}

fn gen_ir_stmt_value(
    ir: &mut IR,
    builder: &mut FunctionBuilder,
    stmt: &Stmt,
) -> Option<Id<VariableInfo>> {
    match stmt {
        Stmt::Local(local) => {
            if let Some((_, init_expr)) = &local.init {
                let init_value = gen_ir_expr_value(ir, builder, &init_expr);
                let id = builder.stmt_let(&local.ident.as_str());
                builder.push_stmt_ir(IRStmt::MovI(id, init_value));
            } else {
                let id = builder.stmt_let(&local.ident.as_str());
            }
            None
        }
        Stmt::StmtSemi(stmt_semi) => {
            gen_ir_expr_value(ir, builder, &stmt_semi.expr);
            None
        }
        Stmt::Expr(expr) => {
            let item_id = gen_ir_expr_value(ir, builder, &expr);
            Some(item_id)
        }
    }
}

fn gen_ir_expr_paren_value(
    ir: &mut IR,
    builder: &mut FunctionBuilder,
    expr_paren: &ExprParen,
) -> Id<VariableInfo> {
    gen_ir_expr_value(ir, builder, &expr_paren.expr)
}

fn gen_ir_expr_binary_value(
    ir: &mut IR,
    builder: &mut FunctionBuilder,
    expr_binary: &ExprBinary,
) -> Id<VariableInfo> {
    let left_item_id = gen_ir_expr_value(ir, builder, &expr_binary.left);
    let right_item_id = gen_ir_expr_value(ir, builder, &expr_binary.right);
    let res = builder.stmt_let_anonymous();
    match expr_binary.op {
        BinOp::Add(_) => {
            builder.push_stmt_ir(IRStmt::III(IRInst::Add, res, left_item_id, right_item_id))
        }
        BinOp::Sub(_) => {
            builder.push_stmt_ir(IRStmt::III(IRInst::Sub, res, left_item_id, right_item_id))
        }
        BinOp::Mul(_) => {
            builder.push_stmt_ir(IRStmt::III(IRInst::Mul, res, left_item_id, right_item_id))
        }
        BinOp::Div(_) => {
            builder.push_stmt_ir(IRStmt::III(IRInst::Div, res, left_item_id, right_item_id))
        }
        BinOp::Rem(_) => {
            builder.push_stmt_ir(IRStmt::III(IRInst::Mod, res, left_item_id, right_item_id))
        }
        BinOp::Eq(_) => {
            builder.push_stmt_ir(IRStmt::III(IRInst::Eq, res, left_item_id, right_item_id))
        }
        BinOp::Ne(_) => {
            builder.push_stmt_ir(IRStmt::III(IRInst::Ne, res, left_item_id, right_item_id))
        }
        BinOp::Lt(_) => {
            builder.push_stmt_ir(IRStmt::III(IRInst::Lt, res, left_item_id, right_item_id))
        }
        BinOp::Le(_) => {
            builder.push_stmt_ir(IRStmt::III(IRInst::Le, res, left_item_id, right_item_id))
        }
        BinOp::Gt(_) => {
            builder.push_stmt_ir(IRStmt::III(IRInst::Gt, res, left_item_id, right_item_id))
        }
        BinOp::Ge(_) => {
            builder.push_stmt_ir(IRStmt::III(IRInst::Ge, res, left_item_id, right_item_id))
        }
    }
    res
}

fn gen_ir_expr_unary_value(
    ir: &mut IR,
    builder: &mut FunctionBuilder,
    expr_unary: &ExprUnary,
) -> Id<VariableInfo> {
    match expr_unary.op {
        UnOp::Neg(_) => {
            let item_id = gen_ir_expr_value(ir, builder, &expr_unary.expr);
            let t1 = builder.stmt_let_anonymous();
            let t2 = builder.stmt_let_anonymous();
            builder.push_stmt_ir(IRStmt::MovV(t1, Value::Integer(-1)));
            builder.push_stmt_ir(IRStmt::III(IRInst::Mul, t2, item_id, t1));
            t2
        }
        UnOp::Ref(_) => {
            let t2 = gen_ir_expr_addr(ir, builder, &expr_unary.expr);
            t2
        }
        UnOp::Deref(_) => {
            let t1 = builder.stmt_let_anonymous();
            let t2 = gen_ir_expr_value(ir, builder, &expr_unary.expr);
            builder.push_stmt_ir(IRStmt::Deref(t1, t2));
            t1
        }
    }
}

fn gen_ir_expr_match_value(
    ir: &mut IR,
    builder: &mut FunctionBuilder,
    expr_match: &ExprMatch,
) -> Id<VariableInfo> {
    let mut pairs_of_variant_and_label = vec![];
    for arm in &expr_match.arms {
        let label = builder.new_label();
        let name = format!(
            "{}::{}",
            arm.data_name.as_str(),
            arm.constructor_name.as_str()
        );
        pairs_of_variant_and_label.push((name, label));
    }
    let arg = gen_ir_expr_value(ir, builder, &expr_match.expr);
    let res = builder.stmt_let_anonymous();
    builder.push_stmt_ir(IRStmt::MatchJmpByName(
        arg,
        pairs_of_variant_and_label.clone(),
    ));
    let fin_label = builder.new_label();
    for (arm, (variant, label)) in expr_match
        .arms
        .iter()
        .zip(pairs_of_variant_and_label.iter())
    {
        builder.push_stmt_ir(IRStmt::SetLabel(*label));
        builder.push_stmt_ir(IRStmt::EnterScope);
        let mut args = vec![];
        for elem in &arm.elems {
            let item_id = builder.stmt_let(elem.as_str());
            args.push(item_id);
        }
        builder.push_stmt_ir(IRStmt::DestructByName(args, variant.clone(), arg));
        let expr = gen_ir_expr_value(ir, builder, &arm.expr);
        builder.push_stmt_ir(IRStmt::MovI(res, expr));
        builder.push_stmt_ir(IRStmt::LeaveScope);
        builder.push_stmt_ir(IRStmt::JmpL(fin_label));
    }
    builder.push_stmt_ir(IRStmt::SetLabel(fin_label));
    res
}

fn gen_ir_expr_data_value(
    ir: &mut IR,
    builder: &mut FunctionBuilder,
    expr_data: &ExprData,
) -> Id<VariableInfo> {
    let name = format!(
        "{}::{}",
        expr_data.data_name.as_str(),
        expr_data.constructor_name.as_str()
    );
    let mut args = vec![];
    for arg in &expr_data.args {
        args.push(gen_ir_expr_value(ir, builder, arg));
    }
    let res = builder.stmt_let_anonymous();
    builder.push_stmt_ir(IRStmt::ConstructByName(res, name, args));
    res
}

fn gen_ir_expr_index_value(
    ir: &mut IR,
    builder: &mut FunctionBuilder,
    expr_index: &ExprIndex,
) -> Id<VariableInfo> {
    let res = builder.stmt_let_anonymous();
    let expr = gen_ir_expr_addr(ir, builder, &expr_index.expr);
    let index = gen_ir_expr_value(ir, builder, &expr_index.index);
    builder.push_stmt_ir(IRStmt::GetArrayElemAddr(res, expr, index));
    let res2 = builder.stmt_let_anonymous();
    builder.push_stmt_ir(IRStmt::Deref(res2, res));
    res2
}

fn gen_ir_expr_repeat_value(
    ir: &mut IR,
    builder: &mut FunctionBuilder,
    expr_repeat: &ExprRepeat,
) -> Id<VariableInfo> {
    let res = builder.stmt_let_anonymous();
    let len = match expr_repeat.len.as_ref() {
        Lit::Num(lit_num) => lit_num.as_str().parse::<i64>().unwrap(),
        _ => panic!(),
    };
    builder
        .set_type(res, Ty::Array(Box::new(Ty::Infer), len))
        .unwrap();
    let v = gen_ir_expr_value(ir, builder, &expr_repeat.expr);
    let ref_res = builder.stmt_let_anonymous();
    builder.push_stmt_ir(IRStmt::MakeRef(ref_res, res));

    let counter = builder.stmt_let_anonymous();
    builder.push_stmt_ir(IRStmt::MovV(counter, Value::I(64, 0)));
    let len_v = builder.stmt_let_anonymous();
    builder.push_stmt_ir(IRStmt::MovV(len_v, Value::I(64, len)));
    let one = builder.stmt_let_anonymous();
    builder.push_stmt_ir(IRStmt::MovV(one, Value::I(64, 1)));
    let label_cont = builder.new_label();
    let label_fin = builder.new_label();
    let label_begin = builder.new_label();
    let label_end = builder.new_label();
    let cond = builder.stmt_let_anonymous();
    let u = builder.stmt_let_anonymous();
    builder.push_stmt_ir(IRStmt::SetLabel(label_begin));
    builder.push_stmt_ir(IRStmt::GetArrayElemAddr(u, ref_res, counter));
    builder.push_stmt_ir(IRStmt::MovRI(u, v));
    builder.push_stmt_ir(IRStmt::III(IRInst::Add, counter, counter, one));
    builder.push_stmt_ir(IRStmt::III(IRInst::Eq, cond, counter, len_v));
    builder.push_stmt_ir(IRStmt::IfElse(cond, label_fin, label_cont));
    builder.push_stmt_ir(IRStmt::SetLabel(label_fin));
    builder.push_stmt_ir(IRStmt::JmpL(label_end));
    builder.push_stmt_ir(IRStmt::SetLabel(label_cont));
    builder.push_stmt_ir(IRStmt::JmpL(label_begin));
    builder.push_stmt_ir(IRStmt::SetLabel(label_end));

    /*
    for i in 0..len {
        let t = builder.stmt_let_anonymous();
        builder.push_stmt_ir(IRStmt::MovV(t, Value::I(64, i)));
        let u = builder.stmt_let_anonymous();
        builder.push_stmt_ir(IRStmt::GetArrayElemAddr(u, ref_res, t));
        builder.push_stmt_ir(IRStmt::MovRI(u, v));
    }
    */
    res
}

fn gen_ir_expr_addr(ir: &mut IR, builder: &mut FunctionBuilder, expr: &Expr) -> Id<VariableInfo> {
    match expr {
        Expr::ExprBlock(expr_block) => gen_ir_expr_block_addr(ir, builder, expr_block),
        Expr::If(expr_if) => gen_ir_expr_if_addr(ir, builder, expr_if),
        Expr::ExprCall(expr_call) => gen_ir_expr_call_addr(ir, builder, expr_call),
        Expr::ExprLit(expr_lit) => gen_ir_expr_lit_addr(ir, builder, expr_lit),
        Expr::ExprUnit(expr_unit) => gen_ir_expr_unit_addr(ir, builder, expr_unit),
        Expr::ExprTuple(expr_tuple) => gen_ir_expr_tuple_addr(ir, builder, expr_tuple),
        Expr::ExprAssign(expr_assign) => gen_ir_expr_assign_addr(ir, builder, expr_assign),
        Expr::Paren(expr_paren) => gen_ir_expr_paren_addr(ir, builder, expr_paren),
        Expr::Binary(expr_binary) => gen_ir_expr_binary_addr(ir, builder, expr_binary),
        Expr::Match(expr_match) => gen_ir_expr_match_addr(ir, builder, expr_match),
        Expr::Data(expr_data) => gen_ir_expr_data_addr(ir, builder, expr_data),
        Expr::Unary(expr_unary) => gen_ir_expr_unary_addr(ir, builder, expr_unary),
        Expr::Index(expr_index) => gen_ir_expr_index_addr(ir, builder, expr_index),
        Expr::Repeat(expr_repeat) => gen_ir_expr_repeat_addr(ir, builder, expr_repeat),
        Expr::Loop(expr_loop) => gen_ir_expr_loop_addr(ir, builder, expr_loop),
        Expr::Break(expr_break) => gen_ir_expr_break_addr(ir, builder, expr_break),
    }
}

fn gen_ir_expr_block_addr(
    _ir: &mut IR,
    _builder: &mut FunctionBuilder,
    _expr_block: &Block,
) -> Id<VariableInfo> {
    unimplemented!()
}

fn gen_ir_expr_if_addr(
    _ir: &mut IR,
    _builder: &mut FunctionBuilder,
    _expr_if: &ExprIf,
) -> Id<VariableInfo> {
    unimplemented!()
}

fn gen_ir_expr_call_addr(
    _ir: &mut IR,
    _builder: &mut FunctionBuilder,
    _expr_call: &ExprCall,
) -> Id<VariableInfo> {
    unimplemented!()
}

fn gen_ir_expr_lit_addr(
    _ir: &mut IR,
    builder: &mut FunctionBuilder,
    expr_lit: &ExprLit,
) -> Id<VariableInfo> {
    match &expr_lit.lit {
        Lit::Num(_num) => {
            unimplemented!();
        }
        Lit::Builtin(_) => {
            panic!("expr_lit = {:?}", expr_lit);
        }
        Lit::Verbatim(token) => {
            let res = builder.stmt_let_anonymous();
            builder.push_stmt_ir(IRStmt::GetReferenceByName(res, token.s.clone()));
            res
        }
    }
}

fn gen_ir_expr_unit_addr(
    _ir: &mut IR,
    _builder: &mut FunctionBuilder,
    _expr_unit: &ExprUnit,
) -> Id<VariableInfo> {
    unimplemented!()
}

fn gen_ir_expr_tuple_addr(
    _ir: &mut IR,
    _builder: &mut FunctionBuilder,
    _expr_tuple: &ExprTuple,
) -> Id<VariableInfo> {
    unimplemented!()
}

fn gen_ir_expr_assign_addr(
    _ir: &mut IR,
    _builder: &mut FunctionBuilder,
    _expr_assign: &ExprAssign,
) -> Id<VariableInfo> {
    unimplemented!()
}

fn gen_ir_expr_paren_addr(
    ir: &mut IR,
    builder: &mut FunctionBuilder,
    expr_paren: &ExprParen,
) -> Id<VariableInfo> {
    gen_ir_expr_addr(ir, builder, &expr_paren.expr)
}

fn gen_ir_expr_binary_addr(
    _ir: &mut IR,
    _builder: &mut FunctionBuilder,
    _expr_binary: &ExprBinary,
) -> Id<VariableInfo> {
    unimplemented!()
}

fn gen_ir_expr_match_addr(
    _ir: &mut IR,
    _builder: &mut FunctionBuilder,
    _expr_match: &ExprMatch,
) -> Id<VariableInfo> {
    unimplemented!()
}

fn gen_ir_expr_data_addr(
    _ir: &mut IR,
    _builder: &mut FunctionBuilder,
    _expr_data: &ExprData,
) -> Id<VariableInfo> {
    unimplemented!()
}

fn gen_ir_expr_unary_addr(
    ir: &mut IR,
    builder: &mut FunctionBuilder,
    expr_unary: &ExprUnary,
) -> Id<VariableInfo> {
    match expr_unary.op {
        UnOp::Neg(_) => {
            panic!("neg in addr");
        }
        UnOp::Ref(_) => unimplemented!(),
        UnOp::Deref(_) => {
            let t1 = gen_ir_expr_value(ir, builder, &expr_unary.expr);
            t1
        }
    }
}

fn gen_ir_expr_index_addr(
    ir: &mut IR,
    builder: &mut FunctionBuilder,
    expr_index: &ExprIndex,
) -> Id<VariableInfo> {
    let res = builder.stmt_let_anonymous();
    let expr = gen_ir_expr_addr(ir, builder, &expr_index.expr);
    let index = gen_ir_expr_value(ir, builder, &expr_index.index);
    builder.push_stmt_ir(IRStmt::GetArrayElemAddr(res, expr, index));
    res
}

fn gen_ir_expr_repeat_addr(
    _ir: &mut IR,
    _builder: &mut FunctionBuilder,
    _expr_repeat: &ExprRepeat,
) -> Id<VariableInfo> {
    unimplemented!()
}

fn gen_ir_expr_loop_addr(
    _ir: &mut IR,
    _builder: &mut FunctionBuilder,
    _expr_loop: &ExprLoop,
) -> Id<VariableInfo> {
    unimplemented!()
}

fn gen_ir_expr_break_addr(
    _ir: &mut IR,
    _builder: &mut FunctionBuilder,
    _expr_break: &ExprBreak,
) -> Id<VariableInfo> {
    unimplemented!()
}
