pub mod compiler;

use compiler::*;
use neco_ir::IRConfig;
use std::path::Path;

pub fn compile<P: AsRef<Path>>(config: IRConfig, path: P) -> String {
    let mut compiler = Compiler::new_with_config(config);
    compiler.load(path);
    compiler.resolve_name();
    compiler.infer_types();
    compiler.inlining();
    compiler.convert_to_x64();
    compiler.convert_to_ptx();
    let mut res = compiler.emit_x64();
    let ptx = compiler.emit_ptx();
    eprintln!("ptx code = \n{}", compiler.emit_ptx());

    let escape = |s: &str| {
        let mut res = String::new();
        for c in s.chars() {
            if c == '\n' {
                res.push_str("\\n");
            } else {
                res.push(c);
            }
        }
        res
    };
    res.push_str("    .section .rodata\n");
    res.push_str(&format!("__kernel:\n"));
    res.push_str(&format!("    .string \"{}\"\n", escape(&ptx)));
    eprintln!();
    res
}

#[cfg(test)]
mod tests {
    use super::*;

    fn test_by_status_code<P: AsRef<Path>>(path: P, expected: i32) {
        use std::process::Command;
        let stem = path.as_ref().file_stem().unwrap().to_str().unwrap();
        let asm_path = format!("/tmp/{}.s", stem);
        let obj_path = format!("/tmp/{}.o", stem);
        let exe_path = format!("/tmp/{}.out", stem);

        let s = compile(IRConfig::new(), path);
        std::fs::write(&asm_path, &s).unwrap();

        let as_status = Command::new("as")
            .arg("-o")
            .arg(&obj_path)
            .arg(&asm_path)
            .status()
            .unwrap();

        assert!(as_status.success());

        let ld_status = Command::new("ld")
            .arg("-o")
            .arg(&exe_path)
            .arg(&obj_path)
            .status()
            .unwrap();

        assert!(ld_status.success());

        let aout_status = Command::new(&exe_path).status().unwrap();

        assert_eq!(aout_status.code(), Some(expected));
    }

    macro_rules! add_test {
        ($s:ident, $f:expr, $e:expr) => {
            #[test]
            fn $s() {
                test_by_status_code(
                    &format!("{}/../../tests/Felis/{}.fe", env!("CARGO_MANIFEST_DIR"), $f),
                    $e,
                );
            }
        };
    }

    fn test_by_status_code_cuda<P: AsRef<Path>>(path: P, expected: i32) {
        use std::process::Command;
        let stem = path.as_ref().file_stem().unwrap().to_str().unwrap();
        let asm_path = format!("/tmp/{}.s", stem);
        let obj_path = format!("/tmp/{}.o", stem);
        let exe_path = format!("/tmp/{}.out", stem);

        let mut config = IRConfig::new();
        config.use_crt = true;
        let s = compile(config, path);
        std::fs::write(&asm_path, &s).unwrap();

        let as_status = Command::new("as")
            .arg("-o")
            .arg(&obj_path)
            .arg(&asm_path)
            .status()
            .unwrap();

        assert!(as_status.success());

        let gcc_status = Command::new("gcc")
            .arg("-o")
            .arg(&exe_path)
            .arg(&asm_path)
            .arg("/opt/cuda/lib64/stubs/libcuda.so")
            .status()
            .unwrap();

        assert!(gcc_status.success());

        let aout_status = Command::new(&exe_path).status().unwrap();

        assert_eq!(aout_status.code(), Some(expected));
    }

    macro_rules! add_test_cuda {
        ($s:ident, $f:expr, $e:expr) => {
            #[test]
            #[ignore]
            fn $s() {
                test_by_status_code_cuda(
                    &format!("{}/../../tests/Felis/{}.fe", env!("CARGO_MANIFEST_DIR"), $f),
                    $e,
                );
            }
        };
    }

    add_test!(test_sys_exit, "sys_exit", 42);

    add_test!(test_let, "let", 42);
    add_test!(test_let_in_block, "let_in_block", 6);
    add_test!(
        test_let_with_initial_value_1,
        "let_with_initial_value_1",
        42
    );
    add_test!(test_let_with_initial_value_2, "let_with_initial_value_2", 6);
    add_test!(
        test_let_with_initial_value_3,
        "let_with_initial_value_3",
        42
    );
    add_test!(
        test_let_with_initial_value_4,
        "let_with_initial_value_4",
        42
    );
    add_test!(test_let_shadow_1, "let_shadow_1", 42);
    add_test!(test_let_shadow_2, "let_shadow_2", 42);

    add_test!(test_arithmetic_add, "arithmetic_add", 42);
    add_test!(test_arithmetic_sub, "arithmetic_sub", 42);
    add_test!(test_arithmetic_mul, "arithmetic_mul", 42);
    add_test!(test_arithmetic_div, "arithmetic_div", 42);
    add_test!(test_arithmetic_mod, "arithmetic_mod", 2);
    add_test!(test_arithmetic_precedence_1, "arithmetic_precedence_1", 42);
    add_test!(test_arithmetic_precedence_2, "arithmetic_precedence_2", 4);
    add_test!(test_arithmetic_paren_1, "arithmetic_paren_1", 14);
    add_test!(test_arithmetic_paren_2, "arithmetic_paren_2", 10);
    add_test!(test_arithmetic_negate_1, "arithmetic_negate_1", 3);
    add_test!(test_arithmetic_negate_2, "arithmetic_negate_2", 50);

    add_test!(test_line_comment_1, "line_comment_1", 42);
    add_test!(test_line_comment_2, "line_comment_2", 42);
    add_test!(test_line_comment_3, "line_comment_3", 42);

    add_test!(test_block_comment_1, "block_comment_1", 42);
    add_test!(test_block_comment_2, "block_comment_2", 42);

    add_test!(test_function_call_1, "function_call_1", 42);
    add_test!(test_function_call_2, "function_call_2", 8);
    add_test!(test_function_call_3, "function_call_3", 90);
    add_test!(test_function_call_4, "function_call_4", 3);
    add_test!(test_function_args, "function_args", 30);

    add_test!(test_if, "if", 6);

    add_test!(test_data_and_match_1, "data_and_match_1", 3);
    add_test!(test_data_and_match_2, "data_and_match_2", 3);
    add_test!(test_data_and_match_3, "data_and_match_3", 6);

    add_test!(test_reference_1, "reference_1", 1);
    add_test!(test_reference_2, "reference_2", 20);
    add_test!(test_reference_3, "reference_3", 40);

    add_test!(test_array_1, "array_1", 30);
    add_test!(test_array_2, "array_2", 42);

    add_test!(test_compare_eq_1, "compare_eq_1", 2);
    add_test!(test_compare_eq_2, "compare_eq_2", 3);
    add_test!(test_compare_lt_1, "compare_lt_1", 3);
    add_test!(test_compare_lt_2, "compare_lt_2", 4);
    add_test!(test_compare_leq_1, "compare_leq_1", 3);
    add_test!(test_compare_leq_2, "compare_leq_2", 3);
    add_test!(test_compare_leq_3, "compare_leq_3", 4);
    add_test!(test_compare_gt_1, "compare_gt_1", 4);
    add_test!(test_compare_gt_2, "compare_gt_2", 3);
    add_test!(test_compare_geq_1, "compare_geq_1", 4);
    add_test!(test_compare_geq_2, "compare_geq_2", 3);
    add_test!(test_compare_geq_3, "compare_geq_3", 3);

    add_test!(test_if_else_1, "if_else_1", 10);
    add_test!(test_if_else_2, "if_else_2", 20);
    add_test!(test_if_else_3, "if_else_3", 30);

    add_test!(test_typed_literal_1, "typed_literal_1", 42);
    add_test!(test_typed_literal_2, "typed_literal_2", 42);

    add_test!(test_f64, "f64", 42);
    add_test!(test_f64_add, "f64_add", 13);
    add_test!(test_f64_sub, "f64_sub", 1);
    add_test!(test_f64_mul, "f64_mul", 43);
    add_test!(test_f64_div, "f64_div", 6);

    add_test!(test_heap_1, "heap_1", 42);
    add_test!(test_heap_2, "heap_2", 30);

    add_test!(test_attribute_1, "attribute_1", 42);
    add_test!(test_attribute_2, "attribute_2", 42);

    add_test!(test_f32, "f32", 42);
    add_test!(test_f32_add, "f32_add", 13);
    add_test!(test_f32_sub, "f32_sub", 1);
    add_test!(test_f32_mul, "f32_mul", 43);
    add_test!(test_f32_div, "f32_div", 6);

    add_test!(test_inline, "inline", 42);

    add_test!(test_ptx_1, "ptx_1", 42);
    add_test!(test_ptx_2, "ptx_2", 42);
    add_test!(test_ptx_3, "ptx_3", 42);
    add_test!(test_ptx_4, "ptx_4", 42);
    add_test_cuda!(test_ptx_5, "ptx_5", 42);
    add_test_cuda!(test_ptx_6, "ptx_6", 42);
    add_test_cuda!(test_ptx_7, "ptx_7", 42);
    add_test_cuda!(test_ptx_8, "ptx_8", 42);

    add_test!(test_loop_1, "loop_1", 3);
    add_test!(test_loop_2, "loop_2", 5);
    add_test!(test_loop_3, "loop_3", 42);
    add_test!(test_loop_4, "loop_4", 42);
}
