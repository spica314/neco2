use neco::*;
use neco_ir::*;

fn main() {
    let args: Vec<_> = std::env::args().collect();
    eprintln!("args = {:?}", args);
    let mut sources = vec![];
    let mut config = IRConfig::new();
    for arg in args.iter().skip(1) {
        if arg.starts_with("--") {
            if arg == "--use-crt" {
                config.use_crt = true;
                continue;
            }
            eprintln!("unknown option {}", arg);
            return;
        }
        sources.push(arg.clone());
    }
    if sources.len() == 1 {
        let s = compile(config, &sources[0]);
        println!("{}", s);
    } else {
        panic!();
    }
}
